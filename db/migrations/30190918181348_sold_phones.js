exports.up = knex => knex.schema.createTable('sold_phones', (table) => {
  table.increments('id').primary();
  
  table.string('seller_name');
  table.string('seller_tel');
  table.string('seller_email').notNullable();
  table.string('seller_referrel');

  table.string('buyer_name');
  table.string('buyer_tel');
  table.string('buyer_email').notNullable();

  table.integer('phone_id').unsigned();
  table.foreign('phone_id').references('id').inTable('products_inventory');

	table.timestamp('created_at').defaultTo(knex.fn.now());
	table.unique('id');
});

exports.down = knex => knex.schema.dropTable('sold_phones');

