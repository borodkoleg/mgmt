exports.up = knex => knex.schema.createTable('photos_phone', (table) => {
  table.increments('id').primary();
  
  table.string('name');
  table.integer('key');

  table.integer('phone_id').unsigned();
  table.foreign('phone_id').references('id').inTable('products_inventory');

  table.timestamp('created_at').defaultTo(knex.fn.now());
  
  table.unique('id');
});

exports.down = knex => knex.schema.dropTable('photos_phone');