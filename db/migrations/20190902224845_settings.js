exports.up = knex => knex.schema.createTable('settings', (table) => {
  table.increments('id').primary();

  table.datetime('last_update');
  
  table.unique('id');
});

exports.down = knex => knex.schema.dropTable('settings');
