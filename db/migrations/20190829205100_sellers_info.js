exports.up = knex => knex.schema.createTable('sellers_info', (table) => {
  table.increments('id').primary();

  table.string('seller_name'); //+
  table.string('seller_tel'); //+
  table.string('seller_email').notNullable(); //+
  table.string('seller_referrel'); //+
  table.string('ad_type_sold');
  table.string('seller_identification').notNullable(); //+

  table.integer('category_id').unsigned();
  table.foreign('category_id').references('id').inTable('categories');

	table.timestamp('created_at').defaultTo(knex.fn.now());

	//table.unique(['seller_name', 'seller_email']);
  table.unique('seller_email');
  table.unique('id');
  table.unique('seller_identification');
});

exports.down = knex => knex.schema.dropTable('sellers_info');
