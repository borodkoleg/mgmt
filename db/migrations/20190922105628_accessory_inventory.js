exports.up = knex => knex.schema.createTable('accessory_inventory', (table) => {
  table.increments('id').primary();

  table.string('product_code');
  table.string('product_name');
  table.string('brand');
  table.string('model');
  table.integer('quantity');	
  table.float('buying_price');
  table.float('selling_price');

  table.integer('category_id').unsigned();
  table.foreign('category_id').references('id').inTable('categories');

  table.timestamp('created_at').defaultTo(knex.fn.now());
  
  table.unique('id');
  table.unique(['product_code', 'product_name']);
});

exports.down = knex => knex.schema.dropTable('accessory_inventory');
