exports.up = knex => knex.schema.createTable('cameras', (table) => {
  table.increments('id').primary();

  table.string('sku');
	table.string('brand');
	table.string('model');
	table.string('serial_number');
	table.integer('shutter_count');
	table.string('condition');
	table.string('warranty');
	table.string('accessories');
	table.boolean('is_sold').defaultTo(false);

	table.integer('category_id').unsigned();
  table.foreign('category_id').references('id').inTable('categories');

  table.timestamp('created_at').defaultTo(knex.fn.now());
  
  table.unique('id');
  table.unique('serial_number');
});

exports.down = knex => knex.schema.dropTable('cameras');