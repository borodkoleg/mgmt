exports.up = knex => knex.schema.createTable('users', (table) => {
  table.increments('id').primary();
  table.string('login').notNullable();
  table.string('password').notNullable();
  //created_at defaultTo(knex.fn.now())
  //table.timestamps(true, true);
  //knex.raw('SELECT CURRENT_TIMESTAMP()') TIMESTAMP();
	table.timestamp('created_at').defaultTo(knex.fn.now());

  table.unique('login');
  table.unique('id');
});

exports.down = knex => knex.schema.dropTable('users');