exports.up = knex => knex.schema.createTable('sold_accessories', (table) => {
  table.increments('id').primary();
  
  table.string('product_code');
  table.string('product_name');
  table.string('brand');
  table.string('model');
  table.integer('quantity');	
  table.string('category');
  table.float('buying_price');
  table.float('selling_price');

  table.timestamp('created_at').defaultTo(knex.fn.now());
  
  table.unique('id');
});

exports.down = knex => knex.schema.dropTable('sold_accessories');