exports.up = knex => knex.schema.createTable('buyers_info', (table) => {
  table.increments('id').primary();

  table.string('buyer_name');
  table.string('buyer_tel');
  table.string('buyer_email').notNullable();

  table.integer('category_id').unsigned();
  table.foreign('category_id').references('id').inTable('categories');

	table.timestamp('created_at').defaultTo(knex.fn.now());

	//table.foreign('id').references('buyer_id').inTable('products_buyers');

  table.unique('buyer_email');
  table.unique('id');
});

exports.down = knex => knex.schema.dropTable('buyers_info');
