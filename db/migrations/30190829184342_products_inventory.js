exports.up = knex => knex.schema.createTable('products_inventory', (table) => {
  table.increments('id').primary();
  table.datetime('purchase_date');
  table.datetime('device_updated_date');
  table.string('product_code'); //++
  table.string('brand'); //++
  table.string('imei'); //++
  table.string('model'); //++
  table.string('color'); //++
  table.string('carrier'); //++
  table.string('warranty'); //++
  table.string('condition'); //++
  table.string('case'); //++
  table.date('date_sold');
  table.float('gb');

  table.float('repair_cost'); //++
  table.string('repair_place'); //+
  table.datetime('repair_date_sent'); //+
  table.datetime('repair_date_return'); //+

  table.float('selling_price'); //++
  table.float('buying_price'); //++
  table.string('product_status'); //++

  table.float('total_buying_price');
  
  table.boolean('is_deleted').defaultTo(false);

  table.string('screen_protector');
  table.string('headphone');
  table.float('total_upsell');
  table.float('total_profit');
  table.string('notes');

  table.integer('seller_id').unsigned();
  table.integer('buyer_id').unsigned();
  table.foreign('seller_id').references('id').inTable('sellers_info');
  table.foreign('buyer_id').references('id').inTable('buyers_info');

  table.integer('category_id').unsigned();
  table.foreign('category_id').references('id').inTable('categories');

	table.timestamp('created_at').defaultTo(knex.fn.now());
	table.unique('id');
});

exports.down = knex => knex.schema.dropTable('products_inventory');
