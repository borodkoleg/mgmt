exports.up = knex => knex.schema.createTable('categories', (table) => {
  table.increments('id').primary();  
  table.string('name').notNullable();
  table.integer('index').notNullable();

	table.timestamp('created_at').defaultTo(knex.fn.now());
	table.unique('id');
	table.unique(['name', 'index']);
});

exports.down = knex => knex.schema.dropTable('categories');
