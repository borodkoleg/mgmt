require('dotenv').config()
const { cryptPasswordSync } = require('../../src/server/libs/bcrypt.js');

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      let password = cryptPasswordSync(process.env.ADMIN_PASSWORD);  	
  		return knex('users').insert([
    	{id: 1, password: password, login: process.env.ADMIN_LOGIN}
  		]);
    });
};
