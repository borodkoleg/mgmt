require('dotenv').config()
const Koa = require('koa')
const path = require('path')
const staticFIles = require('koa-static')
const fs = require('fs')
//const route = require('koa-route')
const Router = require('koa-router');
const router = new Router();
const bodyParser = require('koa-bodyparser');
const errorHandle = require('koa-json-error');
const server = require('./src/server');
const cors = require('koa2-cors');
const jwtMiddleware = require('koa-jwt');
const { createReadStream } = require ('fs');
const helmet = require("koa-helmet");

const app = new Koa()
//const serverApi = require('./server')
//var serve = require('koa-static-server')

app.use(helmet());

app.use(errorHandle({
    format: function formatError(err) {
    	return {
    		res: false,
    		errors: err.message
    	};
    }
}));

if (process.env.NODE_ENV === 'development') { //temp !]
	app.use(cors({
		origin: '*',
  	// maxAge: 5,
  	// credentials: true,
  	// allowMethods: ['GET', 'POST', 'DELETE']
	})); 
}

if (process.env.NODE_ENV === 'production') {
	// app.use(cors({
	// 	origin: process.env.HTTPS_DOMAIN
	// })); 
}

app.use(staticFIles(path.resolve('build')))
app.use(staticFIles(path.resolve('public')))

app.use(
  jwtMiddleware({
    secret: process.env.REACT_APP_JWT_SECRET,
    cookie: 'token'
  }).unless({
    path: [
    	'/',
    	'/login',
    	'/mgmt',
    	'/settings',
    	'/upsells-accessories',
    	'/sellers',
    	'/buyers',
    	'/sold_phones',
    	'/basis',
    	'/all-products',
    	'/products-category',
    	'/add-product',
    	'/all-sellers',
    	'/add-seller',
    	'/sellers-category',
    	'/edit-seller',
    	'/all-buyers',
    	'/add-buyer',
    	'/buyers-category',
    	'/edit-buyer',
    	'/edit-product',
    	'/sold-products',
    	'/all-accessories',
    	'/accessories-category',
    	'/add-accessories',
    	'/edit-accessories',
    	'/sold-accessories',
    	'/all-cameras',
    	'/add-camera',
    	'/cameras-category',
    	'/edit-camera',
    	'/cameras-sold',
    	'/add-other',
    	'/others-category',
    	'/all-others',
    	'/edit-other',
    	'/others-sold',
    	
      '/api/user_check'
      //'/api/mail_test'
    ],
  }),
);

app.use(bodyParser());
app.use(server());

router.get(
	'*',
  async (ctx, next) => {
	  ctx.type = 'html';
	  ctx.body = createReadStream('./build/index.html');
});

app.use(router.routes());
app.use(router.allowedMethods());


// app.use(route.get('/123', (ctx, next) => {
// 	ctx.body = 'Hello World!';
// }))
//app.use(serverApi())

if (process.env.NODE_ENV === 'test') {
	module.exports = app.listen(process.env.PORT)
} else {
	app.listen(process.env.PORT || 3001)
}