require('dotenv').config()

module.exports = {

  development: {
    client: 'mysql',
    connection: {
	    host : '127.0.0.1',
	    user : 'root',
	    password : 'root',
	    database : 'mgmt',
	    //timezone: 'UTC',
      //dateStrings: true
  	},
  	pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'mgmt_migrations',
      directory: './db/migrations',
    },
    seeds: {
      directory: './db/seeds',
    }
  },

  test: {
    client: 'mysql',
    connection: {
	    host : '127.0.0.1',
	    user : 'root',
	    password : 'root',
	    database : 'mgmt_test',
	    //timezone: 'UTC',
      //dateStrings: true
  	},
  	pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'mgmt_migrations',
      directory: './db/migrations',
    },
    seeds: {
      directory: './db/seeds',
    }
  },

  // production: { //heroku
  //   client: 'mysql',
  //   connection: `${process.env.CLEARDB_DATABASE_URL}?ssl=true`,
  //   migrations: {
  //     tableName: 'mgmt_migrations',
  //     directory: './db/migrations',
  //   },
  //   seeds: {
  //     directory: './db/seeds',
  //   },
  // }

  production: {
    client: 'mysql',
    connection: {
	    host : '127.0.0.1',
	    user : process.env.MYSQL_USER,
	    password : process.env.MYSQL_PASSWORD,
	    database : process.env.MYSQL_DATABASE,
	    timezone: 'UTC',
      dateStrings: true
  	},
  	pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'mgmt_migrations',
      directory: './db/migrations',
    },
    seeds: {
      directory: './db/seeds',
    }
  }

};
