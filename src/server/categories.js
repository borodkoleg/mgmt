require('dotenv').config();
const Router = require('koa-router');
const router = new Router();
const knex = require('./libs/knexConfig');

router.post('/api/get-categories', async (ctx, next) => {
	try {
		const index = ctx.request.body.index;

		let result = await knex.select('*')
			.from('categories')
			.where('index', index)

		ctx.response.status = 200;
		ctx.body = result;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/add-category', async (ctx, next) => {
	try {
		const index = ctx.request.body.index;
		const name = ctx.request.body.name;

		let result = await knex('categories').insert({
			name: name,
			index: index
		});

		ctx.response.status = 200;
		ctx.body = result;
	} catch(err){
		if (err.code === 'ER_DUP_ENTRY'){
			ctx.response.status = 200;
			ctx.body = false;
			return;
		}
		console.log(err);
		ctx.response.status = 400;
	}
});

router.post('/api/edit-category', async (ctx, next) => {
	try {
		const index = ctx.request.body.index;
		const name = ctx.request.body.name;
		const id = ctx.request.body.id;

		let result = await knex('categories')
		.where('id', id)
		.update({
			name: name,
			index: index
		});

		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		if (err.code === 'ER_DUP_ENTRY'){
			ctx.response.status = 200;
			ctx.body = false;
			return;
		}
		console.log(err);
		ctx.response.status = 400;
	}
});

router.post('/api/delete-category', async (ctx, next) => {
	try {
		const id = ctx.request.body.id;
		const index = ctx.request.body.index;

		if (index === 1){
			await knex('products_inventory')
  			.where('category_id', id)
  			.update({
  				category_id: null
  		})
		}

		if (index === 2){
			await knex('sellers_info')
  			.where('category_id', id)
  			.update({
  				category_id: null
  			})
		}

		if (index === 3){
			await knex('buyers_info')
  			.where('category_id', id)
  			.update({
  				category_id: null
  			})
		}

		if (index === 4){
			await knex('accessory_inventory')
  			.where('category_id', id)
  			.update({
  				category_id: null
  			})
		}

		if (index === 5){
			await knex('cameras')
  			.where('category_id', id)
  			.update({
  				category_id: null
  			})
		}

		if (index === 6){
			await knex('others')
  			.where('category_id', id)
  			.update({
  				category_id: null
  			})
		}

		await knex('categories')
  		.where('id', id)
  		.del()

		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

module.exports = router;