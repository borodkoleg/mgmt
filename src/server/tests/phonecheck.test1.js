const request = require('supertest');
const server = require('../../../index');

const knex = require('../libs/knexConfig');
const { fakeArray1, fakeArray2, fakeArray3 } = require('./phonecheckFake');

let token = '';
jest.setTimeout(10000);

beforeAll(async done => {
	let lastUpdate = await knex.select('*').from('settings');
	let testLastUpdate = '2019-09-16 19:17:09';
	if (lastUpdate.length === 0) {
		await knex('settings').insert([
     {
       id: 1,
       last_update: testLastUpdate
     },
    ]);
	} else {
		let test = await knex('settings')
		 		  .where('id', 1)
		 		  .update({'last_update': testLastUpdate});
	}

	token = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNTY5MDkyMTU2LCJleHAiOjE1NjkxMTAxNTZ9.i9DOoBFSDIjAR9LYkPiRIYb9S3akzlrj2S1BaN9E0Jk';

  done();
});

afterAll(() => {
  server.close();
});


describe('1....phonecheck....3', () => {
	test('...check from date to date...', async () => {
		await knex('products_inventory').del();

		const response = await request(server)
	      .post('/api/get-new-phones')
	      .send({
	        data: fakeArray1,
	        real: true //real data instead fake
	  		})
	  		.set('Authorization', token)

	  let prInv = await knex('products_inventory').select('*');

	  prInv.forEach(function(obj){
	  	console.log(obj.device_updated_date);
	  });

	  //console.log(lastUpdate);
	  expect(response.status).toEqual(200);
  });

  test('...some imei and device_updated_date...', async () => {
		await knex('products_inventory').del();

		const response = await request(server)
	      .post('/api/get-new-phones')
	      .send({
	        data: fakeArray1,
	        real: false //real data instead fake
	  		})
	  		.set('Authorization', token)

	  let prInv = await knex('products_inventory').select('*');

	  // let lastUpdate = await knex.select('*').from('settings');
	  // lastUpdate = lastUpdate[0]['last_update'];

	  // expect(lastUpdate).toEqual(new Date('2019-09-04 19:17:09'));
	  expect(prInv.length).toEqual(1);
  });

  test('...some imei and different device_updated_date...', async () => {
		await knex('products_inventory').del();

		const response = await request(server)
	      .post('/api/get-new-phones')
	      .send({
	        data: fakeArray2,
	        real: false //real data instead fake
	  		})
	  		.set('Authorization', token)

	  let prInv = await knex('products_inventory').select('*');

	  expect(prInv.length).toEqual(1);
  });

  //   test('...counts data...', async () => {
		// await knex('products_inventory').del();

		// const response = await request(server)
	 //      .post('/api/get-new-phones')
	 //      .send({
	 //        data: fakeArray3,
	 //        real: false //real data instead fake
	 //  		})
	 //  		.set('Authorization', token)

	 //  let prInv = await knex('products_inventory').select('*');

	 //  // let lastUpdate = await knex.select('*').from('settings');
	 //  // lastUpdate = lastUpdate[0]['last_update'];
	  
	 //  // expect(lastUpdate).toEqual(new Date('2019-09-04 19:17:09'));
	 //  expect(prInv.length).toEqual(2);
  // });

  // test('...sold out...', async () => {
		// await knex('products_inventory').del();

		// const response = await request(server)
	 //      .post('/api/get-new-phones')
	 //      .send({
	 //        data: fakeArray3,
	 //        real: false //real data instead fake
	 //  		})
	 //  		.set('Authorization', token)

	 //  let pr_iy = await knex.select('*').from('products_inventory');

	 //  console.log(pr_iy.length);

	  // let id = pr_iy[0]['id'];

	  // const response2 = await request(server)
	  //     .post('/api/remove_products')
	  //     .send({
	  //       selectedRows: [{"id":id}]
	  // 		})
	  // 		.set('Authorization', token)

	  // const response3 = await request(server)
	  //     .post('/api/get_products')
	  //     .send({
	        
	  // 		})
	  // 		.set('Authorization', token)
	  // 		.expect('Content-Type', /json/)
	  // 		.expect(200)
	  
	  // let is_deleted = response3.body[0].is_deleted;
	  
	  // expect(is_deleted).toEqual(0);
  // });


});