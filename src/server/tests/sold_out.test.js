const request = require('supertest');
const server = require('../../../index');

const knex = require('../libs/knexConfig');
const { fakeArray1, fakeArray2, fakeArray3 } = require('./phonecheckFake');

let token = '';
jest.setTimeout(10000);

beforeAll(async done => {
	//запускается до всех тестов в файле один раз
	let lastUpdate = await knex.select('*').from('settings');
	let testLastUpdate = '2019-09-16 19:17:09';

	if (lastUpdate.length === 0) {
		await knex('settings').insert([
     {
       id: 1,
       last_update: testLastUpdate
     },
    ]);
	} else {
		let test = await knex('settings')
		 		  .where('id', 1)
		 		  .update({'last_update': testLastUpdate});
	}

	token = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNTY5MDkyMTU2LCJleHAiOjE1NjkxMTAxNTZ9.i9DOoBFSDIjAR9LYkPiRIYb9S3akzlrj2S1BaN9E0Jk';

  done();
});

afterAll(() => {
  server.close();
});


describe('phonecheck sold out test', () => {
  test('...sold out...', async () => {
  	await knex('sold_phones').del();
		await knex('products_inventory').del();

		const response = await request(server)
	      .post('/api/get-new-phones')
	      .send({
	        data: fakeArray3,
	        real: false //real data instead fake
	  		})
	  		.set('Authorization', token)

	  let pr_iy = await knex.select('*').from('products_inventory');

	  //console.log(pr_iy.length);

	  expect(pr_iy.length).toEqual(2);

	  let obj1 = pr_iy[0];
	  //console.log(id);

	  const response2 = await request(server)
	      .post('/api/remove_products')
	      .send({
	        selectedRows: [obj1]
	  		})
	  		.set('Authorization', token)

    expect(response2.status).toEqual(200);

		const response3 = await request(server)
		    .post('/api/get_products')
		    .send({
		      
				})
				.set('Authorization', token)
				.expect('Content-Type', /json/)
				.expect(200)

	 //console.log(JSON.stringify(response3.body, null, 2));
	  
	  expect(response3.body.length).toEqual(1);
  });
});