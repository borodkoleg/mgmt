require('dotenv').config();
const Router = require('koa-router');
const router = new Router();
const Axios = require('axios');
const knex = require('./libs/knexConfig');
const { getDatesBetween, dayToday, compareDate } = require('./libs/date');

router.post('/api/get-new-phones', async (ctx, next) => {
	try{

	let lastUpdate = await knex.select('*').from('settings');
	lastUpdate = lastUpdate[0]['last_update'];

	const arrayDates = getDatesBetween(lastUpdate, dayToday);

	let arrayFromPromice = [];

	arrayDates.forEach(function(date) {
		arrayFromPromice.push(
			phonecheckPromise(date)
		)
	});

	let testCounter = 0;

	function phonecheckPromise(date){
		return new Promise(function(resolve) {
			Axios.post('https://clientapiv2.phonecheck.com/cloud/cloudDB/GetAllDevices' , {
					apiKey: process.env.PHONECHECK_API_KEY,
					username: process.env.PHONECHECK_USERNAME,
					date: date
				},
		  	{
		      headers: {
		          "Content-Type": "multipart/form-data"
		      },
		      params: {}
		  })
		  .then((res) => { 
		  	//console.log('data---->' + JSON.stringify(res['data'], null, 2));
		  	if (process.env.NODE_ENV === 'test'){
					//array of objectPhone
					const data = ctx.request.body.data;
					const real = ctx.request.body.real;

					if (testCounter < 2 && !real){
						resolve(data[testCounter]);
					} 

					if (testCounter >= 2 && !real){
						resolve(data[2]);
					}

					if (real){
						resolve(res['data']);
					}

					testCounter += 1;
				} else {
					// console.log('==========>' + JSON.stringify(res['status']));
		  		resolve(res['data']);
		  	}
		  })
		  .catch(err => {
		  	if (err.response.status === 404){
		  		resolve([]);
		  		return;
		  	}
		  	throw(err);
		  	resolve(false);
		  });
		});
	}

	let knexPromisesArray = [];
	let phonecheckResult = [];

	if (process.env.NODE_ENV === 'test' && arrayFromPromice.length<3){
		console.log(`!!! ${arrayFromPromice.length} < 3`);
		ctx.response.status = 400;
		ctx.body = false;
	}

	await Promise.all(arrayFromPromice).then((arrayResults) => {
		arrayResults.forEach(function(arrayOfPhones) {
			arrayOfPhones.forEach(function(obj, index) {
				//let obj = Object.assign({}, objRes);
				let data = {
					purchase_date: obj.DeviceCreatedDate,
					device_updated_date: obj.DeviceUpdatedDate,
					product_code: obj.Serial,
					brand: obj.Make,
					imei: obj.IMEI,
					model: obj.Model,
					color: obj.Color,
					carrier: obj.Carrier,
					warranty: obj.Notes,
					condition: obj.Grade,
					case: obj.Cosmetics,
				};

				phonecheckResult.push(data);
			});
		});
	});


	//console.log('phonecheckResult===>' + phonecheckResult.length); 

	for (let i = 0; i < phonecheckResult.length; i ++){

		let obj = phonecheckResult[i];
		let newPhone = {
			purchase_date: obj.purchase_date,
			device_updated_date: obj.device_updated_date,
			product_code: obj.product_code,
			brand: (obj.brand).toUpperCase(),
			imei: obj.imei,
			model: (obj.model).toUpperCase(),
			color: (obj.color).toUpperCase(),
			carrier: (obj.carrier).toUpperCase(),
			warranty: (obj.warranty).toUpperCase(),
			condition: (obj.condition).toUpperCase(),
			case: (obj.case).toUpperCase()
		}

		//console.log(newPhone.device_updated_date); 

		let arrWithIdDate = await knex('products_inventory')
		  .where({
		  	imei: newPhone.imei,
		  	is_deleted: 0
		  })
		 	.select('id', 'device_updated_date');
	  //console.log(newPhone.imei);
		//console.log('=arrWithIdDate===========>' + arrWithIdDate.length);

		if (arrWithIdDate.length > 0){
		 	const id = arrWithIdDate[0]['id'];
		 	const purchaseDateInDB = arrWithIdDate[0]['device_updated_date'];
		 	//console.log(id);

		 	if (compareDate(newPhone.device_updated_date, purchaseDateInDB)){
		 		//console.log('newPhone========>' + newPhone);
		 		await knex('products_inventory')
		 		  .where('id', id)
		 		  .update(newPhone);
		 	}

		}	else {
			let somePhoneIsDeleted = await knex('products_inventory')
		  .where({
		  	imei: newPhone.imei,
		  	is_deleted: 1,
		  	device_updated_date: newPhone.device_updated_date
		  })
		 	.select('id');

			if (somePhoneIsDeleted.length === 0){
				//add phone
			 	await knex('products_inventory').insert(newPhone);
			}

		}
	}

	//settings last_update
	const deviceUpdatedDate = await knex('products_inventory').max('device_updated_date');
	const valuesUpdatedDate = Object.values(deviceUpdatedDate[0]);

	await knex.raw('REPLACE INTO settings SET last_update = ?, id = 1', [valuesUpdatedDate[0]]);


	ctx.response.status = 200;
	ctx.body = true;

	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}

});

module.exports = router;