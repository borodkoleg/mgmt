require('dotenv').config();
const Router = require('koa-router');
const router = new Router();
const knex = require('./libs/knexConfig');
const multer = require('@koa/multer');
const Path = require('path');
const fs = require('fs');

let newKey = -1;
let storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/uploads')
  },
  filename: function (req, file, cb) {
  	const id = req.body.id;
  	newKey += 1;

  	const keysArray = JSON.parse(req.body.keysArray);
		//objFileParse = JSON.parse(objFile);

		//console.log(JSON.stringify(keysArray, null, 2));
		//console.log('newKey=>' + JSON.stringify(file, null, 2));

  	//console.log(newKey)
  	//newKey += 1;
    cb(null, `${id}-${keysArray[newKey]}${Path.extname(file.originalname)}`)
  }
})

const upload = multer({ 
	storage: storage,
	limits: {fileSize: 5000000},
	fileFilter: function(req, file, cb){
    checkFileType(file, cb);
  }
})

function checkFileType(file, cb){
  const filetypes = /jpeg|jpg|png|gif|bmp/
  const extname = filetypes.test(Path.extname(file.originalname).toLowerCase());
  const mimetype = filetypes.test(file.mimetype);

  if(mimetype && extname){
      return cb(null,true);
  } else {
      cb('Error: Images Only!');
  }
}

router.post('/api/save-photos-phone', 
	 upload.fields([
    {
      name: 'file',
      maxCount: 5
    }
  ])
	, async (ctx, next) => {
		newKey = -1;
	try {
		const id = ctx.request.body.id; //phone id
		const objFile = ctx.request.body.objFile;

		objFileParse = JSON.parse(objFile);
		//console.log(__dirname);

		//console.log(objFileParse);
		//console.log('ctx.request.files', ctx.request.files);
    //console.log('ctx.files', ctx.files['file']);

    if (ctx.files['file'] && ctx.files['file'].length > 0){
    	let filesNumber = 0;
    	for (var key in objFileParse) {
    		if (!objFileParse[key.toString()]) {
    			//console.log(key)
    			continue;
    		}

    		//console.log(key)
    		//Object.keys(ctx.files['file'])[0]
    		//ctx.files['file']

    		let photo = await knex('photos_phone')
     	 		.where({
     	 			phone_id: id,
     	 			key: key
     	 		})
     	 	//console.log(photo);
     	 	
     	 	if (photo.length > 0){
     	 		//remove old file
     	 		if (Object.values(ctx.files['file'])[filesNumber]['filename'] !== photo[0]['name']){
	     	 		fs.unlink('./public/uploads/' + photo[0]['name'], (err) => {
						  // if (err) {
						  //   console.error(err)
						  //   return
						  // }

						  //file removed
						})
     	 		}

     	 		//update
     	 		await knex('photos_phone')
     	 			.where('id', photo[0]['id'])
	     	 		.update({
	     	 			phone_id: id,
	     	 			key: key,
							name: Object.values(ctx.files['file'])[filesNumber]['filename']
	     	 		})
     	 	} else {
     	 		//insert
     	 		await knex('photos_phone')
	     	 		.insert({
	     	 			phone_id: id,
	     	 			key: key,
							name: Object.values(ctx.files['file'])[filesNumber]['filename']
	     	 		})
     	 	}
     	 	filesNumber += 1;
     	}
    }
		// let result = await knex.select('*')
		// 	.from('categories')
		// 	.where('index', index)

		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/get-photos-by-phone', async (ctx, next) => {
	try {
		const id = ctx.request.body.id;

		let result = 
			await knex('photos_phone').
			where('phone_id', id)

		ctx.response.status = 200;
		ctx.body = result;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/delete-photo-by-name', async (ctx, next) => {
	try {
		const name = ctx.request.body.name;

		let result = 
			await knex('photos_phone')
			.where('name', name)
			.del();

		let promise = await new Promise(function(resolve) {
			fs.unlink('./public/uploads/' + name, (err) => {
			  if (err) {
			  	console.log(err);
			    resolve(false);
			  }
			  //file removed
			  resolve(true);
			})
		});
		
		if (promise){
			ctx.response.status = 200;
			ctx.body = true;
		} else {
			ctx.response.status = 400;
			ctx.body = false;
		}

	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

module.exports = router;