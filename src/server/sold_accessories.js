const Router = require('koa-router');
const router = new Router();
const knex = require('./libs/knexConfig');

router.post('/api/sold-accessories', async (ctx, next) => {
	try {
		const data = ctx.request.body.data;
		const id = ctx.request.body.id;
		const value = ctx.request.body.value;

		if (data['quantity'] === value){
			//remove
			await knex('accessory_inventory')
				.where('id', id)
 		  	.del()
		} else {
			//update
			await knex('accessory_inventory')
				.where('id', id)
 		  	.update({
 		  		quantity: parseInt(data['quantity']) - value
 		  	})
		}

		data['quantity'] = value
		await knex('sold_accessories')
 		  .insert(data)

		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/get-sold-accessories', async (ctx, next) => {
	try {
		
		let result = await knex('sold_accessories')
 		  .select('*')

		ctx.response.status = 200;
		ctx.body = result;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/delete-sold-accessories', async (ctx, next) => {
	try {
		const id = ctx.request.body.id;

		await knex('sold_accessories')
 		  .where('id', id)
 		  .del()

		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

module.exports = router;