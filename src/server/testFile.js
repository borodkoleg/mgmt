const Router = require('koa-router');
const router = new Router();

router.get('/api/123', (ctx, next) => {
	ctx.body = 'Hello World!';
});

module.exports = router;