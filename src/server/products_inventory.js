const Router = require('koa-router');
const router = new Router();
const knex = require('./libs/knexConfig');
const { dayToday, dayTodayWithTime } = require('./libs/date');

router.post('/api/get_products', async (ctx, next) => {
	try {
		let result = await knex.raw(`
			SELECT 
			ps_iy.*,
			cat.name AS category,
			ss_info.ad_type_sold,
			ss_info.seller_name AS seller,
			bs_info.buyer_name AS buyer,
			ss_info.seller_email,
			ss_info.seller_name,
			ss_info.seller_referrel,
			ss_info.seller_email,
			ss_info.seller_tel,
			ss_info.seller_identification,
			bs_info.buyer_email,
			bs_info.buyer_name,
			bs_info.buyer_tel
			FROM products_inventory ps_iy
			LEFT JOIN sellers_info ss_info ON ps_iy.seller_id = ss_info.Id
			LEFT JOIN buyers_info bs_info ON ps_iy.buyer_id = bs_info.Id
			LEFT JOIN categories cat ON ps_iy.category_id = cat.id
			WHERE ps_iy.is_deleted = 0;
		`);

		//console.log(JSON.stringify(result[0], null, 2));
		ctx.response.status = 200;
		ctx.body = result[0];
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/remove_products', async (ctx, next) => {
	try {
		const selectedRows = ctx.request.body.selectedRows;

		//console.log(JSON.stringify(selectedRows, null, 2));

		let arrPromice = [];

    selectedRows.forEach((obj) => {
    	arrPromice.push(
    		knex('products_inventory')
	 		  .where('id', obj.id)
	 	 	  .update({
	 	 	  	'is_deleted': true,
	 	 	  	'date_sold': dayToday
	 	 	  })
    	);

			arrPromice.push(
				knex('buyers_info')
				.select('*')
				.where('id', obj.buyer_id)
				.then((res) => {

					let newObjBuyer = {};
					if (res[0]){
						newObjBuyer = {
							buyer_name: res[0]['buyer_name'],
							buyer_tel: res[0]['buyer_tel'],
							buyer_email: res[0]['buyer_email'],
							phone_id: obj.id
						}
					} else {
							newObjBuyer = {
								buyer_name: '',
								buyer_tel: '',
								buyer_email: '',
								phone_id: obj.id
							}
					}

					return knex('sellers_info')
					.select('*')
					.where('id', obj.seller_id)
					.then((res) => {
						let newObjSeller = {};

						if (res[0]){
							newObjSeller = {
								seller_name: res[0]['seller_name'],
								seller_tel: res[0]['seller_tel'],
								seller_email: res[0]['seller_email'],
								seller_referrel: res[0]['seller_referrel']
							}
						} else {
							newObjSeller = {
								seller_name: '',
								seller_tel: '',
								seller_email: '',
								seller_referrel: ''
							}
						}

						let resObject = Object.assign(newObjBuyer, newObjSeller);

						return knex('sold_phones')
						.insert(resObject).then(() => {
							return null;
						});
					})
				})
			);

    });
			//console.log(JSON.stringify(allData[0][0], null, 2));

    await Promise.all(arrPromice);

		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/set-phone-data', async (ctx, next) => {
	try {
		const data = ctx.request.body.data;
		const phoneId = ctx.request.body.phoneId;

		function vZero(data){
			if (data == null){
				return data;
			}
			return data.length > 0 ? data : null;
		}
		
		let dataForProducts = {
		  "repair_cost": vZero(data.repair_cost),
		  "buying_price": vZero(data.buying_price),
		  "total_buying_price": vZero(data.total_buying_price),
		  "product_status": data.product_status,
		  "selling_price": vZero(data.selling_price),
		  "screen_protector": data.screen_protector,
		  "headphone": data.headphone,
		  "total_upsell": vZero(data.total_upsell),
		  "total_profit": vZero(data.total_profit),
		  "notes": data.notes
		};

		await knex('products_inventory')
	  .where('id', phoneId)
	  .update(dataForProducts);

	  //console.log(JSON.stringify(dataForProducts, null, 2));

		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		if (err.code === 'WARN_DATA_TRUNCATED') {
			ctx.response.status = 200;
			ctx.body = false;
			return;
		}

		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/get-phone-data', async (ctx, next) => {
	try {
		const phoneId = ctx.request.body.phoneId;

		const pr = await knex('products_inventory')
	  .where('id', phoneId)
	  .select('*');

	  let objRes = Object.assign(pr[0], {});

		ctx.response.status = 200;
		ctx.body = objRes;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/save-form-screen-1', async (ctx, next) => {
	try {
		const data = ctx.request.body.data;

		let parameters = [
			'id',
			'imei',
			'brand',
			'color',
			'warranty',
			'case',
			'product_code',
			'model',
			'carrier',
			'condition',
			'category_id',
			'gb'
		]

		let phone = {}
		let resultId = null;
		parameters.forEach((el) => {
			phone[el] = data[el]
		})

		if (phone.id){
			resultId = phone.id
			//update
			//phone.device_updated_date = dayTodayWithTime
			await knex('products_inventory')
			.where('id', phone.id)
			.update(phone)
		} else {
			//insert
			let isSameImei = await knex('products_inventory')
			.select('*')
			.where({
				imei: phone.imei,
				is_deleted: 0
			})

			if (isSameImei.length > 0) {
				phone.id = isSameImei[0].id
				resultId = phone.id
				
				//phone.device_updated_date = dayTodayWithTime
				//update
				await knex('products_inventory')
				 .where('id', isSameImei[0].id)
				 .update(phone)
			} else {
				phone.purchase_date = dayTodayWithTime
			  phone.device_updated_date = dayTodayWithTime
			  await knex('products_inventory')
			   	.insert(phone).then((res)=> {
			   		resultId = res[0];
			   	})
			}
		}

		ctx.response.status = 200;
		ctx.body = resultId;
	} catch(err){
		// if (err.code === 'WARN_DATA_TRUNCATED') {
		// 	ctx.response.status = 200;
		// 	ctx.body = false;
		// 	return;
		// }

		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/save-form-screen-2', async (ctx, next) => {
	try {
		const data = ctx.request.body.data;
		const phoneId = ctx.request.body.phoneId;
		const buyingPrice = ctx.request.body.buyingPrice;
		//console.log(JSON.stringify(data));
		let sellerId;
		
		let sellerArr = await knex('sellers_info')
			.where('seller_identification', data['seller_identification'])
			.select('id')

		if (sellerArr.length > 0) {
			sellerId = sellerArr[0]['id']

		} else {
			let insertResultArray = await knex('sellers_info')
				.insert(data);
			sellerId = insertResultArray[0];
		}

		await knex('products_inventory')
				.where('id', phoneId)
				.update({
					seller_id: sellerId,
					buying_price: buyingPrice
				})

		// console.log(JSON.stringify(id));	

		ctx.response.status = 200;
		ctx.body = phoneId;	

	} catch(err){
		if (err.code === 'ER_DUP_ENTRY'){
			ctx.response.status = 200;	
			ctx.body = false;
			return;
		}
		console.log(err);
		ctx.response.status = 400;
	}
});

router.post('/api/save-form-screen-3', async (ctx, next) => {
	try {
		const data = ctx.request.body.data;
		
		await knex('products_inventory')
			.where('id', data['id'])
			.update(data)

		ctx.response.status = 200;
		ctx.body = data['id'];	

	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/phone-update', async (ctx, next) => {
	try {
		const data = ctx.request.body.data;
		const id = ctx.request.body.id;
		
		await knex('products_inventory')
			.where('id', id)
			.update(data)

		ctx.response.status = 200;
		ctx.body = true;	

	} catch(err){
		if (err.code === 'ER_DUP_ENTRY'){
			ctx.response.status = 200;
			ctx.body = false;
			return;
		}
		console.log(err);
		ctx.response.status = 400;
	}
});

//===========================================

// router.post('/api/get-products-by-date', async (ctx, next) => {
// 	try {
// 		let date = ctx.request.body.date;
// 		date = date.substring(0, 10);
// 		console.log(date);
// 		//const date = '2019-09-20';
// 		const date_start = `${date} 0:0:0`;
// 		const date_end =`${date} 23:59:59`;

// 		let result = await knex.raw(`
// 			SELECT 
// 			ps_iy.imei,
// 			ps_iy.device_updated_date
// 			FROM products_inventory ps_iy
// 			WHERE ps_iy.is_deleted = 0 AND
// 			ps_iy.device_updated_date > DATE(?)
// 			AND ps_iy.device_updated_date < DATE(?)
// 		`,[date_start, date_end]);

// 		ctx.response.status = 200;
// 		ctx.body = result[0];
// 	} catch(err){
// 		console.log(err);
// 		ctx.response.status = 400;
// 		ctx.body = false;
// 	}
// });

module.exports = router;