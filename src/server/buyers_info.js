const Router = require('koa-router');
const router = new Router();
const knex = require('./libs/knexConfig');

router.post('/api/get-all-buyers', async (ctx, next) => {
	try {
		let result = await knex.raw(`
			SELECT 
			bb.*,
			cat.name AS category 
			FROM buyers_info bb
			LEFT JOIN categories cat ON bb.category_id = cat.Id;
		`);

		ctx.response.status = 200;
		ctx.body = result[0];
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/add-buyer', async (ctx, next) => {
	try {
		const params = ctx.request.body.data;

		let result = await knex('buyers_info').insert(params);
		
		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		if (err.code === 'ER_DUP_ENTRY'){
			ctx.body = false;
			ctx.response.status = 200;
		} else {
			console.log(err);
			ctx.response.status = 400;
		}
	}
});

router.post('/api/set-buyer', async (ctx, next) => {
	try {
		const phonesArray = ctx.request.body.phonesArray;
		const buyerId = ctx.request.body.buyerId;

		let arrPromice = [];

		phonesArray.forEach((obj) => {
			arrPromice.push(
    		knex('products_inventory')
	 		  .where('id', obj.id)
	 	 	  .update({
	 	 	  	'buyer_id': buyerId
	 	 	  })
    	);
		});

		await Promise.all(arrPromice);

		//console.log(JSON.stringify(buyerId, null, 2));
		
		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
			console.log(err);
			ctx.response.status = 400;
	}
});

router.post('/api/delete-buyer', async (ctx, next) => {
	try {
		const id = ctx.request.body.id;

		await knex('buyers_info')
			.where('id', id)
			.del();
		
		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
	}
});

router.post('/api/buyer-edit', async (ctx, next) => {
	try {
		const data = ctx.request.body.data;
		const id = ctx.request.body.id;

		await knex('buyers_info')
			.where('id', id)
			.update(data)
		
		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		if (err.code === 'ER_DUP_ENTRY'){
			ctx.response.status = 200;
			ctx.body = false;
			return;
		}
		console.log(err);
		ctx.response.status = 400;
	}
});

module.exports = router;