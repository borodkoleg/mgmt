const Router = require('koa-router');
const router = new Router();
const knex = require('./libs/knexConfig');

router.post('/api/get_settings', async (ctx, next) => {
	try {
		let result = await knex.select('*').from('settings');

		//console.log(JSON.stringify(result, null, 2));

		ctx.response.status = 200;
		ctx.body = result;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/set_settings', async (ctx, next) => {
	try {
		const date = ctx.request.body.date;

		let getSettings = await knex('settings').select('*');

		if (getSettings.length > 0){
			//update
			await knex('settings')
				.where('id', 1)
	 	 	  .update({
 	 	 	  	last_update: date
 	 	 	  })
		} else {
			//insert
			await knex('settings')
				.insert({
					id: 1,
					last_update: date
				});
		}

		let result = await knex('settings').select('*');

		ctx.response.status = 200;
		ctx.body = result;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

module.exports = router;