const Router = require('koa-router');
const router = new Router();
const knex = require('./libs/knexConfig');

router.post('/api/get-all-sellers', async (ctx, next) => {
	try {
		let result = await knex.raw(`
			SELECT 
			sel.*,
			cat.name AS category 
			FROM sellers_info sel
			LEFT JOIN categories cat ON sel.category_id = cat.Id;
		`);

		ctx.response.status = 200;
		ctx.body = result[0];
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/add-seller', async (ctx, next) => {
	try {
		const params = ctx.request.body.data;

		let result = await knex('sellers_info').insert(params);
		
		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		if (err.code === 'ER_DUP_ENTRY'){
			ctx.body = false;
			ctx.response.status = 200;
		} else {
			console.log(err);
			ctx.response.status = 400;
		}
	}
});

router.post('/api/set-seller', async (ctx, next) => {
	try {
		const phonesArray = ctx.request.body.phonesArray;
		const sellerId = ctx.request.body.sellerId;

		let arrPromice = [];

		phonesArray.forEach((obj) => {
			arrPromice.push(
    		knex('products_inventory')
	 		  .where('id', obj.id)
	 	 	  .update({
	 	 	  	'seller_id': sellerId
	 	 	  })
    	);
		});

		await Promise.all(arrPromice);

		//console.log(JSON.stringify(params, null, 2));
		
		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
			console.log(err);
			ctx.response.status = 400;
	}
});

router.post('/api/delete-seller', async (ctx, next) => {
	try {
		const id = ctx.request.body.id;

		await knex('sellers_info')
			.where('id', id)
			.del();
		
		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
	}
});

router.post('/api/seller-edit', async (ctx, next) => {
	try {
		const data = ctx.request.body.data;
		const id = ctx.request.body.id;

		await knex('sellers_info')
			.where('id', id)
			.update(data)
		
		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		if (err.code === 'ER_DUP_ENTRY'){
			ctx.response.status = 200;
			ctx.body = false;
			return;
		}
		console.log(err);
		ctx.response.status = 400;
	}
});

module.exports = router;