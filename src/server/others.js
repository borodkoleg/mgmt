require('dotenv').config();
const Router = require('koa-router');
const router = new Router();
const knex = require('./libs/knexConfig');

router.post('/api/get-all-others', async (ctx, next) => {
	try {
		
		let result = await knex.raw(`
			SELECT 
			oth.*,
			cat.name AS category 
			FROM others oth
			LEFT JOIN categories cat ON oth.category_id = cat.Id
			WHERE oth.is_sold = false;
		`);

		ctx.response.status = 200;
		ctx.body = result[0];
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/get-sold-others', async (ctx, next) => {
	try {

		let result = await knex.raw(`
			SELECT 
			oth.*,
			cat.name AS category 
			FROM others oth
			LEFT JOIN categories cat ON oth.category_id = cat.Id
			WHERE oth.is_sold = true;
		`);

		ctx.response.status = 200;
		ctx.body = result[0];
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/add-other', async (ctx, next) => {
	try {
		const data = ctx.request.body.data;
		if (!data.category_id) {
			data.category_id = null
		}
		
		let result = await knex('others').insert(data);

		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		if (err.code === 'ER_DUP_ENTRY'){
			ctx.response.status = 200;
			ctx.body = false;
			return;
		}
		console.log(err);
		ctx.response.status = 400;
	}
});

router.post('/api/delete-sold-other', async (ctx, next) => {
	try {
		const id = ctx.request.body.id;

		await knex('others')
			.where('id', id)
			.del();
		
		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
	}
});

router.post('/api/other-edit', async (ctx, next) => {
	try {
		const data = ctx.request.body.data;
		const id = ctx.request.body.id;

		await knex('others')
			.where('id', id)
			.update(data)
		
		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		if (err.code === 'ER_DUP_ENTRY'){
			ctx.response.status = 200;
			ctx.body = false;
			return;
		}
		console.log(err);
		ctx.response.status = 400;
	}
});

router.post('/api/sold-other', async (ctx, next) => {
	try {
		const id = ctx.request.body.id;

		await knex('others')
		.update('is_sold', true)
		.where('id', id)
		
		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
	}
});

module.exports = router;