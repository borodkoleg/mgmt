require('dotenv').config();
const Router = require('koa-router');
const router = new Router();
const knex = require('./libs/knexConfig');
const {comparePassword} = require('./libs/bcrypt');
const {jwtEncode, jwtDecode, jwtEncodeNoodles, jwtDecodeLocalStor} = require('./libs/jwt');
const jwt = require('jsonwebtoken');

router.post('/api/user_check', async (ctx) => {
	const params = ctx.request.body;
	
	let hash = await knex('users').where({
	  login: params.login
	}).select('password', 'id');

	let correctPassword = await comparePassword(params.password, hash[0].password);

	if (correctPassword){
		let token = await jwtEncode({id: hash[0].id});
		let tokenNoodles = await jwtEncodeNoodles({id: hash[0].id});
		ctx.cookies.set('token', token, { 
			httpOnly: true
		});
		
		ctx.response.body = tokenNoodles;
		ctx.response.status = 200;
		return;
	} 
	
	ctx.cookies.set('token', false);
	ctx.response.body = {res: false};
	ctx.response.status = 500;
});

router.post('/api/user_logout', async (ctx) => {
	ctx.cookies.set('token', false);

	ctx.response.body = true;
	ctx.response.status = 200;
});

module.exports = router;