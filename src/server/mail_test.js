const Router = require('koa-router');
const router = new Router();
const { sendMail } = require('./libs/nodemailer');

router.post('/api/mail_test', async (ctx, next) => {
	await sendMail('borodkoleg@gmail.com', 'subject', 'text', '<h1>html</h1>');

	ctx.body = 'Hello World!';
});

module.exports = router;