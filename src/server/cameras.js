require('dotenv').config();
const Router = require('koa-router');
const router = new Router();
const knex = require('./libs/knexConfig');

router.post('/api/get-all-cameras', async (ctx, next) => {
	try {

		let result = await knex.raw(`
			SELECT 
			cam.*,
			cat.name AS category 
			FROM cameras cam
			LEFT JOIN categories cat ON cam.category_id = cat.Id
			WHERE cam.is_sold = false;
		`);

		ctx.response.status = 200;
		ctx.body = result[0];
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/get-sold-cameras', async (ctx, next) => {
	try {

		let result = await knex.raw(`
			SELECT 
			cam.*,
			cat.name AS category 
			FROM cameras cam
			LEFT JOIN categories cat ON cam.category_id = cat.Id
			WHERE cam.is_sold = true;
		`);

		ctx.response.status = 200;
		ctx.body = result[0];
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/add-camera', async (ctx, next) => {
	try {
		const data = ctx.request.body.data;
		if (!data.category_id) {
			data.category_id = null
		}
		if (!data.shutter_count) {
			data.shutter_count = null
		}
		
		let result = await knex('cameras').insert(data);

		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		if (err.code === 'ER_DUP_ENTRY'){
			ctx.response.status = 200;
			ctx.body = false;
			return;
		}
		console.log(err);
		ctx.response.status = 400;
	}
});

router.post('/api/delete-sold-camera', async (ctx, next) => {
	try {
		const id = ctx.request.body.id;

		await knex('cameras')
			.where('id', id)
			.del();
		
		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
	}
});

router.post('/api/camera-edit', async (ctx, next) => {
	try {
		const data = ctx.request.body.data;
		const id = ctx.request.body.id;

		await knex('cameras')
			.where('id', id)
			.update(data)
		
		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		if (err.code === 'ER_DUP_ENTRY'){
			ctx.response.status = 200;
			ctx.body = false;
			return;
		}
		console.log(err);
		ctx.response.status = 400;
	}
});

router.post('/api/sold-camera', async (ctx, next) => {
	try {
		const id = ctx.request.body.id;

		await knex('cameras')
		.update('is_sold', true)
		.where('id', id)
		
		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
	}
});

module.exports = router;