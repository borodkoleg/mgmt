const Router = require('koa-router');
const router = new Router();
const knex = require('./libs/knexConfig');

router.post('/api/get-all-accessories', async (ctx, next) => {
	try {
		let result = await knex.raw(`
			SELECT 
			ac.*,
			cat.name AS category 
			FROM accessory_inventory ac
			LEFT JOIN categories cat ON ac.category_id = cat.Id;
		`);

		ctx.response.status = 200;
		ctx.body = result[0];
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/add-accessories', async (ctx, next) => {
	try {
		const params = ctx.request.body.data;

		let result = await knex('accessory_inventory').insert(params);
		
		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		if (err.code === 'ER_DUP_ENTRY'){
			ctx.body = false;
			ctx.response.status = 200;
			return;
		} 

		if (err.code === 'WARN_DATA_TRUNCATED'){
			ctx.body = false;
			ctx.response.status = 200;
			return;
		}
		
		console.log(err);
		ctx.response.status = 400;
		
	}
});

router.post('/api/accessories-edit', async (ctx, next) => {
	try {
		const data = ctx.request.body.data;
		const id = ctx.request.body.id;

		await knex('accessory_inventory')
			.where('id', id)
			.update(data)
		
		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		if (err.code === 'ER_DUP_ENTRY'){
			ctx.response.status = 200;
			ctx.body = false;
			return;
		}
		console.log(err);
		ctx.response.status = 400;
	}
});

module.exports = router;