const Router = require('koa-router');
const router = new Router();
const knex = require('./libs/knexConfig');

router.post('/api/get_sold_phones', async (ctx, next) => {
	try {
		let result = await knex.raw(`
			SELECT 
	    ps_iy.*,
	    sold_p.id AS id,
	    cat.name AS category,
	    sold_p.seller_email,
	    sold_p.seller_name,
	    sold_p.seller_referrel,
	    sold_p.seller_tel,
	    sold_p.buyer_email,
	    sold_p.buyer_name,
	    sold_p.buyer_tel
	    FROM sold_phones sold_p
	    LEFT JOIN products_inventory ps_iy ON sold_p.phone_id = ps_iy.Id
	    LEFT JOIN categories cat ON ps_iy.category_id = cat.id
	    WHERE ps_iy.is_deleted = 1;
	  `);

		ctx.response.status = 200;
		ctx.body = result[0];
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/delete-sold-phone', async (ctx, next) => {
	try {
		const id = ctx.request.body.id;

		await knex('sold_phones')
 		  .where('id', id)
 	 	  .del()

		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

module.exports = router;