const combineRouters = require('koa-combine-routers')

const testFile = require('./testFile')
const users = require('./users')
const mail_test = require('./mail_test');
const phonecheck = require('./phonecheck');
const products_inventory = require('./products_inventory');
const settings = require('./settings');
const sellers_info = require('./sellers_info');
const buyers_info = require('./buyers_info');
const sold_phones = require('./sold_phones');
const categories = require('./categories');
const accessories = require('./accessories');
const sold_accessories = require('./sold_accessories');
const photos_phone = require('./photos_phone');
const cameras = require('./cameras');
const others = require('./others');

const router = combineRouters(
	testFile,
	users,
	phonecheck,
	mail_test,
	products_inventory,
	settings,
	sellers_info,
	buyers_info,
	sold_phones,
	categories,
	accessories,
	sold_accessories,
	photos_phone,
	cameras,
	others
)

module.exports = router;