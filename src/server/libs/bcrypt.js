require('dotenv').config();
const bcrypt = require('bcrypt');
const {promisify} = require('util');

exports.cryptPassword = promisify(function(password, callback) {
   bcrypt.genSalt(+process.env.BCRYPT_ROUNDS, function(err, salt) {
    if (err) 
      return callback(err);

    bcrypt.hash(password, salt, function(err, hash) {
      return callback(err, hash);
    });
  });
});

exports.comparePassword = promisify(function(plainPass, hashword, callback) {
   bcrypt.compare(plainPass, hashword, function(err, isPasswordMatch) {
       return err == null ?
           callback(null, isPasswordMatch) :
           callback(err);
   });
});

exports.cryptPasswordSync = function(password) {
	return bcrypt.hashSync(password, +process.env.BCRYPT_ROUNDS);
}
