module.exports.isNumberOrNull = function(value) {
	if (value == null){
		return true;
	}
	const test = value * 1; // +value;
	if (isNaN(test)){
		return false;
	} else {
		if (value[0] === '0' && value[1] && value[1] !== '.'){
			return false;
		}
		const result = Math.sign(test);

		if (result === 1 || result === 0){
			return true;
		}
		return false;
	}
};

module.exports.isNumber = function(value) {
	if (!value){
		return false;
	}
	const test = value * 1; // +value;
	if (isNaN(test)){
		return false;
	} else {
		if (value[0] === '0' && value[1] && value[1] !== '.'){
			return false;
		}
		const result = Math.sign(test);

		if (result === 1 || result === 0){
			return true;
		}
		return false;
	}
};

module.exports.isNumberOrMinusOne = function(value) {
	if (!value){
		return false;
	}
	if (value === '!startData!'){
		return true;
	}
	const test = value * 1; // +value;
	if (isNaN(test)){
		return false;
	} else {
		if (value[0] === '0' && value[1] && value[1] !== '.'){
			return false;
		}
		const result = Math.sign(test);

		if (result === 1 || result === 0){
			return true;
		}
		return false;
	}
};

module.exports.isIntegerOrMinusOne = function(value) {
	if (!value){
		return false;
	}
	if (value === '!startData!'){
		return true;
	}
	const test = value * 1; // +value;
	if (isNaN(test)){
		return false;
	} else {
		if (value[0] === '0' && value[1] && value[1] !== '.'){
			return false;
		}

		if (!Number.isInteger(test)){
			return false;
		}

		const result = Math.sign(test);

		if (result === 1 || result === 0){
			return true;
		}
		return false;
	}
};

module.exports.isIntegerPositiveOrNull = function(value) {
	if (value === ''){
		return true;
	}
	if (value == null){
		return true;
	}
	const test = value * 1; // +value;

	if (test <= 0){
		return false;	
	}

	return (typeof test === 'number') && (test % 1 === 0);
};

module.exports.isIntegerPositiveOrMinusOne = function(value) {
	if (value === ''){
		return true;
	}
	if (value == null){
		return true;
	}
	if (value === '!startData!'){
		return true;
	}
	const test = value * 1; // +value;

	if (test <= 0){
		return false;	
	}

	return (typeof test === 'number') && (test % 1 === 0);
};