require('dotenv').config();
const jwt = require('jsonwebtoken');

exports.jwtEncodeNoodles = function(data){ 
	return new Promise(function(resolve) {
		jwt.sign(
			data, 
			process.env.REACT_APP_JWT_SECRET_LOCAL_STOR,
			{ expiresIn: process.env.REACT_APP_JWT_TIME },
			function(err, token) {
			if (!err){
	   		resolve(token);
	   	} else {
	   		resolve(false);
	   	}
		});
	});
};

exports.jwtEncode = function(data){ 
	return new Promise(function(resolve) {
		jwt.sign(
			data, 
			process.env.REACT_APP_JWT_SECRET,
			{ expiresIn: process.env.REACT_APP_JWT_TIME },
			function(err, token) {
			if (!err){
	   		resolve(token);
	   	} else {
	   		resolve(false);
	   	}
		});
	});
};

exports.jwtDecode = function(token){
	return new Promise(function(resolve) {
		jwt.verify(token, process.env.REACT_APP_JWT_SECRET, function(err, decoded) {
			//console.log(err);
	  	if (!err){
	  		resolve(decoded);
	  	} else {
	  		resolve(false);
	  	}
		});
	})
};

exports.jwtDecodeLocalStor = function(token){
	return new Promise(function(resolve) {
		jwt.verify(token, process.env.REACT_APP_JWT_SECRET_LOCAL_STOR, function(err, decoded) {
			console.log(err);
	  	if (!err){
	  		resolve(decoded);
	  	} else {
	  		resolve(false);
	  	}
		});
	})
};