const moment = require('moment');

//'YYYY-MM-DD HH:mm:ss' 'YYYY-MM-DD'
const dateTimeFormat = (value, formatTemplateStart, formatTemplateEnd) => {
	try {
		if (moment(value, formatTemplateStart).format(formatTemplateEnd) === 'Invalid date') {
			return value;
		} else {
			return moment(value, formatTemplateStart).format(formatTemplateEnd)
		}
	} catch (e) {
		return value;
	}
};

const isDateValid = (value, formatTemplateStart, formatTemplateEnd) => {
  try {
    if (!value) {
      return false;
    }
    if (value.length !== 10) {
      return false;
    }
    if (moment(value, formatTemplateStart).format(formatTemplateEnd) === 'Invalid date') {
      return false;
    }
    return true;
  } catch (e) {
    return false;
  }
};

const dayToday = moment().format('YYYY-MM-DD');

const dayTodayWithTime = moment().format('YYYY-MM-DD HH:mm:ss');

function dayTodayMinusOne(){
	let dayToday = new Date();
	let result = dayToday.setDate(dayToday.getDate() - 1);
	return moment(result).format('YYYY-MM-DD');
}

function getDatesBetween(startDate, stopDate) {
	let start = new Date(startDate);
	let end = new Date(stopDate);
	let newend = end.setDate(end.getDate()+1);
	let endNew = new Date(newend);
	let result = [];
	while(start < endNew){
	   //console.log(new Date(start).getTime() / 1000); // unix timestamp format
	   //console.log(start); // ISO Date format          
	   result.push(moment(start).format('YYYY-MM-DD'));
	   let newDate = start.setDate(start.getDate() + 1);
	   start = new Date(newDate);
	}

	return result;
}

// function getDatesBetween(startDate, stopDateN) {
//     let dateArray = [];
//     let currentDate = moment(startDate);
//     let stopDate = moment(stopDateN);
//     while (currentDate <= stopDate) {
//         dateArray.push( moment(currentDate).format('YYYY-MM-DD') )
//         currentDate = moment(currentDate).add(1, 'days');
//     }
//     console.log(currentDate);
//     console.log(stopDate);
//     return dateArray;
// }

function compareDate(dateA, dateB) {
    let momentA = moment(dateA).format('YYYY-MM-DD');
    let momentB = moment(dateB).format('YYYY-MM-DD');
    if (momentA > momentB) return true;
    if (momentA < momentB) return false;
    return false;
}


exports.dateTimeFormat = dateTimeFormat;
exports.dayToday = dayToday;
exports.isDateValid = isDateValid;
exports.getDatesBetween = getDatesBetween;
exports.compareDate = compareDate;
exports.dayTodayMinusOne = dayTodayMinusOne;
exports.dayTodayWithTime = dayTodayWithTime;