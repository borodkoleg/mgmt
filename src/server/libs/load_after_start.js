require('dotenv').config();
const Axios = require('axios');
const knex = require('./knexConfig');

let getAllDataPromise = new Promise(function(resolve) {
	Axios.post('https://clientapiv2.phonecheck.com/cloud/cloudDB/GetAllDevices' , {
			apiKey: process.env.PHONECHECK_API_KEY,
			username: process.env.PHONECHECK_USERNAME
		},
  	{
      headers: {
          "Content-Type": "multipart/form-data"
      },
      params: {}
  })
  .then((res) => { 
  	resolve(res);
  })
  .catch(err => {
    resolve(false);
  });
});

getAllDataPromise.then((result) => {
	if (!result){
		return;
	}

	let prInventorySave = [];
	// let sellersSave = [];
	// let buyersSave = [];
	let imeiUnique = new Set();

	result['data'].forEach(function(obj) {
		//if imei already exists then don't push
		if (!imeiUnique.has(obj.IMEI)){
			prInventorySave.push({
				purchase_date: obj.DeviceCreatedDate,
				device_updated_date: obj.DeviceUpdatedDate,
				product_code: obj.Serial,
				brand: (obj.Make).toUpperCase(),
				imei: obj.IMEI,
				model: (obj.Model).toUpperCase(),
				color: (obj.Color).toUpperCase(),
				carrier: (obj.Carrier).toUpperCase(),					
				warranty: (obj.Notes).toUpperCase(),
				condition: (obj.Grade).toUpperCase(),
				case: (obj.Cosmetics).toUpperCase()
			});

			imeiUnique.add(obj.IMEI);
		}

		//=======
	});

	if (prInventorySave.lingth === 0){
		return;
	}

	const keys = Object.keys(prInventorySave[0]);
	const values = prInventorySave.map(function(obj){
	  return Object.values(obj);
	});

	const resInsert = dataInsert(keys, values, prInventorySave).then((res) => {
			console.log(res);
	});

});

async function dataInsert(keys, values, prInventorySave){
	try {

			//insert into products_inventory
		await knex.raw(`
			INSERT INTO products_inventory (??) 
			VALUES ?`,[keys, values]);

		//update last_update (date and time)
		await knex.raw('REPLACE INTO settings SET last_update = ?, id = 1', [prInventorySave[0]['device_updated_date']]);

		return true;
	} catch(err){
		return err;
	}

}