import React, {Component} from 'react';
import { Route, Switch } from "react-router-dom";

import Home from './components/Home';
import Login from './components/Login';
import Page404 from './components/Page404';
import Settings from './components/Settings';
import UpsellsAccessories from './components/UpsellsAccessories'
import Sellers from './components/Sellers'
import Buyers from './components/Buyers'

import BasisLayout from './layouts/BasisLayout';
import Basis from './components/view/Basis'

import ProductsCategory from './components/viewAdmin/ProductsCategory'
import AddProduct from './components/viewAdmin/AddProduct'
import EditProduct from './components/viewAdmin/EditProduct'
import ProductsSold from './components/viewAdmin/ProductsSold'

import AllSellers from './components/viewAdmin/AllSellers'
import AddSeller from './components/viewAdmin/AddSeller'
import SellersCategory from './components/viewAdmin/SellersCategory'
import EditSeller from './components/viewAdmin/EditSeller'

import AllBuyers from './components/viewAdmin/AllBuyers'
import AddBuyer from './components/viewAdmin/AddBuyer'
import BuyersCategory from './components/viewAdmin/BuyersCategory'
import EditBuyer from './components/viewAdmin/EditBuyer'

import AllAccessories from './components/viewAdmin/AllAccessories'
import AccessoriesCategory from './components/viewAdmin/AccessoriesCategory'
import AddAccessories from './components/viewAdmin/AddAccessories'
import EditAccessories from './components/viewAdmin/EditAccessories'
import AccessoriesSold from './components/viewAdmin/AccessoriesSold'

import AllCameras from './components/viewAdmin/AllCameras'
import AddCamera from './components/viewAdmin/AddCamera'
import CamerasCategory from './components/viewAdmin/CamerasCategory'
import EditCamera from './components/viewAdmin/EditCamera'
import CamerasSold from './components/viewAdmin/CamerasSold'

import AddOther from './components/viewAdmin/AddOther'
import OthersCategory from './components/viewAdmin/OthersCategory'
import AllOthers from './components/viewAdmin/AllOthers'
import EditOthers from './components/viewAdmin/EditOthers'
import OthersSold from './components/viewAdmin/OthersSold'

import Mgmt from './components/Mgmt';
import { connect } from 'react-redux';
import AdminLayout from './layouts/AdminLayout';

const jwt = require('jsonwebtoken');

const AppRoute = ({ component: Component, layout: Layout, ...rest }) => (
  <Route {...rest} render={props => (
    <Layout history={props.history}>
      <Component {...props} />
    </Layout>
  )} />
);

//====================

class App extends Component {
	state = {
		accessToken: false
  };
  
  UNSAFE_componentWillMount() {
    this.unlisten = this.props.history.listen((location, action) => {
      //on route change
      this.tokenCheck();
    });
  }
  componentWillUnmount() {
    this.unlisten();
  }

  componentDidMount(){
  	this.tokenCheck();
  }

  tokenCheck() {
  	let this_ = this;

  	let token = false;
  	if (localStorage.getItem('token')){
  		token = localStorage.getItem('token');	
  	} else if (this.props.globalState.token){
  		token = this.props.globalState.token;	
  	}

  	const decodedToken = jwt.decode(token, {complete: true});
		const dateNow = new Date();

		if (decodedToken && 
				decodedToken.hasOwnProperty('payload') &&
			 (decodedToken.payload.exp < dateNow.getTime()))
		{
			this_.setState({
      	accessToken: true
    	})
		} else {
			localStorage.removeItem('token');
			this_.setState({
      	accessToken: false
    	})
		}
  }

	render() {
    const {accessToken} = this.state;

	  return (
	    <Switch>
	      <Route exact path="/" component={Home} />
	      <Route path="/login" component={Login} />
	      {accessToken && <AppRoute path="/mgmt" component={Mgmt} layout={AdminLayout}/>}
	      {accessToken && <AppRoute path="/settings" component={Settings} layout={AdminLayout}/>}
	      {accessToken && <AppRoute path="/upsells-accessories" component={UpsellsAccessories} layout={AdminLayout}/>}
	      {accessToken && <AppRoute path="/sellers" component={Sellers} layout={AdminLayout}/>}
	      {accessToken && <AppRoute path="/buyers" component={Buyers} layout={AdminLayout}/>}
	      
	      {accessToken && <AppRoute path="/basis" component={Basis} layout={BasisLayout}/>}
	      
	      {accessToken && <AppRoute path="/all-products" component={Mgmt} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/products-category" component={ProductsCategory} layout={AdminLayout}/>}
				{accessToken && <AppRoute path="/add-product" component={AddProduct} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/all-sellers" component={AllSellers} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/add-seller" component={AddSeller} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/sellers-category" component={SellersCategory} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/edit-seller" component={EditSeller} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/all-buyers" component={AllBuyers} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/add-buyer" component={AddBuyer} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/buyers-category" component={BuyersCategory} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/edit-buyer" component={EditBuyer} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/edit-product" component={EditProduct} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/sold-products" component={ProductsSold} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/all-accessories" component={AllAccessories} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/accessories-category" component={AccessoriesCategory} layout={AdminLayout}/>}
				
				{accessToken && <AppRoute path="/add-accessories" component={AddAccessories} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/edit-accessories" component={EditAccessories} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/sold-accessories" component={AccessoriesSold} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/all-cameras" component={AllCameras} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/add-camera" component={AddCamera} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/cameras-category" component={CamerasCategory} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/edit-camera" component={EditCamera} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/cameras-sold" component={CamerasSold} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/add-other" component={AddOther} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/others-category" component={OthersCategory} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/all-others" component={AllOthers} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/edit-other" component={EditOthers} layout={AdminLayout}/>}

				{accessToken && <AppRoute path="/others-sold" component={OthersSold} layout={AdminLayout}/>}

	      <Route component={Page404} />
	    </Switch>
	  );
	}
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(App);
