import React from 'react';
import ReactDOM from 'react-dom';
import './scss/index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';

import reducers from './store/index.js';
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';

import { Provider } from 'react-redux';
import { Router } from "react-router-dom";
import history from './history'

const store = createStore(reducers, applyMiddleware(thunk))

ReactDOM.render(
	<Provider store={store}> 
	  <Router history={history}>
				<App history={history}/>
		</Router>
	</Provider>,
document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
