import React, { Component } from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import logo from '../img/logo.png';  

import BtnLogout from '../components/view/BtnLogout';

class BasisLayout extends Component {
	state = {
	
  };

  render() {
    return (
    <Grid container>
    	<Grid item xs={12} sm={12} 
	    	style={{
	    		minHeight:'40px', 
	    		padding: '2px 0',
	    		background:'#ecf0f5'
	    }}>
	    	<img 
		  		src={logo} 
		  		className={'logo-basis'}
		  		alt={'logo mgmt'}
				/> 
	      <BtnLogout
	      	history={this.props.history}
	      />
	    </Grid>
	    <Grid item xs={12} sm={12} 
	    style={{
	    	paddingLeft:'4px'
	    }}>
	      {this.props.children}
      </Grid>
    </Grid>
    );
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(BasisLayout);