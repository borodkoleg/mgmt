import React, { Component } from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import logo from '../img/logo.png';  

import MenuMgmt from '../components/MenuMgmt';

class AdminLayout extends Component {

  render() {
    return (
    <Grid container>
    	<Grid item xs={12} sm={2} style={{background: '#fafafb', padding: '0 4px'}}>
    		<img 
		  		src={logo} 
		  		className={'logo-basis'}
		  		alt={'logo mgmt'}
		  		style={{marginTop:'10px'}}
				/>
				<br/>
				<br/>
	      <MenuMgmt history={this.props.history} />
    		{/*
	      <Link to="/mgmt">Product inventory</Link>
	      <br />
	      <br />
	      <Link to="/upsells-accessories">Upsells Accessories</Link>
	      <br />
	      <br />
	      <Link to="/sellers">Sellers</Link>
	      <br />
	      <br />
	      <Link to="/buyers">Buyers</Link>
	      <br />
	      <br />
	      <Link to="/sold_phones">Sold Phones</Link>
	    	*/}
      </Grid>
      <Grid item xs={12} sm={10} style={{padding: '20px 20px 10px'}}>
      	{this.props.children}
      </Grid>
    </Grid>
    );
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(AdminLayout);