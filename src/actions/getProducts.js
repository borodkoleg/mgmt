const Axios = require('axios');

function getProducts(date){
	return dispatch => {
		Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get_products' , {date: date},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result){
      	dispatch({type: 'BA_GET_PRODUCTS', payload: result['data']});
      	dispatch({type: 'BA_GET_PRODUCTS_STATUS', payload: true});
      }
    });
	}
}

exports.getProducts = getProducts;