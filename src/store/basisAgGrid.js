const phoneDefault = {
	"id": null,
  "purchase_date": null,
  "device_updated_date": null,
  "product_code": null,
  "brand": null,
  "imei": null,
  "model": null,
  "color": null,
  "carrier": null,
  "warranty": null,
  "condition": null,
  "case": null,
  "date_sold": null,
  "gb": null,

  "repair_cost": null,
  "repair_place": null,
  "repair_date_sent": null,
  "repair_date_return": null,

  "buying_price": null,
  "selling_price": null,
  "product_status": null,

  "total_buying_price": null,
  "is_deleted": 0,
  "screen_protector": null,
  "headphone": null,
  "total_upsell": null,
  "total_profit": null,
  "notes": null,
  "seller_id": null,
  "buyer_id": null,
  "created_at": null,
  "ad_type_sold": null,
  "seller_email": null,
  "seller_name": null,
  "seller_referrel": null,
  "seller_tel": null,
  "buyer_email": null,
  "buyer_name": null,
  "buyer_tel": null,

  "category_id": null
}

const initialState = {
	selectedRows: [],
	addPhoneManuallyStart: false,
	tabValue: 0,
	tabsShow: false,
	phone: {},
	images: {
		1: null,
		2: null,
		3: null,
		4: null,
		5: null
	}
};

export default function start(state=initialState, action){
	if (action.type === 'BA_PHONE') {
		if (action.payload === false){
			return {
      	...state,
      	phone: {...phoneDefault}
    	}
    }
    
		return {
    	...state,
    	phone: {...action.payload}
  	}
  }

  if (action.type === 'BA_SELECTED_ROWS') {
  	return {
      ...state,
      selectedRows: [...action.payload]
    }
  }

  if (action.type === 'BA_ADD_MANUALLY') {
  	return {
      ...state,
      addPhoneManuallyStart: action.payload
    }
  }

  if (action.type === 'BA_TAB_VALUE') {
  	return {
      ...state,
      tabValue: action.payload
    }
  }

  if (action.type === 'BA_TABS_SHOW') {
  	return {
      ...state,
      tabsShow: action.payload
    }
  }

  if (action.type === 'BASIS_GRID_CHANGE_ALL') {
  	let objRezult = { ...state };
  	let objParams = { ...action.payload };

  	// for (let key in objParams) {
  	// 	objRezult[key] = objParams[key]
  	// }
  	objRezult = Object.assign(objRezult, objParams);
  	
  	//console.log(JSON.stringify(objRezult, null, 2));

  	return objRezult;
  }



  return state;
}