const initialState = {
	categoryName: '',
	seller: {
		seller_name: '',
		seller_email: '',
		seller_tel: '',
		seller_referrel: '',
		seller_identification: '',
		category_id: ''
	}
};

export default function start(state=initialState, action){
	if (action.type === 'CATEGORY_CHANGE') {
  	return {
      ...state,
      categoryName: action.payload
    }
  }

  if (action.type === 'SELLER_CHANGE') {
  	return {
      ...state,
      seller: {...action.payload}
    }
  }

  return state;
}