export const statusPhone = [
	{
		label: '',
		value: ''
	},
	{
		label: 'AVAILABLE',
		value: 'AVAILABLE'
	},
	{
		label: 'SOLD',
		value: 'SOLD'
	},
	{
		label: 'REPAIR',
		value: 'REPAIR'
	},
	{
		label: 'OTHER',
		value: 'OTHER'
	}
]