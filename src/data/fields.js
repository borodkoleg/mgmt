const { dateTimeFormat } = require('../server/libs/date.js');

// function cellStyling(params){   
//    if (params.data.imei == '356564082836268') {
//      return {'background-color': 'blue'};
//     } else {
//      return {'background-color': 'green'};
//    }
// }

export const products_inventory = [
  {
        headerName: "IMEI", //+
        field: "imei",
        sortable:true,
        resizable:true,
        filter: true,
        width: 180,
        checkboxSelection: true
        //editable:true,
            // cellRenderer: params => {
    //     //return `<input type='checkbox' ${params.value ? 'checked' : ''} />`;
    //     return `<div style="height:30px;background:red"></div>`
    // },
    //cellStyle: {backgroundColor: '#BDC3C7', fontSize: '15px', cursor: 'pointer'}
    //cellStyle: cellStyling
  },
  {
        headerName: "Category",
        field: "category",
        sortable:true,
        resizable:true,
        filter: true,
        width: 160
  },
	// {
 //        headerName: "Purchase Date", 
 //        field: "purchase_date",
 //        sortable:true,
 //        resizable:true,
 //        filter: true,
 //        width: 120
 //  },
 //  {
 //        headerName: "Updated Date", 
 //        field: "device_updated_date",
 //        sortable:true,
 //        resizable:true,
 //        filter: true,
 //        width: 120
 //  },
  {
        headerName: "Serial",
        field: "product_code",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  {
        headerName: "Brand", //+
        field: "brand",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  {
        headerName: "Model", //+
        field: "model",
        sortable:true,
        resizable:true,
        filter: true,
				width: 120
  },
  {
        headerName: "Color", //+
        field: "color",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  {
        headerName: "Carrier", //+
        field: "carrier",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  {
        headerName: "Warranty",
        field: "warranty",
        sortable:true,
        resizable:true,
        filter: true,
        width: 70
  },
  {
        headerName: "Condition",
        field: "condition",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  {
  	    headerName: "Packaging",
        field: "case",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
	},
  {
        headerName: "Repair Cost",
        field: "repair_cost",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  {
        headerName: "Gb",
        field: "gb",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  {
        headerName: "Buying Price",
        field: "buying_price",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  // {
  //       headerName: "Total Buying Price",
  //       field: "total_buying_price",
  //       sortable:true,
  //       resizable:true,
  //       filter: true,
  //       width: 120
  // },
  {
        headerName: "Product Status",
        field: "product_status",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  {
        headerName: "Selling price",
        field: "selling_price",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  {
        headerName: "Seller",
        field: "seller",
        sortable:true,
        resizable:true,
        filter: true,
        width: 100
  },	
  {
        headerName: "Buyer",
        field: "buyer",
        sortable:true,
        resizable:true,
        filter: true,
        width: 100
  }
]

export const sellers_info = [
  // {
  //       headerName: "id", //+
  //       field: "id",
  //       sortable:true,
  //       resizable:true,
  //       filter: true,
  //       width: 70
  // },
  {
        headerName: "Category",
        field: "category",
        sortable:true,
        resizable:true,
        filter: true,
        width: 160
  },
  {
        headerName: "Name",
        field: "seller_name",
        sortable:true,
        resizable:true,
        filter: true,
        width: 160
  },
  {
        headerName: "Tel",
        field: "seller_tel",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  {
        headerName: "Email",
        field: "seller_email",
        sortable:true,
        resizable:true,
        filter: true,
        width: 160
  },
  {
        headerName: "Referrel",
        field: "seller_referrel",
        sortable:true,
        resizable:true,
        filter: true,
        width: 160
  },
  {
        headerName: "Identification",
        field: "seller_identification",
        sortable:true,
        resizable:true,
        filter: true,
        width: 160
  }
  // {
  //       headerName: "Ad Type Sold",
  //       field: "ad_type_sold",
  //       sortable:true,
  //       resizable:true,
  //       filter: true,
  //       width: 160
  // }
]

export const sellers_info_dialog = [
  {
        headerName: "id", //+
        field: "id",
        sortable:true,
        resizable:true,
        filter: true,
        width: 70
  },
  {
        headerName: "Seller Name",
        field: "seller_name",
        sortable:true,
        resizable:true,
        filter: true,
        width: 160
  },
  {
        headerName: "Seller Email",
        field: "seller_email",
        sortable:true,
        resizable:true,
        filter: true,
        width: 160
  },
  {
        headerName: "Seller Tel",
        field: "seller_tel",
        sortable:true,
        resizable:true,
        filter: true,
        width: 140
  },
]

export const buyers_info = [
	// {
 //        headerName: "id", //+
 //        field: "id",
 //        sortable:true,
 //        resizable:true,
 //        filter: true,
 //        width: 70
 //  },
  {
        headerName: "Category",
        field: "category",
        sortable:true,
        resizable:true,
        filter: true,
        width: 160
  },
	{
        headerName: "Buyer Name",
        field: "buyer_name",
        sortable:true,
        resizable:true,
        filter: true,
        width: 160
  },
  {
        headerName: "Buyer Tel",
        field: "buyer_tel",
        sortable:true,
        resizable:true,
        filter: true,
        width: 160
  },
  {
        headerName: "Buyer Email",
        field: "buyer_email",
        sortable:true,
        resizable:true,
        filter: true,
        width: 160
  }
]

export const upsells_accessories = [
	{
        headerName: "IMEI", //+
        field: "imei",
        sortable:true,
        resizable:true,
        filter: true,
        width: 180
  },
	{
        headerName: "Screen Protector",
        field: "screen_protector",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  {
        headerName: "Headphone",
        field: "headphone",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  {
        headerName: "Total Upsell",
        field: "total_upsell",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  {
        headerName: "Total Profit",
        field: "total_profit",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  {
        headerName: "Notes",
        field: "notes",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  }
]

//==========================================
export const sold_phones = [
	// {
 //        headerName: "id", //+
 //        field: "id",
 //        sortable:true,
 //        resizable:true,
 //        filter: true,
 //        width: 70
 //  },
  {
        headerName: "Category",
        field: "category",
        sortable:true,
        resizable:true,
        filter: true,
        width: 180
  },
  {
        headerName: "IMEI",
        field: "imei",
        sortable:true,
        resizable:true,
        filter: true,
        width: 180
  },
	// {
 //        headerName: "Purchase Date", 
 //        field: "purchase_date",
 //        sortable:true,
 //        resizable:true,
 //        filter: true,
 //        width: 120
 //  },
 //  {
 //        headerName: "Updated Date", 
 //        field: "device_updated_date",
 //        sortable:true,
 //        resizable:true,
 //        filter: true,
 //        width: 120
 //  },
  {
        headerName: "Serial",
        field: "product_code",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  {
        headerName: "Brand",
        field: "brand",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  {
        headerName: "Model",
        field: "model",
        sortable:true,
        resizable:true,
        filter: true,
				width: 120
  },
  {
        headerName: "Color",
        field: "color",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  {
        headerName: "Carrier",
        field: "carrier",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  {
        headerName: "Warranty",
        field: "warranty",
        sortable:true,
        resizable:true,
        filter: true,
        width: 70
  },
  {
        headerName: "Condition",
        field: "condition",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  {
  	    headerName: "Packaging",
        field: "case",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
	},
  // {
  //       headerName: "Repair Cost",
  //       field: "repair_cost",
  //       sortable:true,
  //       resizable:true,
  //       filter: true,
  //       width: 120
  // },
  {
        headerName: "Buying Price",
        field: "buying_price",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  // {
  //       headerName: "Total Buying Price",
  //       field: "total_buying_price",
  //       sortable:true,
  //       resizable:true,
  //       filter: true,
  //       width: 120
  // },
  // {
  //       headerName: "Product Status",
  //       field: "product_status",
  //       sortable:true,
  //       resizable:true,
  //       filter: true,
  //       width: 120
  // },
  {
        headerName: "Selling price",
        field: "selling_price",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120
  },
  {
        headerName: "Date Sold",
        field: "date_sold",
        sortable:true,
        resizable:true,
        filter: true,
        width: 120,
        cellRenderer: params => {
        	return dateTimeFormat(params.value, 'YYYY-MM-DD HH:mm:ss', 'MM-DD-YYYY HH:mm:ss');
        }
  },
  	{
        headerName: "Buyer Name",
        field: "buyer_name",
        sortable:true,
        resizable:true,
        filter: true,
        width: 160
  },
  {
        headerName: "Buyer Tel",
        field: "buyer_tel",
        sortable:true,
        resizable:true,
        filter: true,
        width: 160
  },
  {
        headerName: "Buyer Email",
        field: "buyer_email",
        sortable:true,
        resizable:true,
        filter: true,
        width: 160
  },
  {
        headerName: "Seller Name",
        field: "seller_name",
        sortable:true,
        resizable:true,
        filter: true,
        width: 160
  },
  {
        headerName: "Seller Email",
        field: "seller_email",
        sortable:true,
        resizable:true,
        filter: true,
        width: 160
  },
  {
        headerName: "Seller Tel",
        field: "seller_tel",
        sortable:true,
        resizable:true,
        filter: true,
        width: 140
  }
]

export const basis_phones = [
  {
        headerName: "IMEI", //+
        field: "imei",
        filter: true,
        width: 154
        //editable:true,
            // cellRenderer: params => {
    //     //return `<input type='checkbox' ${params.value ? 'checked' : ''} />`;
    //     return `<div style="height:30px;background:red"></div>`
    // },
    //cellStyle: {backgroundColor: '#BDC3C7', fontSize: '15px', cursor: 'pointer'}
    //cellStyle: cellStyling
  },
  {
        headerName: "Scan date", 
        field: "device_updated_date",
        filter: true,
        width: 170,
        cellRenderer: params => {
        	return dateTimeFormat(params.value, 'YYYY-MM-DD HH:mm:ss', 'MM-DD-YYYY HH:mm:ss');
        }
  }
]

export const categories = [
  {
        headerName: "Name",
        field: "name",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  }
]

export const accessories = [
	{
        headerName: "Category",
        field: "category",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Code",
        field: "product_code",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Name",
        field: "product_name",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Brand",
        field: "brand",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Model",
        field: "model",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Quantity",
        field: "quantity",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Buying Price",
        field: "buying_price",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Selling Price",
        field: "selling_price",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  }
]

export const sold_accessories = [
  {
        headerName: "Category",
        field: "category",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Code",
        field: "product_code",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Name",
        field: "product_name",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Brand",
        field: "brand",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Model",
        field: "model",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Quantity",
        field: "quantity",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Buying Price",
        field: "buying_price",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Selling Price",
        field: "selling_price",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Data",
        field: "created_at",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190,
        cellRenderer: params => {
        	return dateTimeFormat(params.value, 'YYYY-MM-DD HH:mm:ss', 'MM-DD-YYYY HH:mm:ss');
        }
  }
]

export const cameras_info = [
	{
        headerName: "Category",
        field: "category",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "SKU",
        field: "sku",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Brand",
        field: "brand",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Model",
        field: "model",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Serial Number",
        field: "serial_number",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Shutter Count",
        field: "shutter_count",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Condition",
        field: "condition",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Warranty",
        field: "warranty",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Accessories",
        field: "accessories",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  }
]

export const others_info = [
	{
        headerName: "Category",
        field: "category",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "SKU",
        field: "sku",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Brand",
        field: "brand",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Model",
        field: "model",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Serial Number",
        field: "serial_number",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Condition",
        field: "condition",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Warranty",
        field: "warranty",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  },
  {
        headerName: "Accessories",
        field: "accessories",
        sortable:true,
        resizable:true,
        filter: true,
        width: 190
  }
]