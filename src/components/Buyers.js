import React, {Component} from 'react';
import { connect } from 'react-redux';

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { buyers_info } from '../data/fields'

const Axios = require('axios');

class Buyers extends Component {
  state = {
    rowData: []
  };
  
  componentDidMount() {
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-all-buyers' , {},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result){
      	this.setState({
      		rowData: [ ...result['data'] ]
      	})
      }
    });
  }

  render() {
    const {rowData} = this.state;

    return (
    	<div 
			className="ag-theme-balham"
    	style={{ 
        height: '500px', 
        width: '100%' }} >
    		<AgGridReact
          columnDefs={buyers_info}
          rowData={rowData}>
        </AgGridReact>
    	</div>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(Buyers);