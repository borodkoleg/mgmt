import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';

const Axios = require('axios');

class MenuMgmt extends Component {
  state = {
    productsMenu: false,
    sellersMenu: false,
    buyersMenu: false,
    accessoriesMenu: false,
    camerasMenu: false,
    othersMenu: false
  };

  LogoutClick = () => {
		let this_ = this;
		Axios.post(process.env.REACT_APP_HOST_PORT + '/api/user_logout' , {},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
    })
    .catch((err) => {
    	console.log(err);
    })
    .finally(function () {
    	localStorage.removeItem('token');
    	this_.props.history.push('/login');
    })
	}
  
  componentDidMount() {
  }

  displayChange = (data) => {
  	if (data === 'products'){
  		this.setState(prevState => ({
			  productsMenu: !prevState.productsMenu,
			  camerasMenu: false,
			  sellersMenu: false,
			  buyersMenu: false,
    		accessoriesMenu: false,
    		othersMenu: false
			}));
  	}

  	if (data === 'sellers'){
  		this.setState(prevState => ({
			  sellersMenu: !prevState.sellersMenu,
			  productsMenu: false,
			  camerasMenu: false,
			  buyersMenu: false,
    		accessoriesMenu: false,
    		othersMenu: false
			}));
  	}

  	if (data === 'buyers'){
  		this.setState(prevState => ({
			  sellersMenu: false,
			  productsMenu: false,
			  camerasMenu: false,
			  buyersMenu: !prevState.buyersMenu,
    		accessoriesMenu: false,
    		othersMenu: false
			}));
  	}

  	if (data === 'accessories'){
  		this.setState(prevState => ({
			  sellersMenu: false,
			  productsMenu: false,
			  camerasMenu: false,
			  buyersMenu: false,
    		accessoriesMenu: !prevState.accessoriesMenu,
    		othersMenu: false
			}));
  	}

  	if (data === 'cameras'){
  		this.setState(prevState => ({
			  sellersMenu: false,
			  productsMenu: false,
			  camerasMenu: !prevState.camerasMenu,
			  buyersMenu: false,
    		accessoriesMenu: false,
    		othersMenu: false
			}));
  	}

  	if (data === 'others'){
  		this.setState(prevState => ({
			  sellersMenu: false,
			  productsMenu: false,
			  camerasMenu: false,
			  buyersMenu: false,
    		accessoriesMenu: false,
    		othersMenu: !prevState.othersMenu
			}));
  	}
  }

  render() {
    //const {} = this.props;
    const { 
    	productsMenu, 
    	camerasMenu,
    	sellersMenu,
    	buyersMenu,
    	accessoriesMenu,
    	othersMenu
    } = this.state;

    return (
    	<div>
    		{/* ======== products */}
	    	<div onClick={() => this.displayChange('products')} className={'sidebar-list-item'}>
	    		{productsMenu && <ArrowDropDownIcon className={'icon-in-sidebar'}/>}
	    		{!productsMenu && <ArrowRightIcon className={'icon-in-sidebar'}/>}
	    		Phones
	    	</div>
	    	<div className={'sidebar-list-submenu'} style={productsMenu ? {} : {display: 'none'}}>
	    		<Link to="/all-products">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/> 
	    			All Phones
	    		</Link>
		    	<Link to="/add-product">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/> 
	    			Add Phone
	    		</Link>
	    		<Link to="/sold-products">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/> 
	    			Sold Phones
	    		</Link>
	    		<Link to="/products-category">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/> 
	    			Phones Category
	    		</Link>
	    	</div>

    		{/* ======== cameras */}
	    	<div onClick={() => this.displayChange('cameras')} className={'sidebar-list-item'}>
	    		{camerasMenu && <ArrowDropDownIcon className={'icon-in-sidebar'}/>}
	    		{!camerasMenu && <ArrowRightIcon className={'icon-in-sidebar'}/>}
	    		Cameras
	    	</div>
	    	<div className={'sidebar-list-submenu'} style={camerasMenu ? {} : {display: 'none'}}>
	    		<Link to="/all-cameras">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/> 
	    			All Cameras
	    		</Link>
		    	<Link to="/add-camera">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/> 
	    			Add Camera
	    		</Link>
	    		<Link to="/cameras-sold">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/> 
	    			Sold Cameras
	    		</Link>
	    		<Link to="/cameras-category">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/> 
	    			Cameras Category
	    		</Link>
	    	</div>

	    	{/* ======== others */}
	    	<div onClick={() => this.displayChange('others')} className={'sidebar-list-item'}>
	    		{othersMenu && <ArrowDropDownIcon className={'icon-in-sidebar'}/>}
	    		{!othersMenu && <ArrowRightIcon className={'icon-in-sidebar'}/>}
	    		Others
	    	</div>
	    	<div className={'sidebar-list-submenu'} style={othersMenu ? {} : {display: 'none'}}>
	    		<Link to="/all-others">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/> 
	    			All Others
	    		</Link>
		    	<Link to="/add-other">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/> 
	    			Add other
	    		</Link>
	    		<Link to="/others-sold">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/> 
	    			Sold others
	    		</Link>
	    		<Link to="/others-category">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/> 
	    			Others Category
	    		</Link>
	    	</div>

	    	{/* ======== sellers */}
	    	<div onClick={() => this.displayChange('sellers')} className={'sidebar-list-item'}>
	    		{sellersMenu && <ArrowDropDownIcon className={'icon-in-sidebar'}/>}
	    		{!sellersMenu && <ArrowRightIcon className={'icon-in-sidebar'}/>}
	    		Sellers
	    	</div>
	    	<div className={'sidebar-list-submenu'} style={sellersMenu ? {} : {display: 'none'}}>
	    		<Link to="/all-sellers">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/> 
	    			All Sellers
	    		</Link>
		    	<Link to="/add-seller">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/> 
	    			Add Seller
	    		</Link>
	    		<Link to="/sellers-category">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/> 
	    			Sellers Category
	    		</Link>
	    	</div>

	    	{/* ======== buyers */}
	    	<div onClick={() => this.displayChange('buyers')} className={'sidebar-list-item'}>
	    		{buyersMenu && <ArrowDropDownIcon className={'icon-in-sidebar'}/>}
	    		{!buyersMenu && <ArrowRightIcon className={'icon-in-sidebar'}/>}
	    		Buyers
	    	</div>
	    	<div className={'sidebar-list-submenu'} style={buyersMenu ? {} : {display: 'none'}}>
	    		<Link to="/all-buyers">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/>
	    			All Buyers
	    		</Link>
		    	<Link to="/add-buyer">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/>
	    			Add Buyer
	    		</Link>
	    		<Link to="/buyers-category">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/>
	    			Buyers Category
	    		</Link>
	    	</div>

	    {/* ======== accessories */}
	    	<div onClick={() => this.displayChange('accessories')} className={'sidebar-list-item'}>
	    		{accessoriesMenu && <ArrowDropDownIcon className={'icon-in-sidebar'}/>}
	    		{!accessoriesMenu && <ArrowRightIcon className={'icon-in-sidebar'}/>}
	    		Accessories
	    	</div>
	    	<div className={'sidebar-list-submenu'} style={accessoriesMenu ? {} : {display: 'none'}}>
	    		<Link to="/all-accessories">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/>
	    			All Accessories
	    		</Link>
		    	<Link to="/add-accessories">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/>
	    			Add Accessories
	    		</Link>
	    		<Link to="/sold-accessories">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/>
	    			Sold Accessories
	    		</Link>
	    		<Link to="/accessories-category">
	    			<FiberManualRecordIcon className={'icon-in-sidebar-circle'}/>
	    			Accessories Category
	    		</Link>
	    	</div>

	    	<Link to="/settings" className={'sidebar-list-item sidebar-list-item-p-23'}>Settings</Link>

	    	<div onClick={this.LogoutClick} className={'sidebar-list-item sidebar-list-item-p-23'}>
	    		Log out
	    	</div>
      </div>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(MenuMgmt);