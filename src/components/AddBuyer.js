import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Grid from '@material-ui/core/Grid';
import DialogActions from '@material-ui/core/DialogActions';
import Notify from './general/Notify';

const Axios = require('axios');

let startInputsValue = {
	buyer_name: '',
	buyer_tel: '',
	buyer_email: ''
}

class AddBuyer extends Component {
  state = {
    dialogOpen: false,
    inputsValue: startInputsValue
  };
  
  componentDidMount() {
  }

  clickAdd = () => {
  	this.setState({
  		dialogOpen: true
  	});
  }

  dialogCancel = () => {
  	this.setState({
  		dialogOpen: false,
  		inputsValue: startInputsValue
  	})
  }

  dialogSave = () => {
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/add-buyer' , {data: this.state.inputsValue},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => { //200
      if (result['data']){ 
      	this.setState({
		  		dialogOpen: false,
		  		inputsValue: startInputsValue
		  	})
		  	this.refs.notifyC.notify('success','Buyer created');
      } else {
      	//this date already exist
      	this.refs.notifyC.notify('error','This buyer already exists');
      }
    }).catch((error) => { //400
    	console.log(error);
    });
  }

  inputChange = (data, key) => {
  	let newData = data.target.value;

  	this.setState(prevState => {
		  let obj = Object.assign({}, prevState.inputsValue);
		  obj[key] = newData.toUpperCase();              
		  return { inputsValue: obj };
		})
  }

  render() {
    const {dialogOpen, inputsValue} = this.state;

    return (
    	<div style={{display:'inline-block'}}>
    		<Button 
	    		variant="contained" 
	    		size="small"
	    		style={{top: '-10px', 
	    		left: '30px'}}
	    		onClick={this.clickAdd}
			  > 
		      Add Buyer
		    </Button>

		    <Dialog
	        open={dialogOpen}
	        onClose={this.dialogCancel}
	        fullWidth={true}
	        aria-labelledby="alert-dialog-title"
	        aria-describedby="alert-dialog-description"
	        style={{minWidth: '300px'}}
   			>
   			 	<DialogTitle style={{paddingButtom: '0!important'}}>Add buyer
   			 	</DialogTitle>
   			 	<DialogContent>
						<Grid container spacing={1}>
							<Grid item lg={6} xs={12}>
								<TextField
					        placeholder="Name"
					        variant="outlined"
					        value={inputsValue['buyer_name']}
					        name="buyer_name"
					        onChange={(value, key) => this.inputChange(value, 'buyer_name')}
					        error={inputsValue['buyer_name']===''}
						    />
						  </Grid>
						  <Grid item lg={6} xs={12}>
						    <TextField
					        placeholder="Email"
					        variant="outlined"
					        value={inputsValue['buyer_email']}
					        name="buyer_email"
					        onChange={(value, key) => this.inputChange(value, 'buyer_email')}
					        error={inputsValue['buyer_email']===''}
						    />
						  </Grid>
						  <Grid item lg={6} xs={12}>
						    <TextField
					        placeholder="Tel"
					        variant="outlined"
					        value={inputsValue['buyer_tel']}
					        name="buyer_tel"
					        onChange={(value, key) => this.inputChange(value, 'buyer_tel')}
						    />
					    </Grid>
					  </Grid>
     			 	</DialogContent>
     			 	<DialogActions>
     			 		<Button onClick={this.dialogCancel} color="primary">
		            Cancel
		          </Button>
		          <Button 
		          	onClick={this.dialogSave} 
		          	disabled={inputsValue['buyer_name']==='' ||inputsValue['buyer_email']===''}
		          	color="primary">
		            Add
		          </Button>
     			 	</DialogActions>
     		</Dialog>
     		<Notify ref='notifyC' {...this.props} />
    	</div>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(AddBuyer);