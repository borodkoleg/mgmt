import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Grid from '@material-ui/core/Grid';
import Notify from './general/Notify';

const Axios = require('axios');
const { isNumberOrNull } = require('../server/libs/validation');

let startInputsValue = {
	repair_cost: '',
	buying_price: '',
	total_buying_price: '',
	product_status: '',
	selling_price: '',
	screen_protector: '',
	headphone: '',
	total_upsell: '',
	total_profit: '',
	notes: ''
};

class PhoneData extends Component {
  state = {
    dialogOpen: false,
    inputsValue: startInputsValue
  };
  
  componentDidMount() {
  }

  clickPhoneData = () => {
  	let phoneId = this.props.selectedRows[0]['id'];

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-phone-data' , { phoneId },
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result.data){
      	//console.log(JSON.stringify(result.data));
	      this.setState({
				  inputsValue: {...result.data}
				});

    //   	inputsValue.repair_cost = result.data.repair_cost;
    //   	inputsValue.buying_price =
				// inputsValue.total_buying_price =
				// inputsValue.product_status =
				// inputsValue.selling_price =
				// inputsValue.screen_protector =
				// inputsValue.headphone: '',
				// inputsValue.total_upsell: '',
				// inputsValue.total_profit: '',
				// inputsValue.notes: ''
      // 	this.refs.notifyC.notify('success','Success');
      // 	this.setState({
	    	// 	dialogOpen: false,
	    	// 	inputsValue: startInputsValue
    		// });
    		// this.props.dataUpdate();
      } else {
      	// this.refs.notifyC.notify('error','Data is not correct');
      }
    }).catch((err) => {
    	console.log(err);
    });

  	this.setState({
  		dialogOpen: true
  	});
  }

  dialogCancel = () => {
  	this.setState({
			dialogOpen: false,
			inputsValue: startInputsValue
  	});

  	this.props.deselectAllRows();
  }

  inputChange = (data, key) => {
  	let newData = data.target.value;

  	this.setState(prevState => {
		  let obj = Object.assign({}, prevState.inputsValue);
		  obj[key] = newData.toUpperCase();              
		  return { inputsValue: obj };
		})
  }

  clickButtonSet = () => {
  	let data = this.state.inputsValue;
  	let phoneId = this.props.selectedRows[0]['id'];

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/set-phone-data' , { data, phoneId },
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result.data){
      	this.refs.notifyC.notify('success','Success');
      	this.setState({
	    		dialogOpen: false,
	    		inputsValue: startInputsValue
    		});
    		this.props.deselectAllRows();
    		this.props.dataUpdate();
      } else {
      	this.refs.notifyC.notify('error','Data is not correct');
      }
    }).catch((err) => {
    	console.log(err);
    	//this.refs.notifyC.notify('error','Success');
    });
  }

  render() {
    const {
    	dialogOpen,
    	inputsValue
    } = this.state;

    return (
    	<div style={{display:'inline-block'}}>
    		<Button 
	    		variant="contained" 
	    		size="small"
	    		style={{top: '-10px', 
	    		left: '60px'}}
	    		disabled = {this.props.disabled}
	    		onClick={this.clickPhoneData}
			  > 
		      Phone Data
		    </Button>

		    <Dialog
	        open={dialogOpen}
	        onClose={this.dialogCancel}
	        fullWidth={true}
	        aria-labelledby="alert-dialog-title"
	        aria-describedby="alert-dialog-description"
	        style={{minWidth: '300px'}}
   			>
   			 	<DialogTitle style={{paddingButtom: '0!important'}}>
   			 		Phone Data
   			 	</DialogTitle>
   			 	<DialogContent>
						<Grid container spacing={1}>
						<Grid item lg={6} xs={12}>
   			 		<TextField
			        placeholder="Repair cost"
			        variant="outlined"
			        value={inputsValue['repair_cost'] || ''}
			        name="repair_cost"
			        onChange={(value, key) => this.inputChange(value, 'repair_cost')}
			        error={!isNumberOrNull(inputsValue['repair_cost'])}
				    />
				    </Grid>
				    <Grid item lg={6} xs={12}>
				    <TextField
			        placeholder="Buying Price"
			        variant="outlined"
			        value={inputsValue['buying_price'] || ''}
			        name="buying_price"
			        onChange={(value, key) => this.inputChange(value, 'buying_price')}
			        error={!isNumberOrNull(inputsValue['buying_price'])}
				    />
				    </Grid>
				    <Grid item lg={6} xs={12}>
				    <TextField
			        placeholder="Total Buying Price"
			        variant="outlined"
			        value={inputsValue['total_buying_price'] || ''}
			        name="total_buying_price"
			        onChange={(value, key) => this.inputChange(value, 'total_buying_price')}
			        error={!isNumberOrNull(inputsValue['total_buying_price'])}
				    />
				    </Grid>
				    <Grid item lg={6} xs={12}>
				    <TextField
			        placeholder="Product Status"
			        variant="outlined"
			        value={inputsValue['product_status'] || ''}
			        name="product_status"
			        onChange={(value, key) => this.inputChange(value, 'product_status')}
			        //error={inputsValue['seller_name']===''}
				    />
				    </Grid>
				    <Grid item lg={6} xs={12}>
				    <TextField
			        placeholder="Selling Price"
			        variant="outlined"
			        value={inputsValue['selling_price'] || ''}
			        name="selling_price"
			        onChange={(value, key) => this.inputChange(value, 'selling_price')}
			        error={!isNumberOrNull(inputsValue['selling_price'])}
				    />
				    </Grid>
				    <Grid item lg={6} xs={12}>
				    <TextField
			        placeholder="Screen Protector"
			        variant="outlined"
			        value={inputsValue['screen_protector'] || ''}
			        name="screen_protector"
			        onChange={(value, key) => this.inputChange(value, 'screen_protector')}
			        //error={inputsValue['seller_name']===''}
				    />
				    </Grid>
				    <Grid item lg={6} xs={12}>
				    <TextField
			        placeholder="Headphone"
			        variant="outlined"
			        value={inputsValue['headphone'] || ''}
			        name="headphone"
			        onChange={(value, key) => this.inputChange(value, 'headphone')}
			        //error={inputsValue['seller_name']===''}
				    />
				    </Grid>
				    <Grid item lg={6} xs={12}>
				    <TextField
			        placeholder="Total Upsell"
			        variant="outlined"
			        value={inputsValue['total_upsell'] || ''}
			        name="total_upsell"
			        onChange={(value, key) => this.inputChange(value, 'total_upsell')}
			        error={!isNumberOrNull(inputsValue['total_upsell'])}
				    />
				    </Grid>
				    <Grid item lg={6} xs={12}>
				    <TextField
			        placeholder="Total Profit"
			        variant="outlined"
			        value={inputsValue['total_profit'] || ''}
			        name="total_profit"
			        onChange={(value, key) => this.inputChange(value, 'total_profit')}
			        error={!isNumberOrNull(inputsValue['total_profit'])}
				    />
				    </Grid>
				    <Grid item lg={6} xs={12}>
				    <TextField
			        placeholder="Notes"
			        variant="outlined"
			        value={inputsValue['notes'] || ''}
			        name="notes"
			        onChange={(value, key) => this.inputChange(value, 'notes')}
			        //error={inputsValue['seller_name']===''}
				    />
				    </Grid>
				    </Grid>
   			 	</DialogContent>
   			 	<DialogActions>
   			 		<Button onClick={this.dialogCancel} color="primary">
	            Cancel
	          </Button>
	          <Button 
	          	onClick={this.clickButtonSet} 
	          	//disabled={buttonSetDisabled}
	          	color="primary">
	            Set
	          </Button>
   			 	</DialogActions>
     		</Dialog>
     		<Notify ref='notifyC' {...this.props} />
    	</div>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(PhoneData);