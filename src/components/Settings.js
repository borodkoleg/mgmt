import React, {Component} from 'react';
import { connect } from 'react-redux';
import preloader from '../img/preloader.svg';
//const Axios = require('axios');

class Settings extends Component {
  state = {
    getAllButtonDisabled: false
  };
  
  componentDidMount() {
  }

  render() {
    const {getAllButtonDisabled} = this.state;

    return (
    	<div>
	      	{getAllButtonDisabled && 
	      	(<img style={{width: '100px',
    				position: 'relative',
    				top: '5px',
    				left: '10px'}}
    				alt={'preloader'} 
    				src={ preloader } />
    			)}
    	</div>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(Settings);