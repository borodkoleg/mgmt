import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Notify from '../general/Notify';
const { isNumberOrMinusOne, isIntegerOrMinusOne } = require('../../server/libs/validation');

const { basisDialogScreen } = require('../general/themes');
const theme1 = createMuiTheme(basisDialogScreen);
const Axios = require('axios');

let startInputsValue = {
	product_code: '',
	product_name: '',
	brand: '',
	model: '',
	quantity: '',
	buying_price: '',
	selling_price: '',
	category_id: ''
};

class AddAccessories extends Component {
  state = {
    inputsValue: startInputsValue,
    categoryArray: []
  };
  
  componentDidMount() {
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-categories' , {index: 4},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result){
      	this.setState({
      		categoryArray: [ {id: "", name: ""}, ...result['data'] ]
      	})
      }
    })
  }

  inputChange = (data, key) => {
  	let newData = data.target.value;

  	this.setState(prevState => {
		  let obj = Object.assign({}, prevState.inputsValue);
		  obj[key] = newData.toUpperCase();              
		  return { inputsValue: obj };
		})
  }

  addSellerClick = () => {
  	let obj = this.state.inputsValue;
  	if (!obj['category_id']) {
  		delete obj.category_id
  	}

  	if (!obj['buying_price']) {
  		delete obj.buying_price
  	}

  	if (!obj['selling_price']) {
  		delete obj.selling_price
  	}

  	if (!obj['quantity']) {
  		delete obj.quantity
  	}

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/add-accessories' , {data: obj},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result['data']){
      	this.setState({
      		inputsValue: startInputsValue
      	})
      	this.refs.notifyC.notify('success','Success');
      } else {
      	this.refs.notifyC.notify('error','This item already exists');
      }
    })
  }

  render() {
    //const {} = this.props;
    const {inputsValue, categoryArray} = this.state;

    //console.log(JSON.stringify(inputsValue, null, 2));

    return (
    		<Grid container spacing={2}>
    		<MuiThemeProvider theme={theme1}>
	    		<Grid item xs={12} sm={6} md={4}>
		        <div className={'block'}>
		        <div className={'text-headline'}>
		        	Add Accessories
		        </div>

		        <TextField
			        placeholder="Code"
			        variant="outlined"
			        label="Code"
			        value={inputsValue['product_code']}
			        name="product_code"
			        onChange={(value, key) => this.inputChange(value, 'product_code')}
			        error={inputsValue['product_code']===''}
				    />

				    <TextField
			        placeholder="Name"
			        variant="outlined"
			        label="Name"
			        value={inputsValue['product_name']}
			        name="product_name"
			        onChange={(value, key) => this.inputChange(value, 'product_name')}
			        error={inputsValue['product_name']===''}
				    />

				    <TextField
				    	label="Brand"
			        placeholder="Brand"
			        variant="outlined"
			        value={inputsValue['brand']}
			        name="brand"
			        onChange={(value, key) => this.inputChange(value, 'brand')}
					  />

					  <TextField
					  	label="Model"
			        placeholder="Model"
			        variant="outlined"
			        value={inputsValue['model']}
			        name="model"
			        onChange={(value, key) => this.inputChange(value, 'model')}
			      />

			      </div>
			      </Grid>
			      <Grid item xs={12} sm={6} md={4}>
			      <div className={'block'} style={{paddingTop: '58px'}}>

			      <TextField
					  	label="Quantity"
			        placeholder="Quantity"
			        variant="outlined"
			        value={inputsValue['quantity']}
			        name="quantity"
			        onChange={(value, key) => this.inputChange(value, 'quantity')}
			        error={!isIntegerOrMinusOne(inputsValue['quantity'])}
			      />

			      <TextField
					  	label="Buying Price"
			        placeholder="Buying Price"
			        variant="outlined"
			        value={inputsValue['buying_price']}
			        name="buying_price"
			        onChange={(value, key) => this.inputChange(value, 'buying_price')}
			        error={!isNumberOrMinusOne(inputsValue['buying_price'])}
			      />

			      <TextField
					  	label="Selling Price"
			        placeholder="Selling Price"
			        variant="outlined"
			        value={inputsValue['selling_price']}
			        name="selling_price"
			        onChange={(value, key) => this.inputChange(value, 'selling_price')}
			        error={!isNumberOrMinusOne(inputsValue['selling_price'])}
			      />

			      <TextField
		        select
		        label="Category"
		        value={inputsValue['category_id']}
		        onChange={(value, key) => this.inputChange(value, 'category_id')}
		        SelectProps={{
		          native: true
		        }}
			      >
			        {categoryArray.map(option => (
			          <option key={option.id} value={option.id}>
			            {option.name}
			          </option>
			        ))}
	      		</TextField>

				    <br/>
					  <Button 
		    		variant="contained" 
		    		size="medium"	    		
		    		onClick={this.addSellerClick}
		    		disabled={
		    				inputsValue['product_code']!=='' &&
		    				inputsValue['product_name']!=='' &&
		    				isNumberOrMinusOne(inputsValue['selling_price']) &&
		    				isNumberOrMinusOne(inputsValue['buying_price']) &&
		    				isIntegerOrMinusOne(inputsValue['quantity'])
		    			? false
		    			: true
		    		}
					  > 
				      Add
				    </Button>
		        
		        </div>
		      </Grid>
		    </MuiThemeProvider>
		    <Notify ref='notifyC' {...this.props} />
	      </Grid>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(AddAccessories);