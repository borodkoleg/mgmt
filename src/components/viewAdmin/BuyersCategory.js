import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { categories } from '../../data/fields';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Notify from '../general/Notify';
import DialogOneField from '../general/DialogOneField';

const { basisDialogScreen } = require('../general/themes');
const theme1 = createMuiTheme(basisDialogScreen);
const Axios = require('axios');

class BuyersCategory extends Component {
  state = {
    rowData: [],
    name: '',
    buttonsDisabled: true,
    selectedRow: {},
    openEditDialog: false
  };
  
  componentDidMount() {
  	this.dataUpdate();
  }

  dataUpdate(){
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-categories' , {index: 3}, //products
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
	    })
	    .then((result) => {
	    	if (result['data']) {
	    		this.setState({
	    			rowData: [...result['data']]
	    		});
	    	}
	    });
  }

  inputChange = (data) => {
  	let value = data.target.value;

  	this.setState({
  		name: value.toUpperCase()
  	});
  }

  addNewCategory = () => {
  	let name = this.state.name;

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/add-category' , {index: 3, name: name}, //products
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
	    })
	    .then((result) => {
	    	if (result && result['data']) {
	    		this.dataUpdate();
	    		this.setState({
	    			name: '',
				    buttonsDisabled: true,
				    selectedRow: {}
	    		});
	    		this.refs.notifyC.notify('success','Success');
	    	} else {
	    		this.refs.notifyC.notify('error','This category already exists');
	    	}
	    });
  }

  onGridReady = params => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  };

  agGridClick = event => {
  	const selectedRows = this.gridApi.getSelectedRows();

  	if (selectedRows.length === 1) {
  		this.setState({
  			buttonsDisabled: false,
  			selectedRow: {...selectedRows[0]}
  		});
  	} else {
  		this.setState({
  			buttonsDisabled: true,
  			selectedRow: {}
  		});
  	}
  }

  editCaterory = () => {
  	//let selectedRow = this.state.selectedRow;

  	this.setState({
  		openEditDialog: true
  	});
  	//selectedRow['id'];
  }

  clickEditDialog = (value) => {
  	let name = value;
  	let id = this.state.selectedRow['id'];

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/edit-category' , {index: 3, name: name, id: id}, //products
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
	    })
	    .then((result) => {	    	
	    	if (result['data']) {
	    		this.dataUpdate();
	    		this.setState({
	    			name: '',
				    buttonsDisabled: true,
				    selectedRow: {},
				    openEditDialog: false
	    		});
	    		this.refs.notifyC.notify('success','Success');
	    	} else {
	    		this.refs.notifyC.notify('error','This category already exists');
	    	}
	    });
  }

  closeEditDialog = () => {
  	this.setState({
  		openEditDialog: false
  	})
  }

  deleteCaterory = () => {
  	let id = this.state.selectedRow['id'];

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/delete-category' , {index: 3, id: id}, //products
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
	    })
	    .then((result) => {	    	
	    	if (result['data']) {
	    		this.dataUpdate();
	    		this.setState({
	    			name: '',
				    buttonsDisabled: true,
				    selectedRow: {},
				    openEditDialog: false
	    		});
	    		this.refs.notifyC.notify('success','Success');
	    	}
	    });
  }

  render() {
    //const {} = this.props;
    const {rowData, 
    	name, 
    	buttonsDisabled, 
    	selectedRow,
    	openEditDialog
    } = this.state;
    console.log(selectedRow['name']);

    return (
    	<Grid container spacing={2}>
    	<Grid item xs={12} sm={6} md={4}>
    	<div className={'block'}>
    	<div className={'text-headline'}>Categories</div>
    	<div 
				className="ag-theme-balham"
	    	style={{ 
	        height: '300px', 
	        width: '210px' }} >
	    		<AgGridReact
	          columnDefs={categories}
	          rowData={rowData}
	          onGridReady={this.onGridReady}
	          //domLayout={'autoHeight'}
	          rowStyle = {{fontSize: '15px'}}
	      		//headerHeight = {150}
	      		onRowClicked={this.agGridClick}
	      		// rowClassRules = {{
	      		// 	'grid-green': 'data.age < 20',
	    				// 'grid-grey': 'data.age >= 20 && data.age < 25',
	      		// }}
	      		rowSelection = {'single'}
	      		onSelectionChanged={this.onSelectionChanged}
	        >
	        </AgGridReact>
	        </div>

	        <br/>

         	<Button 
	    		variant="contained" 
	    		size="medium"	    		
	    		onClick={this.editCaterory}
	    		disabled={buttonsDisabled}
				  > 
			      Edit
			    </Button>

			    <Button 
	    		variant="contained" 
	    		size="medium"	    		
	    		onClick={this.deleteCaterory}
	    		style={{marginLeft: '20px'}}
	    		disabled={buttonsDisabled}
				  > 
			      Delete
			    </Button>

			    </div>
	        </Grid>

	        <Grid item xs={12} sm={6} md={4}>
	        <div className={'block'}>
	        <div className={'text-headline'}>Add new category</div>
	        <MuiThemeProvider theme={theme1}>
	        	<TextField
			        variant="outlined"
			        label="Name"
			        value={name}
			        name="name"
			        onChange={(value) => this.inputChange(value)}
			        error={!name}
			    	/>
			    	<br/>
		        <Button 
		    		variant="contained" 
		    		size="medium"	    		
		    		onClick={this.addNewCategory}
		    		disabled={name.length > 0 ? false : true}
					  > 
				      Add
				    </Button>

	        </MuiThemeProvider>
	        </div>
	        </Grid>

	        <DialogOneField
	        	open={openEditDialog}
	        	close={this.closeEditDialog}
	        	title={'Category change'}
	        	value={selectedRow['name']}
	        	clickBtn={(data) => this.clickEditDialog(data)}
	        />

	        <Notify ref='notifyC' {...this.props} />
	    </Grid>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(BuyersCategory);