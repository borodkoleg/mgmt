import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Clear from '@material-ui/icons/Clear';
import Notify from '../general/Notify';

const { basisDialogScreen } = require('../general/themes');
const theme1 = createMuiTheme(basisDialogScreen);

const Axios = require('axios');
const dirPublic = window.location.origin;

class ProductScreen4 extends Component {
  state = {
    files: {
    	0: 0,
    	1: 0,
    	2: 0,
    	3: 0,
    	4: 0
    },
    buttonsDisabled: false, 
    next: false,
    images: {
    	1: '',
    	2: '',
    	3: '',
    	4: '',
    	5: ''
    }
  }  
  
  componentDidMount() {
  	this.getImages();
  }

  getBase64(resultObject, index) {
  	let imagesResult = this.state.images;
  	let url = `${dirPublic}/uploads/${resultObject[index]}`;

  	//console.log('url=> ' + url);
  	if (!resultObject[index]){
    	return;
  	}
  	Axios.get(url, {
      responseType: 'arraybuffer'
    })
    .then((response) => {
    	let buff = new Buffer(response.data, 'binary').toString('base64');
    	//data:image/png;base64,
    	let result = 'data:image/jpeg;base64,' + buff;
    	//console.log(result);
    	
    	imagesResult[index] = result;
    	this.setState({
    		images: imagesResult
    	})
    })
	}

  async getImages(){
  	return await Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-photos-by-phone' , {
  			id: this.props.phone['id'],
  		},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
	    })
	    .then((result) => {
	      if (result['data']){
	      	console.log(JSON.stringify(result['data'], null, 2))	
	      	let resultObject = {}
	      	result['data'].forEach((obj) => {
	      		resultObject[obj['key']] = obj['name']
	      	});
      		this.props.changeAll({
						images: {...resultObject}
			  	})

			  	this.getBase64(resultObject,'1')
				  this.getBase64(resultObject,'2')
				  this.getBase64(resultObject,'3')
				  this.getBase64(resultObject,'4')
				  this.getBase64(resultObject,'5')
	      		
	      }
	    })
  }

  handleSubmit = (event) => {
    event.preventDefault();
    let next = this.state.next;

    const data = new FormData();

    data.append('id', this.props.phone['id']);

    let objFile = {}

    if (this.refs.uploadFile1.files[0]){
    	objFile[1] = true
    } else {
    	objFile[1] = false
    }

    if (this.refs.uploadFile2.files[0]){
    	objFile[2] = true
    } else {
    	objFile[2] = false
    }

    if (this.refs.uploadFile3.files[0]){
    	objFile[3] = true
    } else {
    	objFile[3] = false
    }

    if (this.refs.uploadFile4.files[0]){
    	objFile[4] = true
    } else {
    	objFile[4] = false
    }

    if (this.refs.uploadFile5.files[0]){
    	objFile[5] = true
    } else {
    	objFile[5] = false
    }

    //console.log(this.refs.uploadFile1.files[0]);
    data.append('objFile', JSON.stringify(objFile));

    let keysArray = []
    for (var key in objFile){
    	if (objFile[key]){
    		keysArray.push(key);
    	}
    }

    data.append('keysArray', JSON.stringify(keysArray));

    if (Object.values(objFile['1'])) {
    	data.append('file', this.refs.uploadFile1.files[0])
    }

    if (Object.values(objFile['2'])) {
    	data.append('file', this.refs.uploadFile2.files[0])
    }

    if (Object.values(objFile['3'])) {
    	data.append('file', this.refs.uploadFile3.files[0])
    }

    if (Object.values(objFile['4'])) {
    	data.append('file', this.refs.uploadFile4.files[0])
    }

    if (Object.values(objFile['5'])) {
    	data.append('file', this.refs.uploadFile5.files[0])
    }

    // console.log(data.getAll('file'));
    // return;

    Axios({
			method: 'post',
			processData: false,
			contentType: 'multipart/form-data',
			cache: false,
			url: process.env.REACT_APP_HOST_PORT + '/api/save-photos-phone', 
			data: data,
  	//add
		})
	    .then((result) => {
	      if (result['data']){
	      	this.props.sendMessage('success','Success');
	      	console.log(next);
	      	this.props.history.push({
		 				pathname: '/all-products'
					})
	      	//if (next){
	      		// this.props.tabValue(4);
	      		// this.props.dataUpdate(result['data']);
	      	//} else { //save

	     //  		let imagesProps = this.props.globalState.images;
	     //  		let images = {}
	     		// 	this.getImages().then((data) => {
	     		// 		this.props.changeAll({
							 // 	addPhoneManuallyStart: false,
							 // 	tabValue: 0,
							 // 	tabsShow: false
						 	// })
	     		// 	})
	     //  		this.props.changeAll({
						// 	addPhoneManuallyStart: false,
						// 	tabValue: 0,
						// 	tabsShow: false
						// })
	     //  		this.props.dataUpdate();
	     //  		this.props.deselectAllRowsBasis();

	      	//}
	    	}
  		})
	}

	readFile = (event, index) => {
  	let files = {...this.state.files}

  	//console.log(event.target.files[0]['size']);
  	//console.log(event.target.files[0]['type']);
  	if (!event.target.files[0]){
  		files[index] = 0;
			this.validationAndSet(files);
			return;
  	}

  	if (event.target.files[0]['size'] > 5000000){
  		files[index] = -1
  	} else {
  		files[index] = 1
  	}
  
  	if (
  		event.target.files[0]['type'] !== 'image/jpeg' &&
  		event.target.files[0]['type'] !== 'image/jpg' &&
  		event.target.files[0]['type'] !== 'image/png' &&
  		event.target.files[0]['type'] !== 'image/gif' &&
  		event.target.files[0]['type'] !== 'image/bmp'
  	) {
  		files[index] = -1
	  } else{
	  	files[index] = 1
	  }

	  this.validationAndSet(files)
	}

	validationAndSet(files){
		for (var key in files) {
    	if (files[key] === -1) {
		  	this.setState({
					buttonsDisabled: true,
		  		files: files
				});
				this.refs.notifyC.notify('error','File size must be < 5mb and image type is \'jpeg, png, gif, bmp \'');
	  		return;
	  	}
		}
	  
		this.setState({
			buttonsDisabled: false,
			files: files
		});
	}

	closeFile = (index) => {

  	if (index === 0){
  		this.refs.uploadFile1.value = null
  	}

  	if (index === 1){
  		this.refs.uploadFile2.value = null
  	}

  	if (index === 2){
  		this.refs.uploadFile3.value = null
  	}

  	if (index === 3){
  		this.refs.uploadFile4.value = null
  	}

  	if (index === 4){
  		this.refs.uploadFile5.value = null
  	}

  	let files = {...this.state.files}
  	files[index] = 0

  	this.validationAndSet(files)
  }

  componentDidUpdate(){
  	//start every update
  }

  saveData = (next) => {
		this.setState({
			next: next
		})

  	this.refs.formSubmit.click();
  }

	backClick = () => {
  	this.props.tabValue(2);
  }

  deleteFile = (file) => {
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/delete-photo-by-name' , {
  			name: file,
  		},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
	    })
	    .then((result) => {
	    	if (result['data']){
	    		this.getImages();
	    		this.refs.notifyC.notify('success','Success');
	    	} else {
	    		this.refs.notifyC.notify('error','Server error');
	    	}
	    })
	    .catch((err) => {
	    	this.refs.notifyC.notify('error','Server error');
	    })
  }

  render() {
  	let {buttonsDisabled, files, images} = this.state
  	let imagesProps = this.props.globalState.basisAgGrid.images;
    console.log(JSON.stringify(imagesProps['1'], null, 2));

    return (
    	<Grid container>
    		<MuiThemeProvider theme={theme1}>
    		{/* ================================ */}
    		<form ref='form' onSubmit={this.handleSubmit}>
    			<Grid container>
		      <Grid item xs={12} sm={6} md={6}>
		    	<div className='file-upload-container'>
		        <div className="form-group files color">

		        	<div className='uploadFile'>
			        	<div style={files[0] === -1 ? {color:'red'} : {}} className='uploadFileText'>
			        		Upload File 1 

			        		{imagesProps['1'] &&
			        		<div className='delete' onClick={() => this.deleteFile(imagesProps['1'])}>
										Delete image
									</div>
									}
			        	</div>
			          <input ref="uploadFile1" type="file" accept="image/*"
			          	onChange={(event)=> { 
			               this.readFile(event, 0) 
			          	}}
								/>

								{files[0] !== 0 &&
								<div className='cancel' onClick={() => this.closeFile(0)}>
									<Clear/>
								</div>}

								{imagesProps['1'] &&
					    		<img alt='photo1' 
										src={images['1']}
										onError={(e)=>{e.target.onerror = null; e.target.src=`${dirPublic}/uploads/empty.png`}}
									/>
								}
							</div>

							<div className='uploadFile'>
			        	<div style={files[1] === -1 ? {color:'red'} : {}} className='uploadFileText'>Upload File 2

			        		{imagesProps['2'] &&
			        		<div className='delete' onClick={() => this.deleteFile(imagesProps['2'])}>
										Delete image
									</div>
									}
			        	</div>
			          <input ref="uploadFile2" type="file" accept="image/*"
			          	onChange={(event)=> { 
			               this.readFile(event, 1) 
			          	}}
			        		onClick={(event)=> { 
			               //this.closeFile(event, 1)
			          	}}
								/>
								{files[1] !== 0 &&
								<div className='cancel' onClick={() => this.closeFile(1)}>
									<Clear/>
								</div>}

								{imagesProps['2'] &&
					    		<img alt='photo2' 
										src={images['2']}
										onError={(e)=>{e.target.onerror = null; e.target.src=`${dirPublic}/uploads/empty.png`}}
									/>
								}
							</div>

							<div className='uploadFile'>
			        	<div style={files[2] === -1 ? {color:'red'} : {}} className='uploadFileText'>Upload File 3

			        	{imagesProps['3'] &&
			        		<div className='delete' onClick={() => this.deleteFile(imagesProps['3'])}>
										Delete image
									</div>
								}
			        	</div>
			          <input ref="uploadFile3" type="file" accept="image/*"
			          	onChange={(event)=> { 
			               this.readFile(event, 2) 
			          	}}
			        		onClick={(event)=> { 
			               //this.closeFile(event, 2)
			          	}}
								/>
								{files[2] !== 0 &&
								<div className='cancel' onClick={() => this.closeFile(2)}>
									<Clear/>
								</div>}

								{imagesProps['3'] &&
					    		<img alt='photo3' 
										src={images['3']}
										onError={(e)=>{e.target.onerror = null; e.target.src=`${dirPublic}/uploads/empty.png`}}
									/>
								}
							</div>
		        </div>                 
		      </div>
		      </Grid>
		      <Grid item xs={12} sm={6} md={6}>
		      	<div className='file-upload-container'>
		      	<div className="form-group files color">
		      		<div className='uploadFile'>
			        	<div style={files[3] === -1 ? {color:'red'} : {}} className='uploadFileText'>Upload File 4

			        	{imagesProps['4'] &&
			        		<div className='delete' onClick={() => this.deleteFile(imagesProps['4'])}>
										Delete image
									</div>
								}
			        	</div>
			          <input ref="uploadFile4" type="file" accept="image/*"
			          	onChange={(event)=> { 
			               this.readFile(event, 3) 
			          	}}
			        		onClick={(event)=> { 
			               //this.closeFile(event, 3)
			          	}}
								/>
								{files[3] !== 0 &&
								<div className='cancel' onClick={() => this.closeFile(3)}>
									<Clear/>
								</div>}

								{imagesProps['4'] &&
					    		<img alt='photo4' 
										src={images['4']}
										onError={(e)=>{e.target.onerror = null; e.target.src=`${dirPublic}/uploads/empty.png`}}
									/>
								}
							</div>

							<div className='uploadFile'>
			        	<div style={files[4] === -1 ? {color:'red'} : {}} className='uploadFileText'>Upload File 5

			        	{imagesProps['5'] &&
			        		<div className='delete' onClick={() => this.deleteFile(imagesProps['5'])}>
										Delete image
									</div>
								}
			        	</div>
			          <input ref="uploadFile5" type="file" accept="image/*"
			          	onChange={(event)=> { 
			               this.readFile(event, 4) 
			          	}}
			        		onClick={(event)=> { 
			               //this.closeFile(event, 4)
			          	}}
								/>
								{files[4] !== 0 &&
								<div className='cancel' onClick={() => this.closeFile(4)}>
									<Clear/>
								</div>}

								{imagesProps['5'] &&
					    		<img alt='photo5' 
										src={images['5']}
										onError={(e)=>{e.target.onerror = null; e.target.src=`${dirPublic}/uploads/empty.png`}}
									/>
								}
							</div>
						</div>	
						</div>
		      </Grid>
	      	<button type="submit" ref="formSubmit" style={{opacity: 0}}>Submit</button>
	      	</Grid>
      	</form>
    		{/* ================================ */}
    		<Grid container style={{marginTop:'40px'}}>
    		<Grid item xs={12} sm={12} md={12}>
    		<Button 
	    		variant="contained" 
	    		size="medium"	    		
	    		onClick={this.backClick}
			  > 
		      Back
		    </Button>
    		<Button 
	    		variant="contained" 
	    		size="medium"	   
	    		style={{marginLeft: '40px'}} 		
	    		onClick={() => this.saveData(false)}
	    		disabled={buttonsDisabled}
			  > 
		      Save
		    </Button>
		    <Button 
	    		variant="contained" 
	    		size="medium"	
	    		style={{marginLeft: '20px'}}
	    		disabled={buttonsDisabled}
	    		onClick={() => this.saveData(true)}
			  >
		      Save & Next
		    </Button>
		    </Grid>
		    </Grid>
		    </MuiThemeProvider>
		    <Notify ref='notifyC' {...this.props} />
    	</Grid>
    )
  }
}

export default connect(
  state => ({
    globalState: state,
    phone: state.basisAgGrid.phone,
    addPhoneManuallyStart: state.basisAgGrid.addPhoneManuallyStart
  }),
  dispatch => ({
    tabsShow: (data) => {
    	dispatch({ type: 'BA_TABS_SHOW', payload: data });
    },
    tabValue: (data) => {
    	dispatch({ type: 'BA_TAB_VALUE', payload: data });
    },
    selectedRows: (data) => {
    	dispatch({ type: 'BA_SELECTED_ROWS', payload: data });
    },
    phoneChange: (data) => {
    	dispatch({ type: 'BA_PHONE', payload: data });
    },
    addManually: (data) => {
    	dispatch({ type: 'BA_ADD_MANUALLY', payload: data });
    },
    changeAll: (data) => {
    	dispatch({ type: 'BASIS_GRID_CHANGE_ALL', payload: data });
    }
  })
)(ProductScreen4);