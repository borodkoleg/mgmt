import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Notify from '../general/Notify';
import { Redirect } from 'react-router-dom'

const { basisDialogScreen } = require('../general/themes');
const theme1 = createMuiTheme(basisDialogScreen);
const Axios = require('axios');

let startInputsValue = {
	seller_name: -1,
	seller_email: -1,
	seller_tel: -1,
	seller_referrel: -1,
	seller_identification: -1,
	category_id: -1
};

class EditSeller extends Component {
  state = {
    inputsValue: startInputsValue,
    categoryArray: [],
    buttonBack: false
  };
  
  componentDidMount() {
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-categories' , {index: 2},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result){
      	this.setState({
      		categoryArray: [ {id: "", name: ""}, ...result['data'] ]
      	})
      }
    })
  }

  componentDidUpdate(){
	  window.onpopstate  = (e) => {
	 		if (e.currentTarget.location.pathname === '/edit-seller'){
	 			this.setState({
	 				buttonBack: true
	 			})
	 		}
	  }
  }

  inputChange = (data, key) => {
  	let newData = data.target.value;

  	this.setState(prevState => {
		  let obj = Object.assign({}, prevState.inputsValue);
		  obj[key] = newData.toUpperCase();              
		  return { inputsValue: obj };
		})
  }

  removeNotInitialize(obj){
  	let objRezult = {...obj}
  	let key = ''
  	for (key in objRezult) {
  		if (!objRezult[key]){
				objRezult[key] = null
  		}
  		if (objRezult[key] === -1){
				delete objRezult[key]
  		}
  	}

  	if (Object.entries(objRezult).length === 0 && objRezult.constructor === Object){
  		return false;
  	}

  	return objRezult;
  }

  editSellerClick = () => {
  	let obj = this.state.inputsValue;
  	let id = this.props.location.state.selectRow['id'];

  	let data = this.removeNotInitialize(obj);
  	if (data === false){
  		this.setState({
      		inputsValue: startInputsValue
      	})
      this.refs.notifyC.notify('success','Success');
      this.props.history.push({
				pathname: '/all-sellers'
			})
      return;
  	}

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/seller-edit' , {data, id},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result['data']){
      	this.setState({
      		inputsValue: startInputsValue
      	})
      	this.refs.notifyC.notify('success','Success');
      	this.props.history.push({
				  pathname: '/all-sellers'
				})
      } else {
      	this.refs.notifyC.notify('error','This seller already exists');
      }
    })
  }

  forFieldValue(key){
  	let inputsValue = this.state.inputsValue;
  	let selectRow = this.props.location.state.selectRow;

  	if (inputsValue[key] !== startInputsValue[key])
  	{
  		return inputsValue[key];
  	} else {
  		if (selectRow[key]){
				return selectRow[key];
  		} else {
  			return '';
  		}
  	}
  }

  render() {
    //const {} = this.props;
    const {inputsValue, categoryArray, buttonBack} = this.state;

    if (buttonBack){
    	return <Redirect to='/all-sellers'/>;
    }

    if (!this.props.location.state){
    	this.props.history.push('/all-sellers');
    	return null;
    } else {
    	//let selectRow = this.props.location.state.selectRow;
    }

    //console.log(JSON.stringify(inputsValue, null, 2));
    //console.log(JSON.stringify(selectRow, null, 2));

    return (
    		<Grid container spacing={2}>
    		<MuiThemeProvider theme={theme1}>
	    		<Grid item xs={12} sm={6} md={4}>
		        <div className={'block'}>
		        <div className={'text-headline'}>
		        	Edit Seller
		        </div>

		        <TextField
			        placeholder="Name"
			        variant="outlined"
			        label="Name"
			        value={this.forFieldValue('seller_name')}
			        name="seller_name"
			        onChange={(value, key) => this.inputChange(value, 'seller_name')}
			        error={inputsValue['seller_name']===''}
				    />

				    <TextField
				    	label="Email"
			        placeholder="Email"
			        variant="outlined"
			        value={this.forFieldValue('seller_email')}
			        name="seller_email"
			        onChange={(value, key) => this.inputChange(value, 'seller_email')}
			        error={inputsValue['seller_email']===''}
					  />

					  <TextField
					  	label="Tel"
			        placeholder="Tel"
			        variant="outlined"
			        value={this.forFieldValue('seller_tel')}
			        name="seller_tel"
			        onChange={(value, key) => this.inputChange(value, 'seller_tel')}
			      />

			      <TextField
			      	label="Referrel"
			        placeholder="Referrel"
			        variant="outlined"
			        value={this.forFieldValue('seller_referrel')}
			        name="seller_referrel"
			        onChange={(value, key) => this.inputChange(value, 'seller_referrel')}
			      />

			      <TextField
			      	label="Identification"
			        placeholder="Identification"
			        variant="outlined"
			        value={this.forFieldValue('seller_identification')}
			        name="seller_identification"
			        onChange={(value, key) => this.inputChange(value, 'seller_identification')}
			        error={inputsValue['seller_identification']===''}
			      />

			      <TextField
		        select
		        label="Category"
		        value={this.forFieldValue('category_id')}
		        onChange={(value, key) => this.inputChange(value, 'category_id')}
		        SelectProps={{
		          native: true
		        }}
			      >
			        {categoryArray.map(option => (
			          <option key={option.id} value={option.id}>
			            {option.name}
			          </option>
			        ))}
	      		</TextField>

				    <br/>
					  <Button 
		    		variant="contained" 
		    		size="medium"	    		
		    		onClick={this.editSellerClick}
		    		disabled={
		    				inputsValue['seller_name']!=='' &&
		    				inputsValue['seller_email']!=='' &&
		    				inputsValue['seller_identification']!==''
		    			? false
		    			: true
		    		}
					  > 
				      Edit
				    </Button>
		        
		        </div>
		      </Grid>
		    </MuiThemeProvider>
		    <Notify ref='notifyC' {...this.props} />
	      </Grid>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(EditSeller);