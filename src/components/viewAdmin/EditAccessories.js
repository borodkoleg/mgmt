import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Notify from '../general/Notify';
import { Redirect } from 'react-router-dom'
const { isNumberOrMinusOne, isIntegerOrMinusOne } = require('../../server/libs/validation');

const { basisDialogScreen } = require('../general/themes');
const theme1 = createMuiTheme(basisDialogScreen);
const Axios = require('axios');

let startInputsValue = {
	product_code: '!startData!',
	product_name: '!startData!',
	brand: '!startData!',
	model: '!startData!',
	quantity: '!startData!',
	buying_price: '!startData!',
	selling_price: '!startData!',
	category_id: '!startData!'
};

class EditBuyer extends Component {
  state = {
    inputsValue: startInputsValue,
    categoryArray: [],
    buttonBack: false
  };
  
  componentDidMount() {
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-categories' , {index: 4},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result){
      	this.setState({
      		categoryArray: [ {id: "", name: ""}, ...result['data'] ]
      	})
      }
    })
  }

  componentDidUpdate(){
	  window.onpopstate  = (e) => {
	 		if (e.currentTarget.location.pathname === '/edit-accessories'){
	 			this.setState({
	 				buttonBack: true
	 			})
	 		}
	  }
  }

  inputChange = (data, key) => {
  	let newData = data.target.value;

  	this.setState(prevState => {
		  let obj = Object.assign({}, prevState.inputsValue);
		  obj[key] = newData.toUpperCase();              
		  return { inputsValue: obj };
		})
  }

  removeNotInitialize(obj){
  	let objRezult = {...obj}
  	for (var key in objRezult) {
  		if (!objRezult[key]){
				objRezult[key] = null
  		}
  		if (objRezult[key] === '!startData!'){
				delete objRezult[key]
  		}
  	}

  	if (Object.entries(objRezult).length === 0 && objRezult.constructor === Object){
  		return false;
  	}

  	return objRezult;
  }

  editSellerClick = () => {
  	let obj = this.state.inputsValue;
  	let id = this.props.location.state.selectRow['id'];

  	let data = this.removeNotInitialize(obj);
  	if (data === false){
  		this.setState({
      		inputsValue: startInputsValue
      	})
      this.refs.notifyC.notify('success','Success');
      this.props.history.push({
				pathname: '/all-accessories'
			})
      return;
  	}

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/accessories-edit' , {data, id},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result['data']){
      	this.setState({
      		inputsValue: startInputsValue
      	})
      	this.refs.notifyC.notify('success','Success');
      	this.props.history.push({
				  pathname: '/all-accessories'
				})
      } else {
      	this.refs.notifyC.notify('error','This item already exists');
      }
    })
  }

  forFieldValue(key){
  	let inputsValue = this.state.inputsValue;
  	let selectRow = this.props.location.state.selectRow;

  	if (inputsValue[key] !== startInputsValue[key])
  	{
  		return inputsValue[key];
  	} else {
  		if (selectRow[key]){
				return selectRow[key];
  		} else {
  			return '';
  		}
  	}
  }

  render() {
    //const {} = this.props;
    const {inputsValue, categoryArray, buttonBack} = this.state;

    if (buttonBack){
    	return <Redirect to='/all-accessories'/>;
    }

    if (!this.props.location.state){
    	this.props.history.push('/all-accessories');
    	return null;
    } else {
    	//let selectRow = this.props.location.state.selectRow;
    }

    //console.log(JSON.stringify(inputsValue, null, 2));
    //console.log(JSON.stringify(selectRow, null, 2));

    return (
    		<Grid container spacing={2}>
    		<MuiThemeProvider theme={theme1}>
	    		<Grid item xs={12} sm={6} md={4}>
		        <div className={'block'}>
		        <div className={'text-headline'}>
		        	Edit Seller
		        </div>

		         <TextField
			        placeholder="Code"
			        variant="outlined"
			        label="Code"
			        value={this.forFieldValue('product_code')}
			        name="product_code"
			        onChange={(value, key) => this.inputChange(value, 'product_code')}
			        error={inputsValue['product_code']===''}
				    />

				    <TextField
			        placeholder="Name"
			        variant="outlined"
			        label="Name"
			        value={this.forFieldValue('product_name')}
			        name="product_name"
			        onChange={(value, key) => this.inputChange(value, 'product_name')}
			        error={inputsValue['product_name']===''}
				    />

				    <TextField
				    	label="Brand"
			        placeholder="Brand"
			        variant="outlined"
			        value={this.forFieldValue('brand')}
			        name="brand"
			        onChange={(value, key) => this.inputChange(value, 'brand')}
					  />

					  <TextField
					  	label="Model"
			        placeholder="Model"
			        variant="outlined"
			        value={this.forFieldValue('model')}
			        name="model"
			        onChange={(value, key) => this.inputChange(value, 'model')}
			      />

			      </div>
			      </Grid>
			       <Grid item xs={12} sm={6} md={4}>
			      <div className={'block'} style={{paddingTop: '58px'}}>

			      <TextField
					  	label="Quantity"
			        placeholder="Quantity"
			        variant="outlined"
			        value={this.forFieldValue('quantity')}
			        name="quantity"
			        onChange={(value, key) => this.inputChange(value, 'quantity')}
			        error={!isIntegerOrMinusOne(inputsValue['quantity'])}
			      />

			      <TextField
					  	label="Buying Price"
			        placeholder="Buying Price"
			        variant="outlined"
			        value={this.forFieldValue('buying_price')}
			        name="buying_price"
			        onChange={(value, key) => this.inputChange(value, 'buying_price')}
			        error={!isNumberOrMinusOne(inputsValue['buying_price'])}
			      />

			      <TextField
					  	label="Selling Price"
			        placeholder="Selling Price"
			        variant="outlined"
			        value={this.forFieldValue('selling_price')}
			        name="selling_price"
			        onChange={(value, key) => this.inputChange(value, 'selling_price')}
			        error={!isNumberOrMinusOne(inputsValue['selling_price'])}
			      />

			      <TextField
		        select
		        label="Category"
		        value={this.forFieldValue('category_id')}
		        onChange={(value, key) => this.inputChange(value, 'category_id')}
		        SelectProps={{
		          native: true
		        }}
			      >
			        {categoryArray.map(option => (
			          <option key={option.id} value={option.id}>
			            {option.name}
			          </option>
			        ))}
	      		</TextField>

				    <br/>
					  <Button 
		    		variant="contained" 
		    		size="medium"	    		
		    		onClick={this.editSellerClick}
		    		disabled={
		    				inputsValue['product_code']!=='' &&
		    				inputsValue['product_name']!=='' &&
		    				isNumberOrMinusOne(inputsValue['selling_price']) &&
		    				isNumberOrMinusOne(inputsValue['buying_price']) &&
		    				isIntegerOrMinusOne(inputsValue['quantity'])
		    			? false
		    			: true
		    		}
					  > 
				      Edit
				    </Button>
		        
		        </div>
		      </Grid>
		    </MuiThemeProvider>
		    <Notify ref='notifyC' {...this.props} />
	      </Grid>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(EditBuyer);