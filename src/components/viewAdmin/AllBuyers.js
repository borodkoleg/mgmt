import React, {Component} from 'react';
import { connect } from 'react-redux';

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { buyers_info } from '../../data/fields'
import Button from '@material-ui/core/Button';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Notify from '../general/Notify';
import DialogConfirm from '../general/DialogConfirm';

const { basisDialogScreen } = require('../general/themes');
const theme1 = createMuiTheme(basisDialogScreen);

const Axios = require('axios');

class AllBuyers extends Component {
  state = {
    rowData: [],
    buttonsDisabled: true,
    selectRow: {},
    dialogConfirmOpen: false
  };
  
  componentDidMount() {
  	this.dataUpdate();
  }

  dataUpdate(){
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-all-buyers' , {},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result){
      	this.setState({
      		rowData: [ ...result['data'] ]
      	})
      }
    });
  }

  onGridReady = params => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  };

  agGridClick = () => {
  	const selectedRows = this.gridApi.getSelectedRows();

  	if (selectedRows.length > 0) {
	  	this.setState({
	  		selectRow: {...selectedRows[0]},
	  		buttonsDisabled: false
	  	})
  	} else {
  		this.setState({
	  		selectRow: {},
				buttonsDisabled: true
	  	})
  	}
  }



  deleteItem = () => {
  	let id = this.state.selectRow['id'];

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/delete-buyer' , {id},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result['data']){
      	this.dataUpdate();
      	this.gridApi.deselectAll();
      	this.refs.notifyC.notify('success','Success');
      }
    });
  }

  clickDelete = () => {
  	this.setState({
  		dialogConfirmOpen: true
  	})
  }

  dialogConfirmClick = (data) => {
  	if (data){
  		this.deleteItem();
  	} 
  	this.setState({
  		dialogConfirmOpen: false,
  		buttonsDisabled: true
  	})
  	this.gridApi.deselectAll();
  }

  editSeller = () => {
  	this.props.history.push({
		  pathname: '/edit-buyer',
		  state: { selectRow: this.state.selectRow }
		})
	}

  render() {
    const {rowData, buttonsDisabled, selectRow, dialogConfirmOpen} = this.state;

    return (
    	<div>
    	<MuiThemeProvider theme={theme1}>
    		<Button 
	    		variant="contained" 
	    		size="medium"	    		
	    		onClick={this.clickDelete}
	    		disabled={buttonsDisabled}
				  > 
			      Delete
			    </Button>

			  <Button 
	    		variant="contained" 
	    		size="medium"	    		
	    		onClick={this.editSeller}
	    		disabled={buttonsDisabled}
	    		style={{marginLeft: '20px'}}
				  > 
			      Edit
			    </Button>
    	<div 
			className="ag-theme-balham"
    	style={{ 
        height: '400px', 
        width: '100%',
        marginTop: '20px' 
      }} 
      >
    		<AgGridReact
          columnDefs={buyers_info}
          rowData={rowData}
          onGridReady={this.onGridReady}
          rowSelection = {'single'}
          onRowClicked={this.agGridClick}
        />
    	</div>

    	<DialogConfirm
    		title = {'Do you want remove this item?'}
    		text = {selectRow['buyer_name']}
    		open = {dialogConfirmOpen}
    		click = {(data) => this.dialogConfirmClick(data)}
    	/>


    	</MuiThemeProvider>

    	<Notify ref='notifyC' {...this.props} />
    	</div>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(AllBuyers);