import React, {Component} from 'react';
import { connect } from 'react-redux';

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { accessories } from '../../data/fields'
import Button from '@material-ui/core/Button';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Notify from '../general/Notify';
import DialogConfirm from '../general/DialogConfirm';
import DialogQuantitySold from '../general/DialogQuantitySold';

const { basisDialogScreen } = require('../general/themes');
const theme1 = createMuiTheme(basisDialogScreen);

const Axios = require('axios');

class AllAccessories extends Component {
  state = {
    rowData: [],
    buttonsDisabled: true,
    selectRow: {},
    dialogConfirmOpen: false,
    openSoldDialog: false
  };
  
  componentDidMount() {
  	this.dataUpdate();
  }

  dataUpdate(){
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-all-accessories' , {},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result){
      	this.setState({
      		rowData: [ ...result['data'] ]
      	})
      }
    });
  }

  onGridReady = params => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  };

  agGridClick = () => {
  	const selectedRows = this.gridApi.getSelectedRows();

  	if (selectedRows.length > 0) {
	  	this.setState({
	  		selectRow: {...selectedRows[0]},
	  		buttonsDisabled: false
	  	})
  	} else {
  		this.setState({
	  		selectRow: {},
				buttonsDisabled: true
	  	})
  	}
  }



  deleteItem = () => {
  	let id = this.state.selectRow['id'];

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/delete-buyer' , {id},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result['data']){
      	this.dataUpdate();
      	this.gridApi.deselectAll();
      	this.refs.notifyC.notify('success','Success');
      }
    });
  }

  clickDelete = () => {
  	this.setState({
  		dialogConfirmOpen: true
  	})
  }

  dialogConfirmClick = (data) => {
  	if (data){
  		this.deleteItem();
  	} 
  	this.setState({
  		dialogConfirmOpen: false,
  		buttonsDisabled: true
  	})
  	this.gridApi.deselectAll();
  }

  editSeller = () => {
  	this.props.history.push({
		  pathname: '/edit-accessories',
		  state: { selectRow: this.state.selectRow }
		})
	}

	clickSold = () => {
		this.setState({
			openSoldDialog: true
		})
	}

	closeSoldDialog = () => {
		this.setState({
			openSoldDialog: false
		})
	}

	clickSoldDialog = (value) => {
		let selectRow = this.state.selectRow
		let data = {}
		data['quantity'] = selectRow['quantity']
		data['product_code'] = selectRow['product_code']
		data['product_name'] = selectRow['product_name']
		data['brand'] = selectRow['brand']
		data['model'] = selectRow['model']
		data['category'] = selectRow['category']
		data['buying_price'] = selectRow['buying_price']
		data['selling_price'] = selectRow['selling_price']

		// console.log('value=> ' + value);
		// console.log('quantity=> ' + data['quantity']);

		this.setState({
			openSoldDialog: false
		})

		Axios.post(process.env.REACT_APP_HOST_PORT + '/api/sold-accessories' , {data, id: selectRow['id'], value: value},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result['data']){
      	this.dataUpdate();
      	this.gridApi.deselectAll();
      	this.refs.notifyC.notify('success','Success');
      }
    });

	}

  render() {
    const {rowData, buttonsDisabled, selectRow, dialogConfirmOpen, openSoldDialog} = this.state;

    return (
    	<div>
    	<MuiThemeProvider theme={theme1}>
    		<Button 
	    		variant="contained" 
	    		size="medium"	    		
	    		onClick={this.clickSold}
	    		disabled={buttonsDisabled}
				  > 
			      Sold Out
			    </Button>

			  <Button 
	    		variant="contained" 
	    		size="medium"	    		
	    		onClick={this.editSeller}
	    		disabled={buttonsDisabled}
	    		style={{marginLeft: '20px'}}
				  > 
			      Edit
			    </Button>
    	<div 
			className="ag-theme-balham"
    	style={{ 
        height: '400px', 
        width: '100%',
        marginTop: '20px' 
      }} 
      >
    		<AgGridReact
          columnDefs={accessories}
          rowData={rowData}
          onGridReady={this.onGridReady}
          rowSelection = {'single'}
          onRowClicked={this.agGridClick}
        />
    	</div>

    	<DialogConfirm
    		title = {'Do you want remove this item?'}
    		text = {selectRow['buyer_name']}
    		open = {dialogConfirmOpen}
    		click = {(data) => this.dialogConfirmClick(data)}
    	/>


    	<DialogQuantitySold
      	open={openSoldDialog}
      	close={this.closeSoldDialog}
      	title={'Quantity'}
      	value={selectRow['quantity']}
      	clickBtn={(data) => this.clickSoldDialog(data)}
	    />
    	</MuiThemeProvider>

    	<Notify ref='notifyC' {...this.props} />
    	</div>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(AllAccessories);