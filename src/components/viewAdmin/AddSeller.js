import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Notify from '../general/Notify';

const { basisDialogScreen } = require('../general/themes');
const theme1 = createMuiTheme(basisDialogScreen);
const Axios = require('axios');

let startInputsValue = {
	seller_name: '',
	seller_email: '',
	seller_tel: '',
	seller_referrel: '',
	seller_identification: '',
	category_id: ''
};

class AddSeller extends Component {
  state = {
    inputsValue: startInputsValue,
    categoryArray: []
  };
  
  componentDidMount() {
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-categories' , {index: 2},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result){
      	this.setState({
      		categoryArray: [ {id: "", name: ""}, ...result['data'] ]
      	})
      }
    })
  }

  inputChange = (data, key) => {
  	let newData = data.target.value;

  	this.setState(prevState => {
		  let obj = Object.assign({}, prevState.inputsValue);
		  obj[key] = newData.toUpperCase();              
		  return { inputsValue: obj };
		})
  }

  addSellerClick = () => {
  	let obj = this.state.inputsValue;
  	if (!obj['category_id']) {
  		delete obj.category_id
  	}

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/add-seller' , {data: obj},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result['data']){
      	this.setState({
      		inputsValue: startInputsValue
      	})
      	this.refs.notifyC.notify('success','Success');
      } else {
      	this.refs.notifyC.notify('error','This seller already exists');
      }
    })
  }

  render() {
    //const {} = this.props;
    const {inputsValue, categoryArray} = this.state;

    //console.log(JSON.stringify(inputsValue, null, 2));

    return (
    		<Grid container spacing={2}>
    		<MuiThemeProvider theme={theme1}>
	    		<Grid item xs={12} sm={6} md={4}>
		        <div className={'block'}>
		        <div className={'text-headline'}>
		        	Add Seller
		        </div>

		        <TextField
			        placeholder="Name"
			        variant="outlined"
			        label="Name"
			        value={inputsValue['seller_name']}
			        name="seller_name"
			        onChange={(value, key) => this.inputChange(value, 'seller_name')}
			        error={inputsValue['seller_name']===''}
				    />

				    <TextField
				    	label="Email"
			        placeholder="Email"
			        variant="outlined"
			        value={inputsValue['seller_email']}
			        name="seller_email"
			        onChange={(value, key) => this.inputChange(value, 'seller_email')}
			        error={inputsValue['seller_email']===''}
					  />

					  <TextField
					  	label="Tel"
			        placeholder="Tel"
			        variant="outlined"
			        value={inputsValue['seller_tel']}
			        name="seller_tel"
			        onChange={(value, key) => this.inputChange(value, 'seller_tel')}
			      />

			      <TextField
			      	label="Referrel"
			        placeholder="Referrel"
			        variant="outlined"
			        value={inputsValue['seller_referrel']}
			        name="seller_referrel"
			        onChange={(value, key) => this.inputChange(value, 'seller_referrel')}
			      />

			      <TextField
			      	label="Identification"
			        placeholder="Identification"
			        variant="outlined"
			        value={inputsValue['seller_identification']}
			        name="seller_identification"
			        onChange={(value, key) => this.inputChange(value, 'seller_identification')}
			        error={inputsValue['seller_identification']===''}
			      />

			      <TextField
		        select
		        label="Category"
		        value={inputsValue['category_id']}
		        onChange={(value, key) => this.inputChange(value, 'category_id')}
		        SelectProps={{
		          native: true
		        }}
			      >
			        {categoryArray.map(option => (
			          <option key={option.id} value={option.id}>
			            {option.name}
			          </option>
			        ))}
	      		</TextField>

				    <br/>
					  <Button 
		    		variant="contained" 
		    		size="medium"	    		
		    		onClick={this.addSellerClick}
		    		disabled={
		    				inputsValue['seller_name']!=='' &&
		    				inputsValue['seller_email']!=='' &&
		    				inputsValue['seller_identification']!==''
		    			? false
		    			: true
		    		}
					  > 
				      Add
				    </Button>
		        
		        </div>
		      </Grid>
		    </MuiThemeProvider>
		    <Notify ref='notifyC' {...this.props} />
	      </Grid>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(AddSeller);