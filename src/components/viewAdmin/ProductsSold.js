import React, {Component} from 'react';
import { connect } from 'react-redux';

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { sold_phones } from '../../data/fields'
import Button from '@material-ui/core/Button';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Notify from '../general/Notify';
//import DialogConfirm from '../general/DialogConfirm';

const { basisDialogScreen } = require('../general/themes');
const theme1 = createMuiTheme(basisDialogScreen);

const Axios = require('axios');

class ProductsSold extends Component {
  state = {
    rowData: [],
    buttonsDisabled: true,
    selectRow: {},
    dialogConfirmOpen: false
  };
  
  componentDidMount() {
  	this.dataUpdate();
  }

  dataUpdate(){
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get_sold_phones' , {},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
    	console.log(JSON.stringify(result['data'], null, 2));
      if (result['data']){
      	this.setState({
      		rowData: [ ...result['data'] ]
      	})
      }
    });
  }

  onGridReady = params => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  };

  agGridClick = () => {
  	const selectedRows = this.gridApi.getSelectedRows();

  	if (selectedRows.length > 0) {
	  	this.setState({
	  		selectRow: {...selectedRows[0]},
	  		buttonsDisabled: false
	  	})
  	} else {
  		this.setState({
	  		selectRow: {},
				buttonsDisabled: true
	  	})
  	}
  }

  clickDelete = () => {
  	let id = this.state.selectRow['id'];
  	//console.log(JSON.stringify(this.state.selectRow, null, 2));

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/delete-sold-phone' , {id},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result['data']){
      	this.gridApi.deselectAll();
      	this.setState({
	  			selectRow: {},
					buttonsDisabled: true
	  		})
	  		this.dataUpdate();
      	this.refs.notifyC.notify('success','Success');
      }
    });
  }

  render() {
    const {rowData, buttonsDisabled} = this.state;

    return (
    	<div>
    	<MuiThemeProvider theme={theme1}>
    	  <Button 
	    		variant="contained" 
	    		size="small"
	    		disabled = {buttonsDisabled}
	    		onClick={this.clickDelete}
			  > 
		      Delete
		    </Button>

    	<div 
			className="ag-theme-balham"
    	style={{ 
        height: '400px', 
        width: '100%',
        marginTop: '20px' 
      }} 
      >
    		<AgGridReact
          columnDefs={sold_phones}
          rowData={rowData}
          onGridReady={this.onGridReady}
          rowSelection = {'single'}
          onRowClicked={this.agGridClick}
        />
    	</div>
    	</MuiThemeProvider>

    	<Notify ref='notifyC' {...this.props} />
    	</div>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(ProductsSold);