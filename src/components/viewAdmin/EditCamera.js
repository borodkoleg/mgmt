import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Notify from '../general/Notify';
import { Redirect } from 'react-router-dom'

const { isIntegerPositiveOrMinusOne } = require('../../server/libs/validation');
const { basisDialogScreen } = require('../general/themes');
const theme1 = createMuiTheme(basisDialogScreen);
const Axios = require('axios');

let startInputsValue = {
	sku: '!startData!',
	brand: '!startData!',
	model: '!startData!',
	serial_number: '!startData!',
	shutter_count: '!startData!',
	condition: '!startData!',
	warranty: '!startData!',
	accessories: '!startData!',
	category_id: '!startData!'
};

class EditCamera extends Component {
  state = {
    inputsValue: startInputsValue,
    categoryArray: [],
    buttonBack: false
  };
  
  componentDidMount() {
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-categories' , {index: 5},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result){
      	this.setState({
      		categoryArray: [ {id: "", name: ""}, ...result['data'] ]
      	})
      }
    })
  }

  componentDidUpdate(){
	  window.onpopstate  = (e) => {
	 		if (e.currentTarget.location.pathname === '/edit-camera'){
	 			this.setState({
	 				buttonBack: true
	 			})
	 		}
	  }
  }

  inputChange = (data, key) => {
  	let newData = data.target.value;

  	this.setState(prevState => {
		  let obj = Object.assign({}, prevState.inputsValue);
		  obj[key] = newData.toUpperCase();              
		  return { inputsValue: obj };
		})
  }

  removeNotInitialize(obj){
  	let objRezult = {...obj}
  	for (var key in objRezult) {
  		if (!objRezult[key]){
				objRezult[key] = null
  		}
  		if (objRezult[key] === '!startData!'){
				delete objRezult[key]
  		}
  	}

  	if (Object.entries(objRezult).length === 0 && objRezult.constructor === Object){
  		return false;
  	}

  	return objRezult;
  }

  editSellerClick = () => {
  	let obj = this.state.inputsValue;
  	let id = this.props.location.state.selectRow['id'];

  	let data = this.removeNotInitialize(obj);
  	if (data === false){
  		this.setState({
      		inputsValue: startInputsValue
      	})
      this.refs.notifyC.notify('success','Success');
      this.props.history.push({
				pathname: '/all-cameras'
			})
      return;
  	}

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/camera-edit' , {data, id},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result['data']){
      	this.setState({
      		inputsValue: startInputsValue
      	})
      	this.refs.notifyC.notify('success','Success');
      	this.props.history.push({
				  pathname: '/all-cameras'
				})
      } else {
      	this.refs.notifyC.notify('error','This item already exists');
      }
    })
  }

  forFieldValue(key){
  	let inputsValue = this.state.inputsValue;
  	let selectRow = this.props.location.state.selectRow;

  	//console.log(JSON.stringify(selectRow, null, 2));
  	//console.log(inputsValue[key]);
  	//console.log(startInputsValue[key]);
  	if (inputsValue[key] !== startInputsValue[key])
  	{
  		return inputsValue[key];
  	} else {
  		if (selectRow[key]){
				return selectRow[key];
  		} else {
  			return '';
  		}
  	}
  }

  render() {
    //const {} = this.props;
    const {inputsValue, categoryArray, buttonBack} = this.state;
    //console.log(JSON.stringify(inputsValue, null, 2));

    if (buttonBack){
    	return <Redirect to='/all-cameras'/>;
    }

    if (!this.props.location.state){
    	this.props.history.push('/all-cameras');
    	return null;
    } else {
    	//let selectRow = this.props.location.state.selectRow;
    }

    //console.log(JSON.stringify(inputsValue, null, 2));
    //console.log(JSON.stringify(selectRow, null, 2));

    return (
    		<Grid container spacing={2}>
    		<MuiThemeProvider theme={theme1}>
	    		<Grid item xs={12} sm={6} md={4}>
		        <div className={'block'}>
		        <div className={'text-headline'}>
		        	Edit Camera
		        </div>

				    <TextField
			        placeholder="SKU"
			        variant="outlined"
			        label="SKU"
			        value={this.forFieldValue('sku')}
			        name="sku"
			        onChange={(value, key) => this.inputChange(value, 'sku')}
			        error={inputsValue['sku']===''}
				    />

				    <TextField
				    	label="Brand"
			        placeholder="Brand"
			        variant="outlined"
			        value={this.forFieldValue('brand')}
			        name="brand"
			        onChange={(value, key) => this.inputChange(value, 'brand')}
			        error={inputsValue['brand']===''}
					  />

					  <TextField
					  	label="Model"
			        placeholder="Model"
			        variant="outlined"
			        value={this.forFieldValue('model')}
			        name="model"
			        onChange={(value, key) => this.inputChange(value, 'model')}
			        error={inputsValue['model']===''}
			      />

			      <TextField
			      	label="Serial Number"
			        placeholder="Serial Number"
			        variant="outlined"
			        value={this.forFieldValue('serial_number')}
			        name="serial_number"
			        onChange={(value, key) => this.inputChange(value, 'serial_number')}
			        error={inputsValue['serial_number']===''}
			      />

			      <TextField
			      	label="Shutter Count"
			        placeholder="Shutter Count"
			        variant="outlined"
			        value={this.forFieldValue('shutter_count')}
			        name="shutter_count"
			        onChange={(value, key) => this.inputChange(value, 'shutter_count')}
			         error={!isIntegerPositiveOrMinusOne(inputsValue['shutter_count'])}
			      />

			      <TextField
			      	label="Condition"
			        placeholder="Condition"
			        variant="outlined"
			        value={this.forFieldValue('condition')}
			        name="condition"
			        onChange={(value, key) => this.inputChange(value, 'condition')}
			      />

			      <TextField
			      	label="Warranty"
			        placeholder="Warranty"
			        variant="outlined"
			        value={this.forFieldValue('warranty')}
			        name="warranty"
			        onChange={(value, key) => this.inputChange(value, 'warranty')}
			      />

			      <TextField
			      	label="Accessories"
			        placeholder="Accessories"
			        variant="outlined"
			        value={this.forFieldValue('accessories')}
			        name="accessories"
			        onChange={(value, key) => this.inputChange(value, 'accessories')}
			      />

			      <TextField
		        select
		        className={'inputWithSelect'}
		        label="Category"
		        value={this.forFieldValue('category_id')}
		        onChange={(value, key) => this.inputChange(value, 'category_id')}
		        SelectProps={{
		          native: true
		        }}
			      >
			        {categoryArray.map(option => (
			          <option key={option.id} value={option.id}>
			            {option.name}
			          </option>
			        ))}
	      		</TextField>
				   
				    <br/>
					  <Button 
		    		variant="contained" 
		    		size="medium"	    		
		    		onClick={this.editSellerClick}
		    		disabled={
		    				inputsValue['sku']!=='' &&
		    				inputsValue['brand']!=='' &&
		    				inputsValue['model']!=='' &&
		    				inputsValue['serial_number']!=='' &&
		    				isIntegerPositiveOrMinusOne(inputsValue['shutter_count'])
		    			? false
		    			: true
		    		}
					  > 
				      Edit
				    </Button>
		        
		        </div>
		      </Grid>
		    </MuiThemeProvider>
		    <Notify ref='notifyC' {...this.props} />
	      </Grid>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(EditCamera);