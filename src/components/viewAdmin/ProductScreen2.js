import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

const { isNumberOrNull } = require('../../server/libs/validation');

const { dateTimeFormat } = require('../../server/libs/date');

const { basisDialogScreen } = require('../general/themes');
const theme1 = createMuiTheme(basisDialogScreen);

const Axios = require('axios');

class ProductScreen2 extends Component {
  state = {
    dateSentOpen: false,
    dateReturnOpen: false
  };
  
  componentDidMount() {
  	
  }

  componentDidUpdate(){
  	//start every update
  }

  saveData = (next) => {
  	let phone = this.props.phone;
  	let data = {};
  	data['repair_cost'] = phone['repair_cost'];
  	data['repair_place'] = phone['repair_place'];
  	data['repair_date_sent'] = dateTimeFormat(phone['repair_date_sent'], 'YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD HH:mm:ss');
  	data['repair_date_return'] = dateTimeFormat(phone['repair_date_return'], 'YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD HH:mm:ss');
  	data['id'] = phone['id'];

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/save-form-screen-3' , {
  			data: data,
  		},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
	    })
	    .then((result) => {
	      if (result['data']){
	      	this.props.sendMessage('success','Success');
	      	//console.log(next);
	      	if (next){
	      		this.props.tabValue(2);
	      		this.props.dataUpdate(result['data']);
	      	} else { //save
	     //  		this.props.changeAll({
						// 	addPhoneManuallyStart: false,
						// 	tabValue: 0,
						// 	tabsShow: false
						// })
	      		
	     //  		this.props.dataUpdate();
	     //  		this.props.deselectAllRowsBasis();
	     			this.props.history.push('/all-products');
	      	}
	    	}
	  });
  }

  onGridReady = params => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    //this.gridColumnApi.getColumn('device_updated_date').setSort("desc")
  };

  onSelectionChanged = () => {
  	const selectedRow = this.gridApi.getSelectedRows();

  	this.setState({
  		selectedRow: {...selectedRow[0]}
  	});
  }

	dateSentChange = (date) => {
		let phone = this.props.phone;
		phone['repair_date_sent'] = date;
		this.props.phoneChange(phone);
		this.setState({
  		dateSentOpen: false
  	})
	}

	dateReturnChange = (date) => {
		let phone = this.props.phone;
		phone['repair_date_return'] = date;
		this.props.phoneChange(phone);
		this.setState({
  		dateReturnOpen: false
  	})
	}

	dateSentOpen = () => {
  	this.setState({
  		dateSentOpen: true
  	})
	}

	dateReturnOpen = () => {
  	this.setState({
  		dateReturnOpen: true
  	})
	}

	inputChange = (data, key) => {
		let phone = this.props.phone;
		phone[key] = (data.target.value).toUpperCase();
		this.props.phoneChange(phone);
	}

	backClick = () => {
  	this.props.tabValue(0);
  }

  render() {
    const {dateSentOpen, dateReturnOpen} = this.state;
    //console.log(JSON.stringify(inputsValue, null, 2));

    return (
    	<Grid container>
    		<MuiThemeProvider theme={theme1}>
    		<Grid item xs={12} sm={12} md={12}>
    			<div className='fixMaxWidth'>
  				<TextField
		        variant="outlined"
		        label="Cost"
		        name="repair_cost"
		        value={this.props.phone['repair_cost'] || ''}
		        onChange={(value, key) => this.inputChange(value, 'repair_cost')}
		        error={!isNumberOrNull(this.props.phone['repair_cost'])}
	        />

	       	<TextField
		        variant="outlined"
		        label="Place"
		        name="repair_place"
		        value={this.props.phone['repair_place'] || ''}
		        onChange={(value, key) => this.inputChange(value, 'repair_place')}
		        //error={!this.props.phone['imei']}
		       />

	        <MuiPickersUtilsProvider utils={DateFnsUtils}>
		    		<KeyboardDatePicker
		    			style={{
		    				marginTop: '0',
    						marginBottom: '0',
    						maxWidth: '230px'}}
		          disableToolbar
		          variant="inline"
		          format="MM/dd/yyyy"
		          margin="normal"
		          open={dateReturnOpen}
		          label="Date Return"
		          value={this.props.phone['repair_date_return'] || null}
		          onChange={this.dateReturnChange}
		          onOpen={this.dateReturnOpen}
		          className="datePickerBasis"
		          KeyboardButtonProps={{
		            'aria-label': 'change date',
		          }}
		        />
		      </MuiPickersUtilsProvider> 

		      <MuiPickersUtilsProvider utils={DateFnsUtils}>
		    		<KeyboardDatePicker
		          disableToolbar
		          style={{
		    				marginTop: '0',
    						marginBottom: '0',
    						maxWidth: '230px'}}
		          variant="inline"
		          format="MM/dd/yyyy"
		          margin="normal"
		          open={dateSentOpen}
		          label="Date Sent"
		          value={this.props.phone['repair_date_sent'] || null}
		          onChange={this.dateSentChange}
		          onOpen={this.dateSentOpen}
		          className="datePickerBasis"
		          KeyboardButtonProps={{
		            'aria-label': 'change date',
		          }}
		        />
	      	</MuiPickersUtilsProvider>
	      	</div>
    		</Grid>
    		<Grid container style={{marginTop:'40px'}}>
    		<Grid item xs={12} sm={12} md={12}>
    		<Button 
	    		variant="contained" 
	    		size="medium"	    		
	    		onClick={this.backClick}
			  > 
		      Back
		    </Button>
    		<Button 
	    		variant="contained" 
	    		size="medium"	    
	    		style={{marginLeft: '40px'}}		
	    		onClick={() => this.saveData(false)}
	    		disabled={!isNumberOrNull(this.props.phone['repair_cost'])}
			  > 
		      Save
		    </Button>
		    <Button 
	    		variant="contained" 
	    		size="medium"	
	    		style={{marginLeft: '20px'}}
	    		disabled={!isNumberOrNull(this.props.phone['repair_cost'])}
	    		onClick={() => this.saveData(true)}
			  > 
		      Save & Next
		    </Button>
		    </Grid>
		    </Grid>
		    </MuiThemeProvider>
    	</Grid>
    )
  }
}

export default connect(
  state => ({
    globalState: state,
    phone: state.basisAgGrid.phone,
    addPhoneManuallyStart: state.basisAgGrid.addPhoneManuallyStart
  }),
  dispatch => ({
    tabsShow: (data) => {
    	dispatch({ type: 'BA_TABS_SHOW', payload: data });
    },
    tabValue: (data) => {
    	dispatch({ type: 'BA_TAB_VALUE', payload: data });
    },
    selectedRows: (data) => {
    	dispatch({ type: 'BA_SELECTED_ROWS', payload: data });
    },
    phoneChange: (data) => {
    	dispatch({ type: 'BA_PHONE', payload: data });
    },
    addManually: (data) => {
    	dispatch({ type: 'BA_ADD_MANUALLY', payload: data });
    },
    changeAll: (data) => {
    	dispatch({ type: 'BASIS_GRID_CHANGE_ALL', payload: data });
    }
  })
)(ProductScreen2);