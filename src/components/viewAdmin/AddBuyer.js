import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Notify from '../general/Notify';

const { basisDialogScreen } = require('../general/themes');
const theme1 = createMuiTheme(basisDialogScreen);
const Axios = require('axios');

let startInputsValue = {
	buyer_name: '',
	buyer_tel: '',
	buyer_email: '',
	category_id: ''
};

class AddBuyer extends Component {
  state = {
    inputsValue: startInputsValue,
    categoryArray: []
  };
  
  componentDidMount() {
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-categories' , {index: 3},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result){
      	this.setState({
      		categoryArray: [ {id: "", name: ""}, ...result['data'] ]
      	})
      }
    })
  }

  inputChange = (data, key) => {
  	let newData = data.target.value;

  	this.setState(prevState => {
		  let obj = Object.assign({}, prevState.inputsValue);
		  obj[key] = newData.toUpperCase();              
		  return { inputsValue: obj };
		})
  }

  addSellerClick = () => {
  	let obj = this.state.inputsValue;
  	if (!obj['category_id']) {
  		delete obj.category_id
  	}

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/add-buyer' , {data: obj},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result['data']){
      	this.setState({
      		inputsValue: startInputsValue
      	})
      	this.refs.notifyC.notify('success','Success');
      } else {
      	this.refs.notifyC.notify('error','This buyer already exists');
      }
    })
  }

  render() {
    //const {} = this.props;
    const {inputsValue, categoryArray} = this.state;

    //console.log(JSON.stringify(inputsValue, null, 2));

    return (
    		<Grid container spacing={2}>
    		<MuiThemeProvider theme={theme1}>
	    		<Grid item xs={12} sm={6} md={4}>
		        <div className={'block'}>
		        <div className={'text-headline'}>
		        	Add Buyer
		        </div>

		        <TextField
			        placeholder="Name"
			        variant="outlined"
			        label="Name"
			        value={inputsValue['buyer_name']}
			        name="buyer_name"
			        onChange={(value, key) => this.inputChange(value, 'buyer_name')}
			        error={inputsValue['buyer_name']===''}
				    />

				    <TextField
				    	label="Email"
			        placeholder="Email"
			        variant="outlined"
			        value={inputsValue['buyer_email']}
			        name="buyer_email"
			        onChange={(value, key) => this.inputChange(value, 'buyer_email')}
			        error={inputsValue['buyer_email']===''}
					  />

					  <TextField
					  	label="Tel"
			        placeholder="Tel"
			        variant="outlined"
			        value={inputsValue['buyer_tel']}
			        name="buyer_tel"
			        onChange={(value, key) => this.inputChange(value, 'buyer_tel')}
			      />

			      <TextField
		        select
		        label="Category"
		        value={inputsValue['category_id']}
		        onChange={(value, key) => this.inputChange(value, 'category_id')}
		        SelectProps={{
		          native: true
		        }}
			      >
			        {categoryArray.map(option => (
			          <option key={option.id} value={option.id}>
			            {option.name}
			          </option>
			        ))}
	      		</TextField>

				    <br/>
					  <Button 
		    		variant="contained" 
		    		size="medium"	    		
		    		onClick={this.addSellerClick}
		    		disabled={
		    				inputsValue['buyer_name']!=='' &&
		    				inputsValue['buyer_email']!==''
		    			? false
		    			: true
		    		}
					  > 
				      Add
				    </Button>
		        
		        </div>
		      </Grid>
		    </MuiThemeProvider>
		    <Notify ref='notifyC' {...this.props} />
	      </Grid>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(AddBuyer);