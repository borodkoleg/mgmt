import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Notify from '../general/Notify';

const { isIntegerPositiveOrNull } = require('../../server/libs/validation');
const { basisDialogScreen } = require('../general/themes');
const theme1 = createMuiTheme(basisDialogScreen);
const Axios = require('axios');

let startInputsValue = {
	sku: '',
	brand: '',
	model: '',
	serial_number: '',
	condition: '',
	warranty: '',
	accessories: '',
	category_id: ''
};

class AddOther extends Component {
  state = {
    inputsValue: startInputsValue,
    categoryArray: []
  };
  
  componentDidMount() {
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-categories' , {index: 6},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result){
      	this.setState({
      		categoryArray: [ {id: "", name: ""}, ...result['data'] ]
      	})
      }
    })
  }

  inputChange = (data, key) => {
  	let newData = data.target.value;

  	this.setState(prevState => {
		  let obj = Object.assign({}, prevState.inputsValue);
		  obj[key] = newData.toUpperCase();              
		  return { inputsValue: obj };
		})
  }

  addSellerClick = () => {
  	let obj = this.state.inputsValue;
  	if (!obj['category_id']) {
  		delete obj.category_id
  	}

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/add-other' , {data: obj},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result['data']){
      	this.setState({
      		inputsValue: startInputsValue
      	})
      	this.refs.notifyC.notify('success','Success');
      } else {
      	this.refs.notifyC.notify('error','This item already exists');
      }
    })
  }

  render() {
    //const {} = this.props;
    const {inputsValue, categoryArray} = this.state;

    //console.log(JSON.stringify(inputsValue, null, 2));

    return (
    		<Grid container spacing={2}>
    		<MuiThemeProvider theme={theme1}>
	    		<Grid item xs={12} sm={6} md={4}>
		        <div className={'block'}>
		        <div className={'text-headline'}>
		        	Add Other
		        </div>

		        <TextField
			        placeholder="SKU"
			        variant="outlined"
			        label="SKU"
			        value={inputsValue['sku']}
			        name="sku"
			        onChange={(value, key) => this.inputChange(value, 'sku')}
			        error={inputsValue['sku']===''}
				    />

				    <TextField
				    	label="Brand"
			        placeholder="Brand"
			        variant="outlined"
			        value={inputsValue['brand']}
			        name="brand"
			        onChange={(value, key) => this.inputChange(value, 'brand')}
			        error={inputsValue['brand']===''}
					  />

					  <TextField
					  	label="Model"
			        placeholder="Model"
			        variant="outlined"
			        value={inputsValue['model']}
			        name="model"
			        onChange={(value, key) => this.inputChange(value, 'model')}
			        error={inputsValue['model']===''}
			      />

			      <TextField
			      	label="Serial Number"
			        placeholder="Serial Number"
			        variant="outlined"
			        value={inputsValue['serial_number']}
			        name="serial_number"
			        onChange={(value, key) => this.inputChange(value, 'serial_number')}
			        error={inputsValue['serial_number']===''}
			      />

			      <TextField
			      	label="Condition"
			        placeholder="Condition"
			        variant="outlined"
			        value={inputsValue['condition']}
			        name="condition"
			        onChange={(value, key) => this.inputChange(value, 'condition')}
			      />

			      <TextField
			      	label="Warranty"
			        placeholder="Warranty"
			        variant="outlined"
			        value={inputsValue['warranty']}
			        name="warranty"
			        onChange={(value, key) => this.inputChange(value, 'warranty')}
			      />

			      <TextField
			      	label="Accessories"
			        placeholder="Accessories"
			        variant="outlined"
			        value={inputsValue['accessories']}
			        name="accessories"
			        onChange={(value, key) => this.inputChange(value, 'accessories')}
			      />

			      <TextField
		        select
		        className={'inputWithSelect'}
		        label="Category"
		        value={inputsValue['category_id'] || ''}
		        onChange={(value, key) => this.inputChange(value, 'category_id')}
		        SelectProps={{
		          native: true
		        }}
			      >
			        {categoryArray.map(option => (
			          <option key={option.id} value={option.id}>
			            {option.name}
			          </option>
			        ))}
	      		</TextField>

				    <br/>
					  <Button 
		    		variant="contained" 
		    		size="medium"	    		
		    		onClick={this.addSellerClick}
		    		disabled={
		    				inputsValue['sku']!=='' &&
		    				inputsValue['brand']!=='' &&
		    				inputsValue['model']!=='' &&
		    				inputsValue['serial_number']!==''
		    			? false
		    			: true
		    		}
					  > 
				      Add
				    </Button>
		        
		        </div>
		      </Grid>
		    </MuiThemeProvider>
		    <Notify ref='notifyC' {...this.props} />
	      </Grid>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(AddOther);