import React, {Component} from 'react';
import { connect } from 'react-redux';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import Notify from '../general/Notify';

import ProductScreen1 from './ProductScreen1';
import ProductScreen2 from './ProductScreen2';
import ProductScreen3 from './ProductScreen3';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
const { tabs } = require('../general/themes');
const theme3 = createMuiTheme(tabs);

const Axios = require('axios');

class AddProduct extends Component {
	state = {
		rowData: []
	}

	dataUpdate(selectId) {
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get_products' , {date: this.state.selectedDate},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result['data']){
      	this.setState({
      		rowData: [ ...result['data'] ]
      	});
      }
    });
  }

	componentDidMount() {
		this.dataUpdate();

		this.props.changeAll({
			addPhoneManuallyStart: true,
			tabValue: 0,
			tabsShow: true,
  		phone: false,
  	})
	}

	sendMessage(status, text){
  	this.refs.notifyC.notify(status,text);
  }

	render() {
		const {rowData} = this.state;
		//const {} = this.props;
		console.log(JSON.stringify(this.props.globalState.basisAgGrid, null, 2))

		return (
			<MuiThemeProvider theme={theme3}>
    		<div className={'basis-center-forms'}>
	    	 	<Tabs
	          value={this.props.globalState.basisAgGrid.tabValue}
	          //onChange={this.handleTabChange}
	          indicatorColor="primary"
	          textColor="primary"
	          variant="fullWidth"
	          style={!this.props.globalState.basisAgGrid.tabsShow ? {display: 'none'} : {}}
	          aria-label="full width tabs example"
	          
        	>
          <Tab disabled label="Product Details" style={{textAlign:'left', background: '#f5f7f7'}} />
          <Tab disabled label="Repair inf." 
          	style={{background: '#f5f7f7'}}
          />
          <Tab disabled label="Status & price" 
          	style={{background: '#f5f7f7'}}
          />
        	</Tabs>
      	
      		{this.props.globalState.basisAgGrid.tabValue === 0 && this.props.globalState.basisAgGrid.tabsShow && (
				    <ProductScreen1
				    	dataUpdate={(select) => this.dataUpdate(select)}
				    	//deselectAllRowsBasis={() => this.deselectAllRowsBasis()}
				    	sendMessage={(d,d1) => this.sendMessage(d,d1)}
				    	basisData={rowData}
				    	history = {this.props.history}
				    />
			    )}

			    {this.props.globalState.basisAgGrid.tabValue === 1 && this.props.globalState.basisAgGrid.tabsShow && ( 	
				    <ProductScreen2
				    	dataUpdate={(select) => this.dataUpdate(select)}
				    	//deselectAllRowsBasis={() => this.deselectAllRowsBasis()}
				    	sendMessage={(d,d1) => this.sendMessage(d,d1)}
				    	history = {this.props.history}
				    />
			    )}

			    {this.props.globalState.basisAgGrid.tabValue === 2 && this.props.globalState.basisAgGrid.tabsShow && ( 	
				    <ProductScreen3
				    	dataUpdate={(select) => this.dataUpdate(select)}
				    	//deselectAllRowsBasis={() => this.deselectAllRowsBasis()}
				    	sendMessage={(d,d1) => this.sendMessage(d,d1)}
				    	history = {this.props.history}
				    />
			    )}
			  </div>
			  <Notify ref='notifyC' {...this.props} />
			</MuiThemeProvider>
		)
	}
	
}

export default connect(
  state => ({
    globalState: state,
  }),
  dispatch => ({
    selectedRows: (data) => {
    	dispatch({ type: 'BA_SELECTED_ROWS', payload: data });
    },
    addManually: (data) => {
    	dispatch({ type: 'BA_ADD_MANUALLY', payload: data });
    },
    tabValue: (data) => {
    	dispatch({ type: 'BA_TAB_VALUE', payload: data });
    },
    tabsShow: (data) => {
    	dispatch({ type: 'BA_TABS_SHOW', payload: data });
    },
   	phone: (data) => {
    	dispatch({ type: 'BA_PHONE', payload: data });
    },
    changeAll: (data) => {
    	dispatch({ type: 'BASIS_GRID_CHANGE_ALL', payload: data });
    }
  })
)(AddProduct);