import React, {Component} from 'react';
import { connect } from 'react-redux';

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { sold_phones } from '../data/fields'

const Axios = require('axios');

class SoldPhones extends Component {
  state = {
    rowData: []
  };
  
  componentDidMount() {
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get_sold_phones' , {},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
    	console.log(JSON.stringify(result, null, 2));
      if (result){
      	this.setState({
      		rowData: [ ...result['data'][0] ]
      	})
      }
    });
  }

  render() {
    const {rowData} = this.state;

    return (
    	<div 
			className="ag-theme-balham"
    	style={{ 
        height: '500px', 
        width: '100%' }} >
    		<AgGridReact
          columnDefs={sold_phones}
          rowData={rowData}>
        </AgGridReact>
    	</div>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(SoldPhones);