import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import Notify from './general/Notify';

import { sellers_info_dialog } from '../data/fields'
const Axios = require('axios');

class SetSeller extends Component {
  state = {
    dialogOpen: false,
    rowData: [],
    buttonSetDisabled: true
  };
  
  componentDidMount() {
  }

  clickSet = () => {
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-all-sellers' , {},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result){
      	//console.log(JSON.stringify(result['data'], null, 2))
	      if (result){
	      	this.setState({
	      		rowData: [ ...result['data'] ],
						dialogOpen: true
	      	})
	      }
      }
    });
  }

  dialogCancel = () => {
  	this.setState({
			dialogOpen: false,
			buttonSetDisabled: true
  	});

  	this.props.deselectAllRows();
  }

  onGridReady = params => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  };

  agGridSelect = event => {
  	// if (this.gridApi.focusedCellController.focusedCellPosition.column.colDef.field === 'imei') {
  		
  	// 	console.log(event);
  	// }
  }

  onSelectionChanged = () => {
  	const selectedRows = this.gridApi.getSelectedRows();
  	if (selectedRows.length > 0){
  		this.setState({
  			buttonSetDisabled: false
  		});
  	} else {
  		this.setState({
  			buttonSetDisabled: true
  		});
  	}
  }

  clickButtonSet = () => {
  	const selectedRows = this.gridApi.getSelectedRows();

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/set-seller' , {
  		phonesArray: this.props.selectedRows,
  		sellerId: selectedRows[0]['id']
  	},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result){
      	this.props.dataUpdate();
      	this.refs.notifyC.notify('success','Success');
      }
      this.setState({
				dialogOpen: false,
				buttonSetDisabled: true
  		});
  		this.props.deselectAllRows();
    });
  }

  render() {
    const {
    	dialogOpen,
    	rowData,
      buttonSetDisabled
    } = this.state;

    return (
    	<div style={{display:'inline-block'}}>
    		<Button 
	    		variant="contained" 
	    		size="small"
	    		style={{top: '-10px', 
	    		left: '40px'}}
	    		disabled = {this.props.disabled}
	    		onClick={this.clickSet}
			  > 
		      Set Seller
		    </Button>

		    <Dialog
	        open={dialogOpen}
	        onClose={this.dialogCancel}
	        fullWidth={true}
	        aria-labelledby="alert-dialog-title"
	        aria-describedby="alert-dialog-description"
	        style={{minWidth: '300px'}}
   			>
   			 	<DialogTitle style={{paddingButtom: '0!important'}}>
   			 		Set Seller
   			 	</DialogTitle>
   			 	<DialogContent>
   			 		<div className="ag-theme-balham agGridReact-for-dialog">
							<AgGridReact 
	        			columnDefs={sellers_info_dialog}
	        			rowData={rowData}
	        			onGridReady={this.onGridReady}
	              rowStyle = {{fontSize: '15px'}}
	    		      onRowClicked={this.agGridSelect}
	    		      onSelectionChanged={this.onSelectionChanged}
	    		      rowSelection = {'single'}
			        >
			        </AgGridReact>
		        </div>
     			 	</DialogContent>
     			 	<DialogActions>
     			 		<Button onClick={this.dialogCancel} color="primary">
		            Cancel
		          </Button>
		          <Button 
		          	onClick={this.clickButtonSet} 
		          	disabled={buttonSetDisabled}
		          	color="primary">
		            Set
		          </Button>
     			 	</DialogActions>
     		</Dialog>
     		<Notify ref='notifyC' {...this.props} />
    	</div>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    globalUpdateSeller: (data) => {
      dispatch({ type: 'UPDATE_SELLER', payload: data });
    }
  })
)(SetSeller);