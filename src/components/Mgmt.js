import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';

import { products_inventory } from '../data/fields'
import Grid from '@material-ui/core/Grid';

import DialogG from './general/DialogG';
//import AddSeller from './AddSeller';
//import AddBuyer from './AddBuyer';
import SetSeller from './SetSeller';
import SetBuyer from './SetBuyer';
//import PhoneData from './PhoneData';
import Notify from './general/Notify';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const { theme, dialogResize } = require('./general/themes');
const theme1 = createMuiTheme(theme);
const theme2 = createMuiTheme(dialogResize);

//const { dayTodayMinusOne } = require('../server/libs/date');

const Axios = require('axios');

class Mgmt extends Component {
  state = {
    rowData: [],
    lastCheckDate: '',
    btnRefreshDisabled: false,
    btnSoldOutDisabled: true,
    btnSetSellerDisabled: true,
    btnSetBuyerDisabled: true,
    dialogSoldOutOpen: false,
    btnPhoneDataDisabled: true,
    dialogSoldText: [],
    getSelectedRowsIds: []
  };
  
  componentDidMount() {
  	this.dataUpdate();
		this.lastDateSettings();
  }

  // setDateSettings(){
  // 	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/set_settings' , {date: dayTodayMinusOne() },
  //   	{
  //       headers: {
  //           "Content-Type": "application/json",
  //           "Authorization": `Bearer ${localStorage.getItem('token')}`
  //       },
  //       params: {}
  //   })
  //   .then((result) => {
  //   	if (result['data'].length > 0){
	 //      this.setState({
	 //      	lastCheckDate: result['data'][0]['last_update']
	 //      })
  //   	}
  //   })
  // }

  lastDateSettings(){
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get_settings' , {},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result['data'].length > 0){
      	this.setState({
      		lastCheckDate: result['data'][0]['last_update']
      	})
      } else {
      	this.setDateSettings();
      }
    })
  }

  dataUpdate() {
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get_products' , {},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result['data']){
      	this.setState({
      		rowData: [ ...result['data'] ]
      	})
      }
    })
  } 

  refreshData = () => {
  	this.setState({
	    btnRefreshDisabled: true
	  })

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-new-phones' , {},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
	    })
	    .then((result) => {
	      if (result){
	      	this.dataUpdate();
	      	this.lastDateSettings();
	      	this.setState({
	    			btnRefreshDisabled: false
	  			})
	      	this.refs.notifyC.notify('success','Success');
	    	}
	  });
  } 

  onGridReady = params => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    //this.gridColumnApi.getColumn('device_updated_date').setSort("desc")
    //this.gridOptions.api.rowStyle = {background: 'black'};
  };

  agGridSelect = event => {
  	// if (this.gridApi.focusedCellController.focusedCellPosition.column.colDef.field === 'imei') {
  		
  	// 	console.log(event);
  	// }
  }

  onSelectionChanged = () => {
  	const selectedRows = this.gridApi.getSelectedRows();
  	//console.log(JSON.stringify(selectedRows));

  	if (selectedRows.length > 0) {
  		this.setState({
	    	btnSoldOutDisabled: false,
	    	btnSetSellerDisabled: false,
	    	btnSetBuyerDisabled: false,
	    	getSelectedRowsIds: [ ...selectedRows ]
	  	});
  	} else {
  		this.setState({
	    	btnSoldOutDisabled: true,
	    	btnSetSellerDisabled: true,
	    	btnSetBuyerDisabled: true,
	    	getSelectedRowsIds: []
	  	})
  	}

  	if (selectedRows.length === 1) {
			this.setState({
  			btnPhoneDataDisabled: false
  		});
	  } else {
	  	this.setState({
  			btnPhoneDataDisabled: true
  		});
	  }


  }

  clickSoldOut = () => {
  	const selectedRows = this.gridApi.getSelectedRows();
  	let arrayImei = [];

  	selectedRows.forEach((obj) => {
  		//console.log(obj.imei);
  		arrayImei.push(obj.imei);
  	});

  	this.setState({
  		dialogSoldOutOpen: true,
  		dialogSoldText: arrayImei
  	})
  }

  deselectAllRows(){
  	this.gridApi.deselectAll();

  	this.setState({
  		btnSoldOutDisabled: true,
    	btnSetSellerDisabled: true,
    	btnSetBuyerDisabled: true,
    	btnPhoneDataDisabled: true
  	})
	}

  clickDialogSoldOut = (data) => {
  	if (data){
  		const selectedRows = this.gridApi.getSelectedRows();

  		Axios.post(process.env.REACT_APP_HOST_PORT + '/api/remove_products' , {selectedRows},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
	    })
	    .then((result) => {
	    	//console.log('result=>' + JSON.stringify(result));
	      if (result['data']){
	      	this.dataUpdate();
	      	this.refs.notifyC.notify('success','Success');
	      } else {
	      	//error
	      }
	      this.setState({
	  			dialogSoldOutOpen: false,
	  			btnSoldOutDisabled: true
	  		})
	  		this.deselectAllRows();
	    })

  		//console.log(JSON.stringify(selectedRows));
  	} else {
  		this.deselectAllRows();
	  	this.setState({
	  		dialogSoldOutOpen: false
	  	})
  	}
  }

  clickDialogSoldOutClose = () => {
  	this.setState({
  		dialogSoldOutOpen: false
  	});
  }

  editPhone = () => {
  	const selectedRows = this.gridApi.getSelectedRows();
  	this.props.phoneChange(selectedRows[0]);
  	this.props.history.push({
		  pathname: '/edit-product'
		})
  }

  render() {
    const {rowData, 
    	//lastCheckDate, 
    	btnRefreshDisabled,
    	btnSoldOutDisabled,
    	dialogSoldOutOpen,
    	dialogSoldText,
    	btnSetSellerDisabled,
    	btnSetBuyerDisabled,
    	getSelectedRowsIds,
    	btnPhoneDataDisabled
    } = this.state;

    return (
    	<Grid container>
    		<Grid item xs={12} sm={12}>
	    	
	    	{/* <div style={{display: 'inline-block', marginRight: '10px'}}>
	    	Last phone update
	    	<br />
	    	{lastCheckDate}
	    	</div> */}
	    	<Button 
	    		variant="contained" 
	    		size="small" 
	    		color="primary"
	    		style={{top: '-10px'}}
	    		disabled={btnRefreshDisabled}
		    	onClick={this.refreshData}
		    >
	        Refresh
		    </Button>
		    
				<Button 
	    		variant="contained" 
	    		size="small" 
	    		color="secondary"
	    		style={{top: '-10px', left: '10px'}}
	    		disabled={btnSoldOutDisabled}
	    		onClick={this.clickSoldOut}
		    > 
	        Sold Out
	      </Button>

	      {/* <MuiThemeProvider theme={theme1}>
		      <AddSeller/>
		      <AddBuyer/>
	      </MuiThemeProvider> */}

	      <MuiThemeProvider theme={theme2}>
		      <SetSeller
		      	disabled={btnSetSellerDisabled}
		      	deselectAllRows={() => this.deselectAllRows()}
		      	selectedRows={getSelectedRowsIds}
		      	dataUpdate={() => this.dataUpdate()}

		      	//ref='refSetSeller'
		      />

		      <SetBuyer
		      	disabled={btnSetBuyerDisabled}
		      	deselectAllRows={() => this.deselectAllRows()}
		      	selectedRows={getSelectedRowsIds}
		      	dataUpdate={() => this.dataUpdate()}
		      	//ref='refSetBuyer'
		      />
	      </MuiThemeProvider>

	      <MuiThemeProvider theme={theme1}>
	      	<Button     		
	    		onClick={this.editPhone}
	    		variant="contained" 
	    		size="small"
	    		style={{top: '-10px', 
	    		left: '60px'}}
	    		disabled = {btnPhoneDataDisabled}
				  > 
			      Edit
			    </Button>
	      	{/* <PhoneData
		      	disabled={btnPhoneDataDisabled}
		      	deselectAllRows={() => this.deselectAllRows()}
		      	selectedRows={getSelectedRowsIds}
		      	dataUpdate={() => this.dataUpdate()}
		      /> */}
	      </MuiThemeProvider>

	      </Grid>

	      <Grid item xs={12} sm={12}>
	    	<div 
				className="ag-theme-balham"
	    	style={{ 
	        height: '444px', 
	        width: '100%' }} >
	    		<AgGridReact
	          columnDefs={products_inventory}
	          rowData={rowData}
	          onGridReady={this.onGridReady}
	          //domLayout={'autoHeight'}
	          rowStyle = {{fontSize: '15px'}}
	      		//headerHeight = {150}
	      		onRowClicked={this.agGridSelect}
	      		// rowClassRules = {{
	      		// 	'grid-green': 'data.age < 20',
	    				// 'grid-grey': 'data.age >= 20 && data.age < 25',
	      		// }}
	      		rowSelection = {'multiple'}
	      		onSelectionChanged={this.onSelectionChanged}
	        >
	        </AgGridReact>
	    	</div>
	    	</Grid>

	    	<DialogG
	    		open={dialogSoldOutOpen}
	    		title={'Are these phones sold?'}
	    		text={dialogSoldText}
	    		clickBtn={(data) => this.clickDialogSoldOut(data)}
	    		close={this.clickDialogSoldOutClose}
	    	/>

	    	<Notify ref='notifyC' {...this.props} />
    	</Grid>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    },
    phoneChange: (data) => {
    	dispatch({ type: 'BA_PHONE', payload: data });
    }
  })
)(Mgmt);