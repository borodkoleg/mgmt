import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import { AgGridReact } from 'ag-grid-react';
import { buyers_info } from '../data/fields';
import Notify from './general/Notify';
const Axios = require('axios');

class SetBuyer extends Component {
  state = {
    dialogOpen: false,
    rowData: [],
    buttonSetDisabled: true
  };
  
  componentDidMount() {
  	
  }

  clickSet = () => {
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-all-buyers' , {},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result){
      	//console.log(JSON.stringify(result['data'], null, 2))
	      if (result){
	      	this.setState({
	      		rowData: [ ...result['data'] ],
  					dialogOpen: true
	      	})
	      }
      }
    });
  }

  dialogCancel = () => {
  	this.setState({
			dialogOpen: false,
			buttonSetDisabled: true
  	});

  	this.props.deselectAllRows();
  }

  clickButtonSet = () => {
  	const selectedRows = this.gridApi.getSelectedRows();

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/set-buyer' , {
  		phonesArray: this.props.selectedRows,
  		buyerId: selectedRows[0]['id']
  	},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result){
      	this.props.dataUpdate();
      	this.refs.notifyC.notify('success','Success');
      }
      this.setState({
				dialogOpen: false,
				buttonSetDisabled: true
  		});
  		this.props.deselectAllRows();
    });
  }

  onSelectionChanged = () => {
  	const selectedRows = this.gridApi.getSelectedRows();
  	if (selectedRows.length > 0){
  		this.setState({
  			buttonSetDisabled: false
  		});
  	} else {
  		this.setState({
  			buttonSetDisabled: true
  		});
  	}
  }

  onGridReady = params => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  };

  render() {
    const { 
    	dialogOpen,
    	rowData,
    	buttonSetDisabled
   	} = this.state;

    return (
    	<div style={{display:'inline-block'}}>
    		<Button 
	    		variant="contained" 
	    		size="small"
	    		style={{top: '-10px', 
	    		left: '50px'}}
	    		disabled = {this.props.disabled}
	    		onClick={this.clickSet}
			  > 
		      Set Buyer
		    </Button>

		    <Dialog
	        open={dialogOpen}
	        onClose={this.dialogCancel}
	        fullWidth={true}
	        aria-labelledby="alert-dialog-title"
	        aria-describedby="alert-dialog-description"
	        style={{minWidth: '300px'}}
   			>
   			 	<DialogTitle style={{paddingButtom: '0!important'}}>
   			 		Set Seller
   			 	</DialogTitle>
   			 	<DialogContent>
   			 		<div className="ag-theme-balham agGridReact-for-dialog">
							<AgGridReact 
	        			columnDefs={buyers_info}
	        			rowData={rowData}
	        			onGridReady={this.onGridReady}
	              rowStyle = {{fontSize: '15px'}}
	    		      onRowClicked={this.agGridSelect}
	    		      onSelectionChanged={this.onSelectionChanged}
	    		      rowSelection = {'single'}
			        >
			        </AgGridReact>
		        </div>
     			 	</DialogContent>
     			 	<DialogActions>
     			 		<Button onClick={this.dialogCancel} color="primary">
		            Cancel
		          </Button>
		          <Button 
		          	onClick={this.clickButtonSet} 
		          	disabled={buttonSetDisabled}
		          	color="primary">
		            Set
		          </Button>
     			 	</DialogActions>
     		</Dialog>
     		<Notify ref='notifyC' {...this.props} />
    	</div>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(SetBuyer);