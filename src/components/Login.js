import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
require('dotenv').config()
const Axios = require('axios');

class Login extends Component {
  state = {
    login: '',
    password: ''
  };
  
  componentDidMount() {

  }

  sendData(){
  	let password = this.state.password;
  	let login = this.state.login;
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/user_check' , {
          password: password,
          login: login
    	},
    	{
        headers: {
            "Content-Type": "application/json"
        },
        params: {}
    })
    .then((res) => {
			this.props.tokenChange(res.data);
			localStorage.setItem('token', res.data);
			this.props.history.push('/basis');      
    })
    .catch(err => {
      this.props.tokenChange(false);
      localStorage.removeItem('token');
    });
  }

  buttonClick = () => {
  	this.sendData();
  }

  buttonDown = (event) => {
  	if (event.key === 'Enter' && this.state.password!=='' && this.state.login!=='') {
      this.sendData();
    }
  }

  handleLogin = (event) => {
  	this.setState({
      login: event.target.value
    })
  }

  handlePassword = (event) => {
  	this.setState({
      password: event.target.value
    })
  }

  render() {
    const {login, password} = this.state;

    return (
    	<div style={{margin: '0 auto', maxWidth: '200px', textAlign: 'center'}}>
		    	<TextField
		        label="Login"
		        value={login}
		        onChange={this.handleLogin}
		        margin="normal"
		        onKeyDown={this.buttonDown}
		        error={login===''}
		      />
		    <br />  
		      <TextField
		        label="Password"
		        value={password}
		        onChange={this.handlePassword}
		        margin="normal"
		        onKeyDown={this.buttonDown}
		        error={password===''}
		        type="password"
		      />
		    <br />
		    <br />
		    <Button 
		    	variant="contained" 
		    	style={{float:'left'}}
		    	disabled={password==='' || login===''}
		    	onClick={this.buttonClick}
		    	onKeyDown={this.buttonDown}
		    	//ref={(input) => { this.sendButton = input; }}
		    	color="primary">
        	SEND
      	</Button>
    	</div>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(Login);