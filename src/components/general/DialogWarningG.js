import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

export default class DialogWarningG extends Component {
	state = {
		
	}

  render() {
    const {title, text} = this.props;

  return (
    <div>
      <Dialog
        open={this.props.open}
        onClose={this.props.close}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle style={{paddingButtom: '0!important'}}>{title}</DialogTitle>
        <DialogContent>
      		{text}
        </DialogContent>
        <DialogActions>
          <Button onClick={(data) => this.props.clickBtn(true)} color="primary">
            I see
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
	}
}