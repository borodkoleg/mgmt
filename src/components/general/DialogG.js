import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

export default class DialogG extends Component {
	state = {
		
	}

  render() {
    const {title, text} = this.props;

  return (
    <div>
      <Dialog
        open={this.props.open}
        onClose={this.props.close}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle style={{paddingButtom: '0!important'}}>{title}</DialogTitle>
        <DialogContent>
            <ul>
      				{text.map((value, i) => <li key={i}>{value}</li>)}
    				</ul>
        </DialogContent>
        <DialogActions>
          <Button onClick={(data) => this.props.clickBtn(false)} color="primary">
            Cancel
          </Button>
          <Button onClick={(data) => this.props.clickBtn(true)} color="primary">
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
	}
}