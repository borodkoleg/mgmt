import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const { basisDialogScreen } = require('../general/themes');
const theme1 = createMuiTheme(basisDialogScreen);

let nameStart = -1;

export default class DialogOneField extends Component {
	state = {
		name: nameStart
	}

	inputChange = (data) => {
		let value = data.target.value;

		this.setState({
			name: value.toUpperCase()
		})
	}

	close = () => {
		this.setState({
    	name: nameStart
		}, () => {
		  this.props.close();
		});
	}

	clickBtn = () => {
		let nowName = this.state.name;
		this.setState({
    	name: nameStart
		}, () => {
			if (nowName !== nameStart){
		  	this.props.clickBtn(nowName);
			} else {
				this.props.close();
			}
		});
	}

  render() {
    const {title} = this.props;
    const {name} = this.state;

  return (
    <MuiThemeProvider theme={theme1}>
      <Dialog
        open={this.props.open}
        onClose={this.close}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle className={'text-headline'} style={{paddingButtom: '0!important'}}>{title}</DialogTitle>
        <DialogContent>
          <TextField
		        placeholder={this.props.placeholder ? this.props.placeholder : "Name"}
		        variant="outlined"
		        value={name!==nameStart ? name : (this.props.value || '')}
		        onChange={(value) => this.inputChange(value)}
		        error={(name && name.length>0) ? false : true}
					/>  
        </DialogContent>
        <DialogActions>
          <Button 
          	onClick={this.close}
          	color="primary"
          	>
            Cancel
          </Button>
          <Button 
          	onClick={this.clickBtn} 
          	color="primary"
          	disabled={(name && name.length>0) ? false : true}
          	>
            Change
          </Button>
        </DialogActions>
      </Dialog>
    </MuiThemeProvider>
  );
	}
}