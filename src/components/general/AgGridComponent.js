import React, {Component} from 'react';
import { AgGridReact } from 'ag-grid-react';


class AgGridComponent extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  render() {
    const {} = this.props;
    const {} = this.state;

    return (
    	<div className="ag-theme-balham">
	    	<AgGridReact>
	    		columnDefs={this.props.columnDefs}
	    		rowData={this.props.rowData}
	    	</AgGridReact>
    	</div>
    )
  }
}

export default AgGridComponent;