import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
const { isNumber } = require('../../server/libs/validation');

const { basisDialogScreen } = require('../general/themes');
const theme1 = createMuiTheme(basisDialogScreen);

export default class DialogOneField extends Component {
	state = {
		name: '',
		reset: ''
	}

	static getDerivedStateFromProps(props, state) {
    if (props.value !== state.reset) {
    	console.log(Math.random());
      return {
        name: props.value,
        reset: props.value
      };
    }
    return null;
  }

	inputChange = (data) => {
		console.log(data.target.value);
		let value = data.target.value;

		this.setState({
			name: value.toUpperCase()
		})
	}

	close = () => {
		this.setState({
			reset: ''
		}, () => {
		  this.props.close();
		});
	}

	clickBtn = () => {
		let value = this.state.name;
		this.setState({
			reset: ''
		}, () => {
			this.props.clickBtn(value);
		});
	}

  render() {
    const {title} = this.props;
    const {name, reset} = this.state;

  return (
    <MuiThemeProvider theme={theme1}>
      <Dialog
        open={this.props.open}
        onClose={this.close}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle className={'text-headline'} style={{paddingButtom: '0!important'}}>{title}</DialogTitle>
        <DialogContent>
          <TextField
		        placeholder={this.props.placeholder ? this.props.placeholder : "Name"}
		        variant="outlined"
		        value={name}
		        onChange={(value) => this.inputChange(value)}
		        error={(isNumber(name) && name <= reset) ? false : true}
					/>  
        </DialogContent>
        <DialogActions>
          <Button 
          	onClick={this.close}
          	color="primary"
          	>
            Cancel
          </Button>
          <Button 
          	onClick={this.clickBtn} 
          	color="primary"
          	disabled={(isNumber(name) && name <= reset ) ? false : true}
          	>
            Change
          </Button>
        </DialogActions>
      </Dialog>
    </MuiThemeProvider>
  );
	}
}