exports.theme = {
 overrides: {
    MuiButton: {
    	contained: {
    		'&:hover':{
    			backgroundColor: '#ffd300',
    			color:'#ffffff'
    		},
    		backgroundColor: '#ffd300',
    		borderColor: '#4cae4c',
    		color: '#000000',
    		textTransform: 'none'
    	}
    },
    MuiDialogContent: {
    	root: {
    		overflowY: 'initial'
    	}
    },
    MuiOutlinedInput: {
    	input: {
    		padding: '8.5px 10px'
    	}
    },
    MuiFormControl: {
    	root: {
    		width: '100%'
    	}
    },
    MuiInputBase: {
    	input:{
    		color: 'black'
    	}
    }
  }
}

exports.dialogResize = {
	overrides: {
		MuiPaper: {
			root: {
				minHeight: '300px'
			}
		},
		MuiButton: {
    	contained: {
    		'&:hover':{
    			color: 'black',
    			backgroundColor: 'green'
    		},
    		backgroundColor: 'green',
    		color: 'white'
    	}
    }
	}
}

exports.basisDialogScreen = {
 overrides: {
    MuiButton: {
    	contained: {
    		'&:hover':{
    			backgroundColor: '#ffd300',
    			color:'#ffffff'
    		},
    		backgroundColor: '#ffd300',
    		borderColor: '#4cae4c',
    		color: '#000000',
    		textTransform: 'none',
    		padding: '4px 8px'
    	}
    },
    MuiDialogContent: {
    	root: {
    		overflowY: 'initial'
    	}
    },
    MuiOutlinedInput: {
    	input: {
    		padding: '7px 10px 9px 10px',
    		marginRight: '5px'
    	}
    },
    MuiFormControl: {
    	root: {
    		//minWidth:'280px'
    	}
    },
    MuiInputLabel: {
    	formControl: {
    		top: '-10px'
    	}
  	},
    MuiInputBase: {
    	root: {
    		marginBottom:'20px',
    		marginRight:'10px'
    	},
    	input:{
    		color: 'black'
    	}
    },
    MuiTypography: {
    	h6: {
    		fontSize: '15px'
    	}
    },
    MuiTextField: {
    	root: {
    		minWidth: '240px'
    	}
    }
  }
}

exports.tabs = {
	overrides: {
		MuiTab: {
			wrapper: {
				color: 'rgba(0, 0, 0, 0.54)',
				alignItems: 'flex-start',
				fontWeight: '600'
			}
		},
		MuiTabs: {
			root: {
				paddingBottom: '40px'
			}
		},
		MuiFormControl: {
			root: {
				marginTop: '50px'
			}
		}
	}
}