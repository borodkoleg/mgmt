import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Notify from '../general/Notify';

const { dayTodayMinusOne, dateTimeFormat } = require('../../server/libs/date');
const Axios = require('axios');

class Mgmt extends Component {
  state = {
    btnRefreshDisabled: false,
    lastCheckDate: ''
  };
  
  componentDidMount() {
  	this.lastDateSettings();
  }

  refreshData = () => {
  	this.setState({
	    btnRefreshDisabled: true
	  })

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-new-phones' , {},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
	    })
	    .then((result) => {
	      if (result){
	      	this.props.dataUpdate();
	      	this.lastDateSettings();
	      	this.setState({
	    			btnRefreshDisabled: false
	  			})
	      	this.refs.notifyC.notify('success','Success');
	    	}
	  });
  }

  lastDateSettings(){
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get_settings' , {},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result['data'].length > 0){
      	this.setState({
      		lastCheckDate: result['data'][0]['last_update']
      	})
      } else {
      	this.setDateSettings();
      }
    })
  }

  setDateSettings(){
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/set_settings' , {date: dayTodayMinusOne() },
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
    	if (result['data'].length > 0){
	      this.setState({
	      	lastCheckDate: result['data'][0]['last_update']
	      })
    	}
    })
  }

  render() {
    const {btnRefreshDisabled, lastCheckDate} = this.state;

    return (
    	<div style={{marginBottom:'6px'}}>
    	{dateTimeFormat(lastCheckDate, 'YYYY-MM-DD HH:mm:ss', 'MM-DD-YYYY HH:mm:ss')}
	    	<Button 
	    		variant="contained" 
	    		size="small" 
	    		color="primary"
	    		disabled={btnRefreshDisabled}
		    	onClick={this.refreshData}
		    	style={{
		    		float: 'right',
		    		position: 'relative',
    				top: '-14px'
		    	}}
			    >
	        Refresh
			  </Button>
			  <Notify ref='notifyC' {...this.props} />
		  </div>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(Mgmt);