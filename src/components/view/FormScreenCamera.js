import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
const { isIntegerPositiveOrNull } = require('../../server/libs/validation');

const { basisDialogScreen } = require('../general/themes');
const theme1 = createMuiTheme(basisDialogScreen);
const Axios = require('axios');

let startInputsValue = {
	sku: '',
	brand: '',
	model: '',
	serial_number: '',
	shutter_count: '',
	condition: '',
	warranty: '',
	accessories: '',
	category_id: ''
};

class FormScreenCamera extends Component {
  state = {
    inputsValue: startInputsValue,
    categoryArray: []
  };
  
  componentDidMount() {
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-categories' , {index: 5},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result){
      	this.setState({
      		categoryArray: [ {id: "", name: ""}, ...result['data'] ]
      	})
      }
    })
  }

  addCamera = () => {
		let obj = this.state.inputsValue;

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/add-camera' , {data: obj},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result['data']){
      	this.props.sendMessage('success','Success');
      	this.setState({
      		inputsValue: startInputsValue
      	})
      	this.props.changeAll({
	  			tabValue: 0
  			})
      } else {
      	this.props.sendMessage('error','This item already exists');
      }
    })
  }

  inputChange = (data, key) => {
  	let newData = data.target.value;

  	this.setState(prevState => {
		  let obj = Object.assign({}, prevState.inputsValue);
		  obj[key] = newData.toUpperCase();              
		  return { inputsValue: obj };
		})
  }

  render() {
    //const {} = this.props;
    const {inputsValue, categoryArray} = this.state;

    //console.log(JSON.stringify(inputsValue, null, 2));

    return (
    		<Grid container>
    		<div className={'basis-camera-block'}>
    		<MuiThemeProvider theme={theme1}>
	    		<Grid item xs={12} sm={12} md={12}>
		        <div className={'basic-camera'}>
		        	Add Camera
		        </div>
		      </Grid>
		      <Grid item xs={12} sm={12} md={12}>
		      <div className='fixMaxWidth'>
		        <TextField
			        placeholder="SKU"
			        variant="outlined"
			        label="SKU"
			        value={inputsValue['sku']}
			        name="sku"
			        onChange={(value, key) => this.inputChange(value, 'sku')}
			        error={inputsValue['sku']===''}
				    />

				    <TextField
			        placeholder="Brand"
			        variant="outlined"
			        label="Brand"
			        value={inputsValue['brand']}
			        name="brand"
			        onChange={(value, key) => this.inputChange(value, 'brand')}
			        error={inputsValue['brand']===''}
				    />

				    <TextField
			        placeholder="Model"
			        variant="outlined"
			        label="Model"
			        value={inputsValue['model']}
			        name="model"
			        onChange={(value, key) => this.inputChange(value, 'model')}
			        error={inputsValue['model']===''}
				    />

				   	<TextField
			        placeholder="Serial Number"
			        variant="outlined"
			        label="Serial Number"
			        value={inputsValue['serial_number']}
			        name="serial_number"
			        onChange={(value, key) => this.inputChange(value, 'serial_number')}
			        error={inputsValue['serial_number']===''}
				    />

				    <TextField
			        placeholder="Shutter Count"
			        variant="outlined"
			        label="Shutter Count"
			        value={inputsValue['shutter_count']}
			        name="shutter_count"
			        onChange={(value, key) => this.inputChange(value, 'shutter_count')}
			        error={!isIntegerPositiveOrNull(inputsValue['shutter_count'])}
				    />

				    <TextField
			        placeholder="Condition"
			        variant="outlined"
			        label="Condition"
			        value={inputsValue['condition']}
			        name="condition"
			        onChange={(value, key) => this.inputChange(value, 'condition')}
			        //error={inputsValue['condition']===''}
				    />

				    <TextField
			        placeholder="Warranty"
			        variant="outlined"
			        label="Warranty"
			        value={inputsValue['warranty']}
			        name="warranty"
			        onChange={(value, key) => this.inputChange(value, 'warranty')}
			        //error={inputsValue['warranty']===''}
				    />

				    <TextField
			        placeholder="Accessories"
			        variant="outlined"
			        label="Accessories"
			        value={inputsValue['accessories']}
			        name="accessories"
			        onChange={(value, key) => this.inputChange(value, 'accessories')}
			        //error={inputsValue['accessories']===''}
				    />

				    <TextField
		        select
		        className={'inputWithSelect'}
		        label="Category"
		        value={inputsValue['category_id'] || ''}
		        onChange={(value, key) => this.inputChange(value, 'category_id')}
		        SelectProps={{
		          native: true
		        }}
			      >
			        {categoryArray.map(option => (
			          <option key={option.id} value={option.id}>
			            {option.name}
			          </option>
			        ))}
	      		</TextField>

				    <br/>
					  <Button 
		    		variant="contained" 
		    		size="medium"	    		
		    		onClick={this.addCamera}
		    		disabled={
		    				inputsValue['sku']!=='' &&
		    				inputsValue['brand']!=='' &&
		    				inputsValue['model']!=='' &&
		    				inputsValue['serial_number']!=='' &&
		    				isIntegerPositiveOrNull(inputsValue['shutter_count'])
		    			? false
		    			: true
		    		}
					  > 
				      Add
				    </Button>
		        </div>
		      </Grid> 
		    </MuiThemeProvider>
		    </div>
	      </Grid>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    },
    changeAll: (data) => {
    	dispatch({ type: 'BASIS_GRID_CHANGE_ALL', payload: data });
    }
  })
)(FormScreenCamera);