import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

//import CreatableSelect from 'react-select/creatable';
import Grid from '@material-ui/core/Grid';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
const { theme } = require('../general/themes');
const theme1 = createMuiTheme(theme);

const Axios = require('axios');

let startInputsValue = {
	seller_name: '',
	seller_email: '',
	seller_tel: '',
	seller_referrel: '',
	ad_type_sold: ''
};

class AddSeller extends Component {
  state = {
    dialogOpen: false,
    inputsValue: startInputsValue
  };
  
  componentDidMount() {
  }

  clickAddSeller = () => {
  	this.setState({
  		dialogOpen: true
  	})
  }

  dialogCancel = () => {
  	this.setState({
  		dialogOpen: false,
  		inputsValue: startInputsValue
  	})
  }

  dialogSave = () => {
  	//inputsValue

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/add-seller' , {data: this.state.inputsValue},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => { //200
      if (result['data']){ 
      	this.setState({
		  		dialogOpen: false,
		  		inputsValue: startInputsValue
		  	});
		  	//this.refs.notifyC.notify('success','Seller created');
		  	this.props.sendMessage('success','Seller created');
		  	this.props.sellersUpdate();
      } else {
      	console.log(result['data']);
      	//this date already exist
      	this.props.sendMessage('error','This seller already exists');
      	//this.refs.notifyC.notify('error','This seller already exists');
      }
    }).catch((error) => { //400
    	console.log(error);
    });
  }

  inputChange = (data, key) => {
  	let newData = data.target.value;

  	this.setState(prevState => {
		  let obj = Object.assign({}, prevState.inputsValue);
		  obj[key] = newData.toUpperCase();              
		  return { inputsValue: obj };
		})
  }

  render() {
    const {
    	dialogOpen,
    	inputsValue
    } = this.state;

    //console.log(JSON.stringify(inputsValue));

    return (
    		<div style={{display:'inline-block'}}>
		      <Button 
		    		variant="contained" 
		    		size="small"
		    		style={{top: '-10px', 
		    		left: '20px'}}
		    		onClick={this.clickAddSeller}
			    > 
		        Add seller
		      </Button>
					<Dialog
		        open={dialogOpen}
		        onClose={this.dialogCancel}
		        fullWidth={true}
		        aria-labelledby="alert-dialog-title"
		        aria-describedby="alert-dialog-description"
		        style={{minWidth: '300px'}}
     			 >
	   			 	<DialogTitle style={{paddingButtom: '0!important'}}>Add seller
	   			 	</DialogTitle>
	   			 	<DialogContent>
						  <MuiThemeProvider theme={theme1}>
							<Grid container spacing={1}>
								<Grid item lg={6} xs={12}>
									<TextField
						        placeholder="Name"
						        variant="outlined"
						        value={inputsValue['seller_name']}
						        name="seller_name"
						        onChange={(value, key) => this.inputChange(value, 'seller_name')}
						        error={inputsValue['seller_name']===''}
							    />
					      </Grid>
					      <Grid item lg={6} xs={12}>
						      <TextField
						        placeholder="Email"
						        variant="outlined"
						        value={inputsValue['seller_email']}
						        name="seller_email"
						        onChange={(value, key) => this.inputChange(value, 'seller_email')}
						        error={inputsValue['seller_email']===''}
								  />
					      </Grid>
					      <Grid item lg={6} xs={12}>
					      	<TextField
						        placeholder="Tel"
						        variant="outlined"
						        value={inputsValue['seller_tel']}
						        name="seller_tel"
						        onChange={(value, key) => this.inputChange(value, 'seller_tel')}
						      />
					      </Grid>
					      <Grid item lg={6} xs={12}>
					      	<TextField
						        placeholder="Referrel"
						        variant="outlined"
						        value={inputsValue['seller_referrel']}
						        name="seller_referrel"
						        onChange={(value, key) => this.inputChange(value, 'seller_referrel')}
						      />
					      </Grid>
					      {/*<Grid item lg={6} xs={12}>
					      	<TextField
						        placeholder="Ad Type (Sold)"
						        variant="outlined"
						        value={inputsValue['ad_type_sold']}
						        name="ad_type_sold"
						        onChange={(value, key) => this.inputChange(value, 'ad_type_sold')}
						      />
					      </Grid>*/}

					    </Grid>
					    </MuiThemeProvider>
     			 	</DialogContent>
     			 	<DialogActions>
     			 		<Button onClick={this.dialogCancel} color="primary">
		            Cancel
		          </Button>
		          <Button 
		          	onClick={this.dialogSave} 
		          	disabled={inputsValue['seller_name']==='' ||inputsValue['seller_email']===''}
		          	color="primary">
		            Add
		          </Button>
     			 	</DialogActions>
     			 </Dialog>
	      </div>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  }),
  dispatch => ({
    tokenChange: (data) => {
      dispatch({ type: 'TOKEN_CHANGE', payload: data });
    }
  })
)(AddSeller);