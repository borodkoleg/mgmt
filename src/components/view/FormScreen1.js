import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import DialogWarningG from '../general/DialogWarningG';

const { isNumber } = require('../../server/libs/validation');
const { basisDialogScreen } = require('../general/themes');
const theme1 = createMuiTheme(basisDialogScreen);
const Axios = require('axios');

function validationFields(obj){
	if (
		!obj['imei'] || 
		!obj['brand'] || 
		!obj['color'] || 
		!obj['warranty'] || 
		!obj['case'] || 
		!obj['product_code'] || 
		!obj['model'] || 
		!obj['carrier'] || 
		!obj['condition'] ||
		!isNumber(obj['gb'])
		) {
		return false
	} 
	return true
}

class FormScreen1 extends Component {
  state = {
    someImei: false
  };
  
  componentDidMount() {
  	//start only one
  }

  componentDidUpdate(){
  	//start every update
  }

  // static getDerivedStateFromProps(props, state) {
  // 	let data = props.globalState.basisAgGrid.selectedRows[0]
  //   if (data && data.imei !== state.imeiNow) {
  //   	//console.log(Math.random());
  //   	let newObj = {};
  //   	newObj.id = data.id;
  //   	newObj.imei = data.imei;
  //   	newObj.brand = data.brand;
  //   	newObj.color = data.color;
  //   	newObj.warranty = data.warranty;
  //   	newObj.case = data.case;
  //   	newObj.product_code = data.product_code;
  //   	newObj.model = data.model;
  //   	newObj.carrier = data.carrier;
  //   	newObj.condition = data.condition;
  //     return {
  //       inputsValue: {...newObj},
  //       imeiNow: data.imei
  //     };
  //   }
  //   // No state update necessary
  //   return null;
  // }

  inputChange = (data, key) => {
  	let phone = this.props.phone;

  	let newObj = {...phone};
  	newObj[key] = (data.target.value).toUpperCase();

  	this.props.phoneChange(newObj);

  	if (key === 'imei'){
  		if (data.target.value.length > 13){
		  	(this.props.basisData).forEach((obj) => {
		  		if ((obj['imei'] + "") === (data.target.value + "")){
		  			console.log('obj[imei]=> ' + obj['imei']);
		  			console.log('phone[imei]=> ' + phone['imei']);
		  			this.setState({
		  				someImei: true
		  			})
		  		}
		  	});
	  	}
  	}

  }

  saveData(next){
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/save-form-screen-1' , {data: this.props.phone},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
	    })
	    .then((result) => {
	      if (result['data']){
	      	//console.log(JSON.stringify(result['data'], null, 2));
	      	this.props.sendMessage('success','Success');
	      	if (next){
	      		let phone = this.props.phone;
	      		phone['id'] = result['data'];
	      		this.props.changeAll({
	      			tabValue: 1,
	      			phone: phone
	      		});
	      		this.props.dataUpdate(result['data']);
	      	} else { //save
	      		this.props.changeAll({
							addPhoneManuallyStart: false,
							tabValue: 0,
							tabsShow: false
						})
	      		//this.props.tabsShow(false);
	      		this.props.dataUpdate();
	      		//this.props.addManually(false)
	      		//this.props.tabValue(0);
	      		this.props.deselectAllRowsBasis();
	      	}
	    	}
	    	// this.setState({
	    	// 	inputsValue: {}
	    	// })
	  });
  }

  saveClick = () => {
  	this.saveData(false);
  }

  saveAndNextClick = () => {
  	this.saveData(true);
  }

  clickSomeImeiClose = () => {
  	this.setState({
  		someImei: false
  	});
  }

  render() {
    const {someImei} = this.state;

    //console.log(JSON.stringify(inputsValue, null, 2));

    return (
    	<Grid container>
    	<MuiThemeProvider theme={theme1}>
    		<Grid item xs={12} sm={12} md={12} style={{
    		}}>
    			<div className='fixMaxWidth'>
    			<TextField
		        variant="outlined"
		        label="IMEI"
		        name="imei"
		        value={this.props.phone['imei'] || ''}
		        //ref={(c) => this.blabla = c}
		        disabled={this.props.addPhoneManuallyStart ? false : true}
		        onChange={(value, key) => this.inputChange(value, 'imei')}
		        error={!this.props.phone['imei']}
			    />
			    <TextField
		        variant="outlined"
		        label="Brand"
		        value={this.props.phone['brand'] || ''}
		        name="brand"
		        disabled={this.props.addPhoneManuallyStart ? false : true}
		        onChange={(value, key) => this.inputChange(value, 'brand')}
		        error={!this.props.phone['brand']}
			    />
			    <TextField
		        variant="outlined"
		        label="Color"
		        value={this.props.phone['color'] || ''}
		        name="color"
		        onChange={(value, key) => this.inputChange(value, 'color')}
		        error={!this.props.phone['color']}
			    />
			    <TextField
		        variant="outlined"
		        label="Warranty"
		        value={this.props.phone['warranty'] || ''}
		        name="warranty"
		        onChange={(value, key) => this.inputChange(value, 'warranty')}
		        error={!this.props.phone['warranty']}
			    />
			    <TextField
		        variant="outlined"
		        label="Packaging"
		        value={this.props.phone['case'] || ''}
		        name="case"
		        onChange={(value, key) => this.inputChange(value, 'case')}
		        error={!this.props.phone['case']}
			    />

    			<TextField
		        variant="outlined"
		        label="Serial number"
		        value={this.props.phone['product_code'] || ''}
		        name="product_code"
		        disabled={this.props.addPhoneManuallyStart ? false : true}
		        onChange={(value, key) => this.inputChange(value, 'product_code')}
		        error={!this.props.phone['product_code']}
			    />
			    <TextField
		        variant="outlined"
		        label="Model"
		        value={this.props.phone['model'] || ''}
		        name="model"
		        onChange={(value, key) => this.inputChange(value, 'model')}
		        error={!this.props.phone['model']}
			    />
			    <TextField
		        variant="outlined"
		        label="Carrier"
		        value={this.props.phone['carrier'] || ''}
		        name="carrier"
		        onChange={(value, key) => this.inputChange(value, 'carrier')}
		        error={!this.props.phone['carrier']}
			    />
			    <TextField
		        variant="outlined"
		        label="Condition"
		        value={this.props.phone['condition'] || ''}
		        name="condition"
		        onChange={(value, key) => this.inputChange(value, 'condition')}
		        error={!this.props.phone['condition']}
			    />
			    <TextField
		        variant="outlined"
		        label="Gb"
		        value={this.props.phone['gb'] || ''}
		        name="gb"
		        onChange={(value, key) => this.inputChange(value, 'gb')}
		        error={!isNumber(this.props.phone['gb'])}
			    />
			    </div>
    		</Grid>
    		<Grid item xs={12} sm={12}>
    		<Button 
	    		variant="contained" 
	    		size="medium"	    		
	    		onClick={this.saveClick}
	    		disabled={!validationFields(this.props.phone)}
			  > 
		      Save
		    </Button>
		    <Button 
	    		variant="contained" 
	    		size="medium"	
	    		style={{marginLeft: '20px'}}
	    		onClick={this.saveAndNextClick}
	    		disabled={!validationFields(this.props.phone)}
			  > 
		      Save & Next
		    </Button>
		    </Grid>

		    <DialogWarningG
	    		open={someImei}
	    		title={'This imei already exists'}
	    		text={'This will overwrite the phone'}
	    		clickBtn={(data) => this.clickSomeImeiClose(data)}
	    		close={this.clickSomeImeiClose}
	    	/>

    		</MuiThemeProvider>
    	</Grid>
    )
  }
}

export default connect(
  state => ({
    globalState: state,
    phone: state.basisAgGrid.phone,
    addPhoneManuallyStart: state.basisAgGrid.addPhoneManuallyStart
  }),
  dispatch => ({
    tabsShow: (data) => {
    	dispatch({ type: 'BA_TABS_SHOW', payload: data });
    },
    tabValue: (data) => {
    	dispatch({ type: 'BA_TAB_VALUE', payload: data });
    },
    selectedRows: (data) => {
    	dispatch({ type: 'BA_SELECTED_ROWS', payload: data });
    },
    phoneChange: (data) => {
    	dispatch({ type: 'BA_PHONE', payload: data });
    },
    addManually: (data) => {
    	dispatch({ type: 'BA_ADD_MANUALLY', payload: data });
    },
    changeAll: (data) => {
    	dispatch({ type: 'BASIS_GRID_CHANGE_ALL', payload: data });
    }
  })
)(FormScreen1);