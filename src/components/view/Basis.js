import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { basis_phones } from '../../data/fields'
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import Notify from '../general/Notify';

import BtnRefresh from './BtnRefresh';
import FormScreen1 from './FormScreen1';
import FormScreen2 from './FormScreen2';
import FormScreen3 from './FormScreen3';
import FormScreen4 from './FormScreen4';
import FormScreen5 from './FormScreen5';
import FormScreenCamera from './FormScreenCamera';
import FormScreenOther from './FormScreenOther';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
const { tabs, theme } = require('../general/themes');
const theme3 = createMuiTheme(tabs);
const theme1 = createMuiTheme(theme);

const { dateTimeFormat } = require('../../server/libs/date');

const Axios = require('axios');

let d = new Date();  
//d.setDate(d.getDate()-2); //temp

class Basis extends Component {
  state = {
    rowData: [],
    selectedDate: d,//new Date(),
    calendarOpen: false
  };

  dataUpdate(selectId) {
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get_products' , {date: this.state.selectedDate},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
      if (result){
      	this.setState({
      		rowData: [ ...result['data'] ]
      	});
      }

      if (selectId){
    		this.gridApi.forEachNode((node) => {
          if (node.data.id === selectId) {
            // Master open detail.  You could also call node.setSelected( true ); for alternate design.
            //node.setExpanded(true);
            node.setSelected( true );
          }
        });
      }
      this.setDate();
    });
  }
  
  componentDidMount() {
  	this.dataUpdate();
  }

  handleDateChange = (date) => {
  	this.setState({
    	selectedDate: date,
    	calendarOpen: false
		}, () => {
			this.deselectAllRowsBasis();
		  this.setDate();
		});
  }

  deselectAllRowsBasis(){
  	this.gridApi.deselectAll();
	}

  setDate = () => {
  	//console.log(typeof this.state.selectedDate);
    let filter = this.gridApi.getFilterInstance("device_updated_date");
    filter.setModel({
      type: "startsWith",
      filter: dateTimeFormat(this.state.selectedDate, "YYYY-MM-DD", "YYYY-MM-DD")
    });
    this.gridApi.onFilterChanged();
  }

  onGridReady = params => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.gridColumnApi.getColumn('device_updated_date').setSort("desc")
  };
  
  onSelectionChanged = () => {

  }

  addPhoneManually = () => {
  	this.gridApi.deselectAll();
  	this.props.changeAll({
			selectedRows: [],
			addPhoneManuallyStart: true,
			tabValue: 0,
			tabsShow: true,
  		phone: false,
  	})
  	// this.props.selectedRows([]);
  	// this.props.addManually(true);
  	// this.props.tabValue(0);
  }

  addCamera = () => {
  	this.gridApi.deselectAll();
  	this.props.changeAll({
			selectedRows: [],
			addPhoneManuallyStart: false,
			tabValue: 5,
			tabsShow: false,
  		phone: false,
  	})
  }

  addOther = () => {
  	this.gridApi.deselectAll();
  	this.props.changeAll({
			selectedRows: [],
			addPhoneManuallyStart: false,
			tabValue: 6,
			tabsShow: false,
  		phone: false,
  	})
  }

  handleDateOpen = () => {
  	this.setState({
  		calendarOpen: true
  	})
  }

  sendMessage(status, text){
  	//success
  	//error
  	
  	this.refs.notifyC.notify(status, text);
  }

  handleTabChange = (event, newValue) => {

  }

  handleDateClose = () => {
  	this.setState({
    	calendarOpen: false
		})
  }

  onRowClicked = () => {
  	const selectedRows = this.gridApi.getSelectedRows();
  	//console.log(JSON.stringify(selectedRows[0],null,2));

  	if (selectedRows.length === 0) {
  		this.props.changeAll({
  			selectedRows: [],
  			tabsShow: false,
  			phone: false,
  			tabValue: 0
  		})

  		// if (this.props.globalState.basisAgGrid.addPhoneManuallyStart){
  		// 	this.props.tabsShow(true);
  		// }
  	}

  	if (selectedRows.length === 1) {
  		this.props.changeAll({
  			selectedRows: selectedRows,
  			addPhoneManuallyStart: false,
  			tabsShow: true,
  			phone: selectedRows[0],
  			tabValue: 0
  		})
  	}
  }

  render() {
    const {
    	rowData, 
    	selectedDate,
    	calendarOpen,
    } = this.state;

    //console.log(JSON.stringify(this.props.globalState.basisAgGrid.phone, null, 2));

    return (
    	<div style={{marginTop:'20px'}}>
    	<div className={'response-div-basis-left'}>
	    	<div style={{
	    		color:'black',
	    		borderBottom: '1px solid rgba(0, 0, 0, 0.42)'
	    		}}>
	    		<div 
	    			className={'small-grey-text'}
	    			style={{marginBottom: '7px'}}>
	    			Last scan time
	    		</div>
	    		<BtnRefresh
		      	history={this.props.history}
		      	dataUpdate={() => this.dataUpdate()}
		      />
		    </div>


	  		<MuiPickersUtilsProvider utils={DateFnsUtils}>
	    		<KeyboardDatePicker
	          disableToolbar
	          variant="inline"
	          format="MM-dd-yyyy"
	          margin="normal"
	          style={{width: '100%'}}
	          open={calendarOpen}
	          label="Phones from PHONECHECK by date"
	          value={selectedDate}
	          onChange={this.handleDateChange}
	          onOpen={this.handleDateOpen}
	          onClose={this.handleDateClose}
	          className="datePickerBasis"
	          KeyboardButtonProps={{
	            'aria-label': 'change date',
	          }}
	        />
	      </MuiPickersUtilsProvider>

	    	<div 
					className="ag-theme-balham"
		    	style={{ 
		        height: '380px'
		      }} >
	    		<AgGridReact
	          columnDefs={basis_phones}
	          rowData={rowData}
	          onGridReady={this.onGridReady}
	          //domLayout={'autoHeight'}
	          rowStyle = {{fontSize: '15px'}}
	      		//headerHeight = {150}
	      		onRowClicked={this.onRowClicked}
	      		rowClassRules = {{
	      			//'grid-green': 'data.imei',
	      			'grid-red': 
								`!data.imei ||
    					 	!data.brand ||
    					 	!data.color ||
    						!data.warranty ||
    						!data.case ||
    						!data.product_code ||
    						!data.model ||
    						!data.carrier ||
    						!data.condition ||
    						!data.gb`,
    					'grid-success':
    					`data.imei &&
								data.brand &&
								data.color &&
								data.warranty &&
								data.case &&
								data.product_code &&
								data.model &&
								data.carrier &&
								data.condition &&
								data.gb &&

								data.seller_id &&

								data.selling_price &&
								data.buying_price &&
								data.product_status`	
	      		}}
	      		//rowSelection = {'multiple'}
	      		rowSelection = {'single'}
	      		onSelectionChanged={this.onSelectionChanged}
		        >
		        </AgGridReact>
		    </div>
	    </div>
	    <div className={'response-div-basis-right'}>
	    		<MuiThemeProvider theme={theme1}>
		    		<Button 
		    		variant="contained" 
		    		size="small"	    		
		    		onClick={this.addPhoneManually}
		    		style={{
		    			top: '6px'
		    		}}
				  	>
				  		Add Phone Manually
				  	</Button>

				  	<Button 
		    		variant="contained" 
		    		size="small"	    		
		    		onClick={this.addCamera}
		    		style={{
		    			top: '6px',
		    			marginLeft: '20px'
		    		}}
				  	>
				  		Add Camera
				  	</Button>

				  	<Button 
		    		variant="contained" 
		    		size="small"	    		
		    		onClick={this.addOther}
		    		style={{
		    			top: '6px',
		    			marginLeft: '20px'
		    		}}
				  	>
				  		Add Other
				  	</Button>
			  	</MuiThemeProvider>
			  	<br/>
			  	<br/>
	    
	    	
	    		<MuiThemeProvider theme={theme3}>
	    			<div className={'basis-center-forms'}>
		    	 	<Tabs
		          value={this.props.globalState.basisAgGrid.tabValue}
		          //onChange={this.handleTabChange}
		          indicatorColor="primary"
		          textColor="primary"
		          variant="fullWidth"
		          style={!this.props.globalState.basisAgGrid.tabsShow ? {display: 'none'} : {}}
		          aria-label="full width tabs example"
		          
	        	>
	          <Tab disabled label="Phone Details" style={{textAlign:'left', background: '#f5f7f7'}} />
	          <Tab disabled label="Seller inf." 
	          	style={{background: '#f5f7f7'}}
	          />
	          <Tab disabled label="Repair inf." 
	          	style={{background: '#f5f7f7'}}
	          />
	          <Tab disabled label="Status & price" 
	          	style={{background: '#f5f7f7'}}
	          />
	          <Tab disabled label="Media" 
	          	style={{background: '#f5f7f7'}}
	          />

	          <Tab 
	          	style={{display: 'none'}}
	          />

	          <Tab
	          	style={{display: 'none'}}
	          />
	        	</Tabs>
        	
        		{this.props.globalState.basisAgGrid.tabValue === 0 && this.props.globalState.basisAgGrid.tabsShow && (
					    <FormScreen1
					    	dataUpdate={(select) => this.dataUpdate(select)}
					    	deselectAllRowsBasis={() => this.deselectAllRowsBasis()}
					    	sendMessage={(d,d1) => this.sendMessage(d,d1)}
					    	basisData={rowData}
					    />
				    )}

				    {this.props.globalState.basisAgGrid.tabValue === 1 && this.props.globalState.basisAgGrid.tabsShow && ( 	
					    <FormScreen2
					    	dataUpdate={(select) => this.dataUpdate(select)}
					    	deselectAllRowsBasis={() => this.deselectAllRowsBasis()}
					    	sendMessage={(d,d1) => this.sendMessage(d,d1)}
					    />
				    )}

				    {this.props.globalState.basisAgGrid.tabValue === 2 && this.props.globalState.basisAgGrid.tabsShow && ( 	
					    <FormScreen3
					    	dataUpdate={(select) => this.dataUpdate(select)}
					    	deselectAllRowsBasis={() => this.deselectAllRowsBasis()}
					    	sendMessage={(d,d1) => this.sendMessage(d,d1)}
					    />
				    )}

				    {this.props.globalState.basisAgGrid.tabValue === 3 && this.props.globalState.basisAgGrid.tabsShow && ( 	
					    <FormScreen4
					    	dataUpdate={(select) => this.dataUpdate(select)}
					    	deselectAllRowsBasis={() => this.deselectAllRowsBasis()}
					    	sendMessage={(d,d1) => this.sendMessage(d,d1)}
					    />
				    )}

				    {this.props.globalState.basisAgGrid.tabValue === 4 && this.props.globalState.basisAgGrid.tabsShow && (
					    <FormScreen5
					    	dataUpdate={(select) => this.dataUpdate(select)}
					    	deselectAllRowsBasis={() => this.deselectAllRowsBasis()}
					    	sendMessage={(d,d1) => this.sendMessage(d,d1)}
					    	basisData={rowData}
					    />
				    )}

				    {this.props.globalState.basisAgGrid.tabValue === 5 && (
				    <FormScreenCamera
				    	sendMessage={(d,d1) => this.sendMessage(d,d1)}
				    />
				    )}

				    {this.props.globalState.basisAgGrid.tabValue === 6 && (
				    <FormScreenOther
				    	sendMessage={(d,d1) => this.sendMessage(d,d1)}
				    />
				    )}

				  </div>
			    </MuiThemeProvider>
			</div>
			<Notify ref='notifyC' {...this.props} />
	    </div>
    )
  }
}

export default connect(
  state => ({
    globalState: state,
  }),
  dispatch => ({
    selectedRows: (data) => {
    	dispatch({ type: 'BA_SELECTED_ROWS', payload: data });
    },
    addManually: (data) => {
    	dispatch({ type: 'BA_ADD_MANUALLY', payload: data });
    },
    tabValue: (data) => {
    	dispatch({ type: 'BA_TAB_VALUE', payload: data });
    },
    tabsShow: (data) => {
    	dispatch({ type: 'BA_TABS_SHOW', payload: data });
    },
   	phone: (data) => {
    	dispatch({ type: 'BA_PHONE', payload: data });
    },
    changeAll: (data) => {
    	dispatch({ type: 'BASIS_GRID_CHANGE_ALL', payload: data });
    }
  })
)(Basis);