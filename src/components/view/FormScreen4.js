import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const { statusPhone } = require('../../data/db');
const { isNumber } = require('../../server/libs/validation');

const { basisDialogScreen } = require('../general/themes');
const theme1 = createMuiTheme(basisDialogScreen);

const Axios = require('axios');

class FormScreen4 extends Component {
  state = {
    
  };
  
  componentDidMount() {
  	
  }

  componentDidUpdate(){
  	//start every update
  }

  saveData = (next) => {
  	let phone = this.props.phone;
  	let data = {};
  	data['selling_price'] = phone['selling_price'];
  	data['product_status'] = phone['product_status'];
  	data['id'] = phone['id'];

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/save-form-screen-3' , {
  			data: data,
  		},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
	    })
	    .then((result) => {
	      if (result['data']){
	      	this.props.sendMessage('success','Success');
	      	//console.log(next);
	      	if (next){
	      		this.props.tabValue(4);
	      		this.props.dataUpdate(result['data']);
	      	} else { //save
	      		this.props.changeAll({
							addPhoneManuallyStart: false,
							tabValue: 0,
							tabsShow: false
						})
	      		//this.props.tabsShow(false);
	      		//this.props.addManually(false);
	      		this.props.dataUpdate();
	      		//this.props.tabValue(0);
	      		this.props.deselectAllRowsBasis();
	      	}
	    	}
	  });
  }

	inputChange = (data, key) => {
		let phone = this.props.phone;
		phone[key] = (data.target.value).toUpperCase();
		this.props.phoneChange(phone);
	}

	validationFunc(){
		if (
			isNumber(this.props.phone['selling_price']) &&
			this.props.phone['product_status']
			){
			return true
		}
		return false
	}

	backClick = () => {
  	this.props.tabValue(2);
  }

  render() {
    //console.log(JSON.stringify(inputsValue, null, 2));

    return (
    	<Grid container>
    		<MuiThemeProvider theme={theme1}>
    		<Grid item xs={12} sm={12} md={12}>
    			<div className='fixMaxWidth'>
    			<TextField
		        variant="outlined"
		        label="Selling Price"
		        name="selling_price"
		        value={this.props.phone['selling_price'] || ''}
		        onChange={(value, key) => this.inputChange(value, 'selling_price')}
		        error={!isNumber(this.props.phone['selling_price'])}
	        />
	        <br/>
	        <TextField
	        	style={{minWidth: '240px'}}
		        select
		        label="Product Status"
		        value={this.props.phone['product_status'] || ''}
		        onChange={(data, key) => this.inputChange(data, 'product_status')}
		        SelectProps={{
		          native: true
		        }}
		        //helperText="Please select your currency"
		        margin="normal"
		        error={!this.props.phone['product_status']}
		      >
		        {statusPhone.map(option => (
		          <option key={option.value} value={option.value}>
		            {option.label}
		          </option>
		        ))}
      		</TextField>
      		</div>
	      </Grid>
    		<Grid container style={{marginTop:'40px'}}>
    		<Grid item xs={12} sm={12} md={12}>
    		<Button 
	    		variant="contained" 
	    		size="medium"	    		
	    		onClick={this.backClick}
			  > 
		      Back
		    </Button>
    		<Button 
	    		variant="contained" 
	    		size="medium"	  
	    		style={{marginLeft: '40px'}}  		
	    		onClick={() => this.saveData(false)}
	    		disabled={!this.validationFunc()}
			  > 
		      Save
		    </Button>
		    <Button 
	    		variant="contained" 
	    		size="medium"	
	    		style={{marginLeft: '20px'}}
	    		disabled={!this.validationFunc()}
	    		onClick={() => this.saveData(true)}
			  >
		      Save & Next
		    </Button>
		    </Grid>
		    </Grid>
		    </MuiThemeProvider>
    	</Grid>
    )
  }
}

export default connect(
  state => ({
    globalState: state,
    phone: state.basisAgGrid.phone,
    addPhoneManuallyStart: state.basisAgGrid.addPhoneManuallyStart
  }),
  dispatch => ({
    tabsShow: (data) => {
    	dispatch({ type: 'BA_TABS_SHOW', payload: data });
    },
    tabValue: (data) => {
    	dispatch({ type: 'BA_TAB_VALUE', payload: data });
    },
    selectedRows: (data) => {
    	dispatch({ type: 'BA_SELECTED_ROWS', payload: data });
    },
    phoneChange: (data) => {
    	dispatch({ type: 'BA_PHONE', payload: data });
    },
    addManually: (data) => {
    	dispatch({ type: 'BA_ADD_MANUALLY', payload: data });
    },
    changeAll: (data) => {
    	dispatch({ type: 'BASIS_GRID_CHANGE_ALL', payload: data });
    }
  })
)(FormScreen4);