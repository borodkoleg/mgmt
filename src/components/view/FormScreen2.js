import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const { isNumber } = require('../../server/libs/validation');
const { basisDialogScreen } = require('../general/themes');
const theme1 = createMuiTheme(basisDialogScreen);
const Axios = require('axios');

function validationFields(obj){
	if (
		!obj['seller_identification'] ||
		!obj['seller_email'] ||
		!isNumber(obj['buying_price'])
		) {
		return false
	} 
	return true
}

class FormScreen2 extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  componentDidUpdate(){
  	//start every update
  }

  saveData(next){
  	let phone = this.props.phone;
  	let data = {};
  	data['seller_name'] = phone['seller_name'];
  	data['seller_tel'] = phone['seller_tel'];
  	data['seller_email'] = phone['seller_email'];
  	data['seller_referrel'] = phone['seller_referrel'];
  	data['seller_identification'] = phone['seller_identification'];

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/save-form-screen-2' , {
  			data: data,
  			phoneId: phone['id'],
  			buyingPrice: phone['buying_price']
  		},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
	    })
	    .then((result) => {
	      if (result['data']){
	      	//console.log(JSON.stringify(result['data'], null, 2));
	      	this.props.sendMessage('success','Success');
	      	if (next){
	      		this.props.tabValue(2);
	      		this.props.dataUpdate(result['data']);
	      	} else { //save
	      		this.props.changeAll({
							addPhoneManuallyStart: false,
							tabValue: 0,
							tabsShow: false
						})
	      		this.props.dataUpdate();
	      		this.props.deselectAllRowsBasis();
	      	}
	    	} else {
	    		this.props.sendMessage('error','This email already exists');
	    	}
	  });
  }

  saveClick = () => {
  	this.saveData();
  }

  saveAndNextClick = () => {
  	this.saveData(true);
  }

  backClick = () => {
  	this.props.tabValue(0);
  }

  // onQuickFilterChanged = () => {
  // 	this.gridApi.setQuickFilter(document.getElementById("quickFilter").value);

  // 	this.setState({
  // 		buttonsDisabled: true
  // 	});
  // 	this.gridApi.deselectAll();
  // }

  inputChange = (data, key) => {
  	let phone = this.props.phone;

  	let newObj = {...phone};
  	newObj[key] = (data.target.value).toUpperCase();

  	this.props.phoneChange(newObj);
  }

  onGridReady = params => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    //this.gridColumnApi.getColumn('device_updated_date').setSort("desc")
  };

  render() {
    //console.log(JSON.stringify(inputsValue, null, 2));

    return (
    	<Grid container 
    	style={this.props.disabled ? {visibility: 'hidden'} : {}}
    	>
    	<MuiThemeProvider theme={theme1}>
    		<Grid item xs={12} sm={12} md={12}>
    			<div className='fixMaxWidth'>
    			{/*<input
            type="text"
            onInput={this.onQuickFilterChanged}
            id="quickFilter"
            placeholder="quick filter..."
            className={'quick-filter'}
          />*/} 
          <TextField
		        variant="outlined"
		        label="Name"
		        name="seller_name"
		        value={this.props.phone['seller_name'] || ''}
		        onChange={(value, key) => this.inputChange(value, 'seller_name')}
		        //error={!this.props.phone['seller_name']}
			    />

			    <TextField
		        variant="outlined"
		        label="Telephone"
		        name="seller_tel"
		        value={this.props.phone['seller_tel'] || ''}
		        onChange={(value, key) => this.inputChange(value, 'seller_tel')}
		        //error={!this.props.phone['seller_name']}
			    />

			    <TextField
		        variant="outlined"
		        label="Email"
		        name="seller_email"
		        value={this.props.phone['seller_email'] || ''}
		        onChange={(value, key) => this.inputChange(value, 'seller_email')}
		        error={!this.props.phone['seller_email']}
			    />
    			<TextField
		        variant="outlined"
		        label="Referrel"
		        name="seller_referrel"
		        value={this.props.phone['seller_referrel'] || ''}
		        onChange={(value, key) => this.inputChange(value, 'seller_referrel')}
		        //error={!this.props.phone['seller_referrel']}
			    />
			    <TextField
		        variant="outlined"
		        label="Identification"
		        name="seller_identification"
		        value={this.props.phone['seller_identification'] || ''}
		        onChange={(value, key) => this.inputChange(value, 'seller_identification')}
		        error={!this.props.phone['seller_identification']}
			    />  	

			    <TextField
		        variant="outlined"
		        label="Buying Price"
		        name="buying_price"
		        value={this.props.phone['buying_price'] || ''}
		        onChange={(value, key) => this.inputChange(value, 'buying_price')}
		        error={!isNumber(this.props.phone['buying_price'])}
	        />	
	        </div>
    		</Grid>
    		<Grid container style={{marginTop:'40px'}}>
    		<Grid item xs={12} sm={12} md={12}>
    		<Button 
	    		variant="contained" 
	    		size="medium"	    		
	    		onClick={this.backClick}
			  > 
		      Back
		    </Button>
    		<Button 
	    		variant="contained" 
	    		size="medium"	    		
	    		onClick={this.saveClick}
	    		style={{marginLeft: '40px'}}
	    		disabled={!validationFields(this.props.phone)}
			  > 
		      Save
		    </Button>
		    <Button 
	    		variant="contained" 
	    		size="medium"	
	    		disabled={!validationFields(this.props.phone)}
	    		onClick={this.saveAndNextClick}
	    		style={{marginLeft: '20px'}}
			  > 
		      Save & Next
		    </Button>
		    </Grid>
		    </Grid>
    		</MuiThemeProvider>
    	</Grid>
    )
  }
}

export default connect(
  state => ({
    globalState: state,
    phone: state.basisAgGrid.phone,
    addPhoneManuallyStart: state.basisAgGrid.addPhoneManuallyStart
  }),
  dispatch => ({
    tabsShow: (data) => {
    	dispatch({ type: 'BA_TABS_SHOW', payload: data });
    },
    tabValue: (data) => {
    	dispatch({ type: 'BA_TAB_VALUE', payload: data });
    },
    selectedRows: (data) => {
    	dispatch({ type: 'BA_SELECTED_ROWS', payload: data });
    },
    phoneChange: (data) => {
    	dispatch({ type: 'BA_PHONE', payload: data });
    },
    addManually: (data) => {
    	dispatch({ type: 'BA_ADD_MANUALLY', payload: data });
    },
    changeAll: (data) => {
    	dispatch({ type: 'BASIS_GRID_CHANGE_ALL', payload: data });
    }
  })
)(FormScreen2);